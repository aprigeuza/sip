-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2020 at 09:30 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sip`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `get_total_item_invoice` (`p_invoice_id` INT) RETURNS DOUBLE BEGIN
DECLARE res DOUBLE DEFAULT 0;

SELECT IFNULL(SUM((qty * unit_price)), 0) INTO res FROM invoice_item WHERE invoice_id = p_invoice_id;

RETURN res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_total_payment` (`p_invoice_id` INT) RETURNS DOUBLE BEGIN
DECLARE res DOUBLE DEFAULT 0;

SELECT IFNULL(SUM(amount), 0) INTO res FROM invoice_payment WHERE invoice_id = p_invoice_id AND cancel = 0;

RETURN res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `get_total_sales_amount` (`p_item_id` INT) RETURNS DOUBLE BEGIN
DECLARE res DOUBLE DEFAULT 0;

SELECT IFNULL(SUM(qty * unit_price), 0) INTO res FROM invoice_item 
INNER JOIN invoice ON invoice.id = invoice_item.invoice_id
WHERE 
invoice.cancel = 0
AND 
invoice.posted = 1
AND
invoice.closed = 1
AND invoice_item.item_id = p_item_id
;

RETURN res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `MENU_ACCESS` (`p_user_id` INT, `p_menu_id` INT) RETURNS INT(11) BEGIN
DECLARE _res boolean default false;

SELECT 
CASE WHEN (COUNT(id)>=1) THEN TRUE ELSE FALSE END INTO _res
FROM user_menu 
WHERE
user_id = p_user_id AND menu_id = p_menu_id;

RETURN _res;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `default_billing_address` text NOT NULL,
  `default_shipping_address` text NOT NULL,
  `obsolete` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `cust_code`, `cust_name`, `npwp`, `telp`, `default_billing_address`, `default_shipping_address`, `obsolete`, `input_date`, `input_by`) VALUES
(1, 'C0001', 'PT. TORABIKA EKA SEMESTA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(4, 'C0003', 'PT. INDAH KARGO', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(5, 'C0004', 'PT. BUKALAPAK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(6, 'C0005', 'PT. INDOMOBI', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(7, 'C0006', 'PT. LAUTAN STELL', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(9, 'C0007', 'PT. INDAH MAYA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(10, 'C0008', 'PT. PERTAMINA, TBK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(11, 'C0009', 'PT. AQUA INDONESIA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(12, 'C0010', 'PT. UNILEVER INDONESIA (BSD)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(13, 'C0011', 'PT. UNILEVER INDONESIA (BOGOR)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order`
--

CREATE TABLE `delivery_order` (
  `id` int(11) NOT NULL,
  `do_number` varchar(10) NOT NULL,
  `do_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `vehicle_type` varchar(45) DEFAULT NULL,
  `vehicle_number` varchar(45) DEFAULT NULL,
  `vehicle_driver` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery_order`
--

INSERT INTO `delivery_order` (`id`, `do_number`, `do_date`, `delivery_date`, `customer_id`, `cust_code`, `cust_name`, `telp`, `billing_address`, `shipping_address`, `cancel`, `posted`, `input_date`, `input_by`, `vehicle_type`, `vehicle_number`, `vehicle_driver`) VALUES
(9, '20020001', '2020-02-02', '0000-00-00', 3, 'C0002', 'PT. MAYORA INDAH', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-02 06:15:14', 1, NULL, 'A 7894 HUY', 'Geovani'),
(10, '20020002', '2020-02-02', '0000-00-00', 10, 'C0008', 'PT. PERTAMINA, TBK', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-02 08:43:22', 1, NULL, 'A 7894 HUY', 'Zepri'),
(11, '20020003', '2020-02-02', '0000-00-00', 10, 'C0008', 'PT. PERTAMINA, TBK', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-02 08:45:42', 1, NULL, 'B 4567 GAS', 'Geovani');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order_item`
--

CREATE TABLE `delivery_order_item` (
  `id` int(11) NOT NULL,
  `delivery_order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `sales_order_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery_order_item`
--

INSERT INTO `delivery_order_item` (`id`, `delivery_order_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `sales_order_item_id`) VALUES
(29, 9, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 200, 'Roll', 16),
(30, 9, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 300, 'Roll', 17),
(32, 10, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 500, 'Roll', 18),
(34, 11, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 500, 'Roll', 18);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `iv_number` varchar(10) NOT NULL,
  `iv_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(4) NOT NULL DEFAULT 0,
  `sales_order_id` int(11) NOT NULL,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `closed` tinyint(4) NOT NULL DEFAULT 0,
  `iv_note` longtext DEFAULT NULL,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0,
  `deduction_dp` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `iv_number`, `iv_date`, `delivery_date`, `customer_id`, `cust_code`, `cust_name`, `npwp`, `telp`, `billing_address`, `ppn`, `cancel`, `posted`, `sales_order_id`, `input_date`, `input_by`, `due_date`, `closed`, `iv_note`, `shipping_cost`, `other_cost`, `deduction_dp`) VALUES
(18, '20020001', '2020-02-02', '0000-00-00', 3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-02 06:40:32', 1, '2020-02-29', 1, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 1200000, 0, 300000),
(19, '20020002', '2020-02-02', '0000-00-00', 10, 'C0008', 'PT. PERTAMINA, TBK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 0, 0, '2020-02-02 08:46:28', 1, NULL, 0, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item`
--

CREATE TABLE `invoice_item` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL,
  `delivery_order_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_item`
--

INSERT INTO `invoice_item` (`id`, `invoice_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `unit_price`, `subtotal`, `delivery_order_item_id`) VALUES
(22, 18, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 200, 'Roll', 5000, 1000000, 29),
(23, 18, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 300, 'Roll', 15000, 4500000, 30),
(24, 19, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 500, 'Roll', 5000, 2500000, 32),
(25, 19, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 500, 'Roll', 5000, 2500000, 34);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payment`
--

CREATE TABLE `invoice_payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_type` varchar(45) NOT NULL,
  `payment_proof` varchar(100) NOT NULL,
  `amount` double NOT NULL,
  `cancel` tinyint(4) DEFAULT 0,
  `input_date` datetime DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_payment`
--

INSERT INTO `invoice_payment` (`id`, `invoice_id`, `payment_date`, `payment_type`, `payment_proof`, `amount`, `cancel`, `input_date`, `input_by`, `remark`) VALUES
(4, 18, '2020-02-21', 'Transfer BCA', 'BCA001', 6950000, 0, '2020-02-02 00:00:00', 1, 'Full Payment');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `unit_price` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `obsolete` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `item_code`, `item_name`, `item_description`, `unit_price`, `unit`, `obsolete`, `input_date`, `input_by`) VALUES
(1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 5000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 25000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 15000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 78000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(5, 'GB-X05', 'GEOBAG X05', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 104000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(6, 'GB-X06', 'GEOBAG X06', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 25800, 'Roll', 0, '2020-01-12 00:32:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(3) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `map` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `sort_number` int(11) NOT NULL,
  `parent_id` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `name`, `uri`, `map`, `icon`, `sort_number`, `parent_id`) VALUES
(10, 'Dashboard', 'dashboard', 'dashboard', 'dashboard', 'fa fa-dashboard fa-fw', 1, 0),
(20, 'Setting', 'setting', '#', 'setting', 'fa fa-cogs fa-fw', 2, 0),
(21, 'Umum', 'general_setting', 'setting/general_setting', 'setting.general_setting', 'fa fa-circle-o', 1, 20),
(22, 'User', 'user', 'setting/user', 'setting.user', 'fa fa-circle-o', 2, 20),
(30, 'Sales', 'sales', '#', 'sales', 'fa fa-shopping-cart fa-fw', 3, 0),
(31, 'Customer', 'customer', 'sales/customer', 'sales.customer', 'fa fa-circle-o', 1, 30),
(32, 'Quotation', 'quotation', 'sales/quotation', 'sales.quotation', 'fa fa-circle-o', 2, 30),
(33, 'Sales Order', 'sales_order', 'sales/sales_order', 'sales.sales_order', 'fa fa-circle-o', 3, 30),
(40, 'Procurement', 'procurement', '#', 'procurement', 'fa fa-building fa-fw', 4, 0),
(41, 'Item', 'item', 'procurement/item', 'procurement.item', 'fa fa-circle-o', 1, 40),
(43, 'Delivery Order', 'delivery_order', 'procurement/delivery_order', 'procurement.delivery_order', 'fa fa-circle-o', 3, 40),
(50, 'Finance', 'finance', '#', 'finance', 'fa fa-money fa-fw', 5, 0),
(51, 'Invoice', 'invoice', 'finance/invoice', 'finance.invoice', 'fa fa-circle-o', 1, 50),
(52, 'Payment', 'payment', 'finance/payment', 'finance.payment', 'fa fa-circle-o', 2, 50),
(60, 'Report', 'report', '#', 'report', 'fa fa-table fa-fw', 6, 0),
(61, 'Item', 'rep_item', 'report/rep_item', 'report.rep_item', 'fa fa-circle-o', 1, 60),
(62, 'Customer', 'rep_customer', 'report/rep_customer', 'report.rep_customer', 'fa fa-circle-o', 2, 60),
(63, 'Sales Order', 'rep_sales_order', 'report/rep_sales_order', 'report.rep_sales_order', 'fa fa-circle-o', 3, 60);

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `quot_number` int(10) NOT NULL,
  `quot_date` date NOT NULL,
  `due_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text NOT NULL,
  `shipping_address` text NOT NULL,
  `ppn` int(11) NOT NULL,
  `refrence` varchar(50) NOT NULL,
  `cancel` tinyint(4) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `quot_note` longtext DEFAULT NULL,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `quot_number`, `quot_date`, `due_date`, `customer_id`, `cust_code`, `cust_name`, `npwp`, `telp`, `billing_address`, `shipping_address`, `ppn`, `refrence`, `cancel`, `input_date`, `input_by`, `quot_note`, `shipping_cost`, `other_cost`) VALUES
(20, 20020001, '2020-02-27', '2020-02-21', 3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 'Pak. Suprapto', 0, '2020-02-02 05:53:46', 1, '<div>\r\nTERM &amp; CONDITIONAL : <br></div><div><ol><li>Harga Sudah Termasuk PPN 10%</li><li>Harga Free Ongkir ke BSD</li><li>Pembayaran Full Payment</li><li>Telp./Fax : (021) 5945 0128 <br></li><li>E-Mail : primageotexindo@gmail.com</li></ol><p>\r\nPembayaran dapat ditransfer via :\r\n\r\n<br></p></div><div><b>Rek : PT. PRIMA GEOTEX INDO</b> <br></div><div><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-22.</li><li>BNI - Cabang Bumi Serpong Damai<br>A/C : 04892162083.</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7\r\n\r\n</li></ol></div><br>', 500000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quotation_item`
--

CREATE TABLE `quotation_item` (
  `id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quotation_item`
--

INSERT INTO `quotation_item` (`id`, `quotation_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `unit_price`, `subtotal`) VALUES
(11, 20, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 1000, 'Roll', 25000, 25000000),
(12, 20, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 1500, 'Roll', 15000, 22500000);

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `id` int(11) NOT NULL,
  `so_number` varchar(10) NOT NULL,
  `so_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(1) NOT NULL DEFAULT 0,
  `quotation_id` int(11) NOT NULL,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `po_number` varchar(100) DEFAULT NULL,
  `total_dp` double DEFAULT 0,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales_order`
--

INSERT INTO `sales_order` (`id`, `so_number`, `so_date`, `delivery_date`, `customer_id`, `cust_code`, `cust_name`, `npwp`, `telp`, `billing_address`, `shipping_address`, `ppn`, `cancel`, `posted`, `quotation_id`, `input_date`, `input_by`, `po_number`, `total_dp`, `shipping_cost`, `other_cost`) VALUES
(12, '20020001', '2020-02-02', '0000-00-00', 3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-02 06:08:46', 1, '', 10000000, 500000, 0),
(13, '20020002', '2020-02-02', '2020-02-28', 10, 'C0008', 'PT. PERTAMINA, TBK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-02 08:40:24', 1, 'PTM-207894582123', 5000000, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_item`
--

CREATE TABLE `sales_order_item` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales_order_item`
--

INSERT INTO `sales_order_item` (`id`, `sales_order_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `unit_price`, `subtotal`) VALUES
(16, 12, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 1000, 'Roll', 5000, 5000000),
(17, 12, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 1500, 'Roll', 15000, 22500000),
(18, 13, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 1000, 'Roll', 5000, 5000000),
(19, 13, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 500, 'Roll', 15000, 7500000);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `name` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`name`, `title`, `type`, `value`) VALUES
('company_name', 'Company Name', 'text', 'PT. PRIMA GEOTEX INDO'),
('company_slogan', 'Company Slogan', 'text', 'Distributor Geotextile Indonesia | Balaraja - Indonesia'),
('contact_email', 'Email', 'text', 'primageotexindo@gmail.com'),
('contact_telp', 'Telp', 'text', '021 1234 123'),
('delivery_order_note', 'Delivery Order Note', 'text', ''),
('factory_address', 'Factory Address', 'text', 'Jln.Raya Serang Km 28\r\nBalaraja , Tangerang - Banten 15610'),
('icon', 'Icon', 'file', 'http://localhost/admin.belajarasik.com/C:\\xampp\\htdocs\\admin.belajarasik.com\\files/icon.png'),
('invoice_note', 'Invoice Note', 'text', '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>'),
('office_address', 'Office Address', 'text', 'Jln.Raya Serang Km 28\r\nBalaraja , Tangerang - Banten 15610'),
('quotation_note', 'Quotation Note', 'text', '<div>\r\nTERM &amp; CONDITIONAL : <br></div><div><ol><li>Harga Sudah Termasuk PPN 10%</li><li>Harga Free Ongkir ke BSD</li><li>Pembayaran Full Payment</li><li>Telp./Fax : (021) 5945 0128 <br></li><li>E-Mail : primageotexindo@gmail.com</li></ol><p>\r\nPembayaran dapat ditransfer via :\r\n\r\n<br></p></div><div><b>Rek : PT. PRIMA GEOTEX INDO</b> <br></div><div><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-22.</li><li>BNI - Cabang Bumi Serpong Damai<br>A/C : 04892162083.</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7\r\n\r\n</li></ol></div><br>'),
('sales_order_note', 'Sales Order Note', 'text', '');

-- --------------------------------------------------------

--
-- Table structure for table `temp_data`
--

CREATE TABLE `temp_data` (
  `id` int(11) NOT NULL,
  `username` varchar(12) NOT NULL,
  `key` varchar(50) NOT NULL,
  `data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `temp_data`
--

INSERT INTO `temp_data` (`id`, `username`, `key`, `data`) VALUES
(308, 'sa', 'sales_quotation_edit', '{\"id\":\"11\",\"quotation_id\":\"20\",\"item_id\":\"2\",\"item_code\":\"GB-X02\",\"item_name\":\"GEOBAG X02\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"qty\":\"1000\",\"unit\":\"Roll\",\"unit_price\":\"25000\",\"subtotal\":\"25000000\"}'),
(309, 'sa', 'sales_quotation_edit', '{\"id\":\"12\",\"quotation_id\":\"20\",\"item_id\":\"3\",\"item_code\":\"GB-X03\",\"item_name\":\"GEOBAG X03\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"qty\":\"1500\",\"unit\":\"Roll\",\"unit_price\":\"15000\",\"subtotal\":\"22500000\"}'),
(310, 'sa', 'sales_quotation_edit', '{\"item_id\":\"1\",\"item_code\":\"GB-X01\",\"item_name\":\"GEOBAG X01\",\"qty\":\"1000.00\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\\r\\nSudah Dipotong Sesuai Permintaan + Jasa Pasang\",\"unit\":\"Roll\",\"unit_price\":\"12000.00\"}'),
(311, 'sa', 'sales_sales_order_edit', '{\"item_id\":\"1\",\"item_code\":\"GB-X01\",\"item_name\":\"GEOBAG X01\",\"qty\":\"1000.00\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"unit\":\"Roll\",\"unit_price\":\"5000.00\"}'),
(312, 'sa', 'sales_sales_order_edit', '{\"item_id\":\"3\",\"item_code\":\"GB-X03\",\"item_name\":\"GEOBAG X03\",\"qty\":\"500.00\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"unit\":\"Roll\",\"unit_price\":\"15000.00\"}'),
(314, 'sa', 'procurement_delivery_order_edit', '{\"id\":\"33\",\"delivery_order_id\":\"11\",\"item_id\":\"1\",\"item_code\":\"GB-X01\",\"item_name\":\"GEOBAG X01\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"qty\":\"500\",\"unit\":\"Roll\",\"sales_order_item_id\":\"18\"}'),
(315, 'sa', 'finance_invoice_edit', '{\"id\":\"24\",\"invoice_id\":\"19\",\"item_id\":\"1\",\"item_code\":\"GB-X01\",\"item_name\":\"GEOBAG X01\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"qty\":\"500\",\"unit\":\"Roll\",\"unit_price\":\"5000\",\"subtotal\":\"2500000\",\"delivery_order_item_id\":\"32\",\"do_number\":\"20020002\",\"do_date\":\"2020-02-02\",\"so_number\":\"20020002\",\"so_date\":\"2020-02-02\",\"po_number\":\"PTM-207894582123\"}'),
(316, 'sa', 'finance_invoice_edit', '{\"id\":\"25\",\"invoice_id\":\"19\",\"item_id\":\"1\",\"item_code\":\"GB-X01\",\"item_name\":\"GEOBAG X01\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"qty\":\"500\",\"unit\":\"Roll\",\"unit_price\":\"5000\",\"subtotal\":\"2500000\",\"delivery_order_item_id\":\"34\",\"do_number\":\"20020003\",\"do_date\":\"2020-02-02\",\"so_number\":\"20020002\",\"so_date\":\"2020-02-02\",\"po_number\":\"PTM-207894582123\"}');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` longtext NOT NULL,
  `level` varchar(25) NOT NULL,
  `last_login` datetime NOT NULL,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `obsolete`) VALUES
(1, 'sa', 'fba44451a795873e1b7921e9e9fe8c95f408e0328cef57a385995c6117391729d469ff0fe8a1d4ec603d34e1d0a9efb18336e9f7de5769303b5f4465bf4809df9FbsxcLgJeDz0CPrYveg0NHLL/rA6vCHWkFObGFAcSk=', 'sa', '2020-02-02 07:27:16', 0),
(2, 'de', '8B3Uif1DlZyzFtc0GdbCZYx0KJjbGj/Kfwl3YUbGsPAQzRSoWhKShbWyEKRdVzZW6q+7b8agCNeEq0UOrx0rHQ==', 'de', '2019-07-17 17:52:12', 0),
(3, 'test', 'mrZFJp5qZ5Jgz0BgZ2mXjuKMLqNS1seITCaRDLb5Y/yGFnu/2SjzlOyxgipZzRj1UMqEVqhuV3Wv1zudGtudrQ==', 'sa', '2019-07-15 14:08:18', 1),
(4, 'tset', '9n58UTnp7RtwmxZwnc259dBVAqZfgH7TPZvQUV8UNc7Djphg+J1UG2n8wS28nQNYr0TOwhsM+9DSM6eLueE9og==', 'sa', '0000-00-00 00:00:00', 1),
(5, 'sdfgsdfg', '9n58UTnp7RtwmxZwnc259dBVAqZfgH7TPZvQUV8UNc7Djphg+J1UG2n8wS28nQNYr0TOwhsM+9DSM6eLueE9og==', 'sa', '2019-07-10 19:36:36', 1),
(6, 'sales', 'd95234bef7362caf343381667de451a4f25ffcebfa974238efa8da50aafcfa8719b1bba051725927b887deb779b9238f01e3c85ce3d59085214dcb9c53335a59bWWKZ8RNkc3G/sDn9vdUqYp6M1o8jNOfcbUzc7y3y6s=', 'de', '0000-00-00 00:00:00', 0),
(7, 'procurement', 'db703791d4857be2a4cd40ea212c6db5957d4575eb9fd2c709890eaa35d12339aac1b4f3b23a5f02d0cb34f6061e2cb2787b560fbd5aac39ef40f5b8b888e498zqKfAnEeXLeWBzgJ4kChPJuuLtZYiVOb6UkFhskKEZo=', 'de', '0000-00-00 00:00:00', 0),
(8, 'finance', 'c6b58ae1d5e9f098e058e09eb4c1a955c7ea24d583c30ccf4da4fc38e2a9d2d3087455e30d42dead70fc3afa1a462a21f12cd716ccc743c11cafa941203fe21dZ37hdxPsloRzcWrVhSHUABZnYL1Ref5A6KQj8lpR5gY=', 'de', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` varchar(25) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `user_id`, `menu_id`) VALUES
('1.10', 1, 10),
('1.20', 1, 20),
('1.21', 1, 21),
('1.22', 1, 22),
('1.30', 1, 30),
('1.31', 1, 31),
('1.32', 1, 32),
('1.33', 1, 33),
('1.40', 1, 40),
('1.41', 1, 41),
('1.43', 1, 43),
('1.50', 1, 50),
('1.51', 1, 51),
('1.60', 1, 60),
('2.1', 2, 1),
('2.10', 2, 10),
('2.11', 2, 11),
('2.12', 2, 12),
('2.13', 2, 13),
('2.14', 2, 14),
('2.15', 2, 15),
('2.2', 2, 2),
('2.6', 2, 6),
('2.7', 2, 7),
('2.8', 2, 8),
('2.9', 2, 9),
('6.10', 6, 10),
('6.30', 6, 30),
('6.31', 6, 31),
('6.32', 6, 32),
('6.33', 6, 33),
('7.10', 7, 10),
('7.40', 7, 40),
('7.41', 7, 41),
('7.42', 7, 42),
('8.10', 8, 10),
('8.40', 8, 40),
('8.42', 8, 42),
('8.43', 8, 43),
('8.50', 8, 50),
('8.53', 8, 53);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order` (
`id` int(11)
,`do_number` varchar(10)
,`do_date` date
,`delivery_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`vehicle_number` varchar(45)
,`vehicle_driver` varchar(45)
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`input_date` datetime
,`input_by` int(11)
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order_for_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order_for_invoice` (
`id` int(11)
,`do_number` varchar(10)
,`do_date` date
,`delivery_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`input_date` datetime
,`input_by` int(11)
,`vehicle_type` varchar(45)
,`vehicle_number` varchar(45)
,`vehicle_driver` varchar(45)
,`do_username` varchar(12)
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
,`iv_numbers` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order_item_for_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order_item_for_invoice` (
`id` int(11)
,`delivery_order_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`qty` double
,`unit` varchar(50)
,`unit_price` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice` (
`id` int(11)
,`iv_number` varchar(10)
,`iv_date` date
,`delivery_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`ppn` int(11)
,`shipping_cost` double
,`other_cost` double
,`deduction_dp` double
,`cancel` tinyint(1)
,`posted` tinyint(4)
,`sales_order_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`due_date` date
,`closed` tinyint(4)
,`iv_note` longtext
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_item_detail`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_item_detail` (
`id` int(11)
,`invoice_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`qty` double
,`unit` varchar(50)
,`unit_price` double
,`subtotal` double
,`delivery_order_item_id` int(11)
,`do_number` varchar(10)
,`do_date` date
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_payment`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_payment` (
`id` int(11)
,`iv_number` varchar(10)
,`iv_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`cancel` tinyint(1)
,`posted` tinyint(4)
,`closed` tinyint(4)
,`shipping_cost` double
,`other_cost` double
,`deduction_dp` double
,`total_ppn` double
,`total_invoice` double
,`grand_total_invoice` double
,`total_payment` double
,`total_outstanding` double
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_quotation`
-- (See below for the actual view)
--
CREATE TABLE `v_quotation` (
`id` int(11)
,`quot_number` int(10)
,`quot_date` date
,`due_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`ppn` int(11)
,`refrence` varchar(50)
,`cancel` tinyint(4)
,`quot_note` longtext
,`shipping_cost` double
,`other_cost` double
,`input_date` datetime
,`input_by` int(11)
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_order`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_order` (
`id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`delivery_date` date
,`po_number` varchar(100)
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`total_dp` double
,`billing_address` text
,`shipping_address` text
,`ppn` int(11)
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`shipping_cost` double
,`other_cost` double
,`quotation_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_order_for_delivery_order`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_order_for_delivery_order` (
`so_id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`delivery_date` date
,`po_number` varchar(100)
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`ppn` int(11)
,`total_dp` double
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`quotation_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`so_username` varchar(12)
,`sales_order_item_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`unit` varchar(50)
,`total_delivery` double
,`total_delivered` double
,`total_outstanding` double
,`do_number` varchar(10)
);

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order`
--
DROP TABLE IF EXISTS `v_delivery_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order`  AS  select `delivery_order`.`id` AS `id`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`delivery_order`.`delivery_date` AS `delivery_date`,`delivery_order`.`customer_id` AS `customer_id`,`delivery_order`.`cust_code` AS `cust_code`,`delivery_order`.`cust_name` AS `cust_name`,`delivery_order`.`telp` AS `telp`,`delivery_order`.`billing_address` AS `billing_address`,`delivery_order`.`shipping_address` AS `shipping_address`,`delivery_order`.`vehicle_number` AS `vehicle_number`,`delivery_order`.`vehicle_driver` AS `vehicle_driver`,`delivery_order`.`cancel` AS `cancel`,`delivery_order`.`posted` AS `posted`,`delivery_order`.`input_date` AS `input_date`,`delivery_order`.`input_by` AS `input_by`,`user`.`username` AS `username` from (`delivery_order` join `user` on(`user`.`id` = `delivery_order`.`input_by`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order_for_invoice`
--
DROP TABLE IF EXISTS `v_delivery_order_for_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order_for_invoice`  AS  select `delivery_order`.`id` AS `id`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`delivery_order`.`delivery_date` AS `delivery_date`,`delivery_order`.`customer_id` AS `customer_id`,`delivery_order`.`cust_code` AS `cust_code`,`delivery_order`.`cust_name` AS `cust_name`,`delivery_order`.`telp` AS `telp`,`delivery_order`.`billing_address` AS `billing_address`,`delivery_order`.`shipping_address` AS `shipping_address`,`delivery_order`.`cancel` AS `cancel`,`delivery_order`.`posted` AS `posted`,`delivery_order`.`input_date` AS `input_date`,`delivery_order`.`input_by` AS `input_by`,`delivery_order`.`vehicle_type` AS `vehicle_type`,`delivery_order`.`vehicle_number` AS `vehicle_number`,`delivery_order`.`vehicle_driver` AS `vehicle_driver`,`user`.`username` AS `do_username`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number`,ifnull(concat(`invoice`.`iv_number`),'') AS `iv_numbers` from ((((((`delivery_order_item` join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) join `user` on(`user`.`id` = `delivery_order`.`input_by`)) join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) left join `invoice_item` on(`invoice_item`.`delivery_order_item_id` = `delivery_order_item`.`id`)) left join `invoice` on(`invoice`.`id` = `invoice_item`.`invoice_id` and `invoice`.`cancel` = 0)) where `delivery_order`.`cancel` = 0 and `delivery_order`.`posted` = 1 group by `delivery_order`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order_item_for_invoice`
--
DROP TABLE IF EXISTS `v_delivery_order_item_for_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order_item_for_invoice`  AS  select `delivery_order_item`.`id` AS `id`,`delivery_order_item`.`delivery_order_id` AS `delivery_order_id`,`delivery_order_item`.`item_id` AS `item_id`,`delivery_order_item`.`item_code` AS `item_code`,`delivery_order_item`.`item_name` AS `item_name`,`delivery_order_item`.`item_description` AS `item_description`,`delivery_order_item`.`qty` AS `qty`,`delivery_order_item`.`unit` AS `unit`,`sales_order_item`.`unit_price` AS `unit_price` from (`delivery_order_item` join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice`
--
DROP TABLE IF EXISTS `v_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice`  AS  select `invoice`.`id` AS `id`,`invoice`.`iv_number` AS `iv_number`,`invoice`.`iv_date` AS `iv_date`,`invoice`.`delivery_date` AS `delivery_date`,`invoice`.`customer_id` AS `customer_id`,`invoice`.`cust_code` AS `cust_code`,`invoice`.`cust_name` AS `cust_name`,`invoice`.`npwp` AS `npwp`,`invoice`.`telp` AS `telp`,`invoice`.`billing_address` AS `billing_address`,`invoice`.`ppn` AS `ppn`,`invoice`.`shipping_cost` AS `shipping_cost`,`invoice`.`other_cost` AS `other_cost`,`invoice`.`deduction_dp` AS `deduction_dp`,`invoice`.`cancel` AS `cancel`,`invoice`.`posted` AS `posted`,`invoice`.`sales_order_id` AS `sales_order_id`,`invoice`.`input_date` AS `input_date`,`invoice`.`input_by` AS `input_by`,`invoice`.`due_date` AS `due_date`,`invoice`.`closed` AS `closed`,`invoice`.`iv_note` AS `iv_note`,`user`.`username` AS `username` from (`invoice` join `user` on(`user`.`id` = `invoice`.`input_by`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_item_detail`
--
DROP TABLE IF EXISTS `v_invoice_item_detail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_item_detail`  AS  select `invoice_item`.`id` AS `id`,`invoice_item`.`invoice_id` AS `invoice_id`,`invoice_item`.`item_id` AS `item_id`,`invoice_item`.`item_code` AS `item_code`,`invoice_item`.`item_name` AS `item_name`,`invoice_item`.`item_description` AS `item_description`,`invoice_item`.`qty` AS `qty`,`invoice_item`.`unit` AS `unit`,`invoice_item`.`unit_price` AS `unit_price`,`invoice_item`.`subtotal` AS `subtotal`,`invoice_item`.`delivery_order_item_id` AS `delivery_order_item_id`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number` from (((((`invoice_item` join `invoice` on(`invoice`.`id` = `invoice_item`.`invoice_id`)) join `delivery_order_item` on(`delivery_order_item`.`id` = `invoice_item`.`delivery_order_item_id`)) join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_payment`
--
DROP TABLE IF EXISTS `v_invoice_payment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_payment`  AS  select `invoice`.`id` AS `id`,`invoice`.`iv_number` AS `iv_number`,`invoice`.`iv_date` AS `iv_date`,`invoice`.`customer_id` AS `customer_id`,`invoice`.`cust_code` AS `cust_code`,`invoice`.`cust_name` AS `cust_name`,`invoice`.`cancel` AS `cancel`,`invoice`.`posted` AS `posted`,`invoice`.`closed` AS `closed`,`invoice`.`shipping_cost` AS `shipping_cost`,`invoice`.`other_cost` AS `other_cost`,`invoice`.`deduction_dp` AS `deduction_dp`,ifnull(`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) * (`invoice`.`ppn` / 100),0) AS `total_ppn`,`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) AS `total_invoice`,ifnull(`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) * (`invoice`.`ppn` / 100) + `invoice`.`shipping_cost` + `invoice`.`other_cost` - `invoice`.`deduction_dp`,0) + `GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) AS `grand_total_invoice`,`GET_TOTAL_PAYMENT`(`invoice`.`id`) AS `total_payment`,ifnull(`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) * (`invoice`.`ppn` / 100) + `invoice`.`shipping_cost` + `invoice`.`other_cost` - `invoice`.`deduction_dp`,0) + `GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) - `GET_TOTAL_PAYMENT`(`invoice`.`id`) AS `total_outstanding`,`user`.`username` AS `username` from (((`invoice` left join `invoice_item` on(`invoice_item`.`invoice_id` = `invoice`.`id`)) left join `invoice_payment` on(`invoice_payment`.`invoice_id` = `invoice`.`id`)) join `user` on(`user`.`id` = `invoice`.`input_by`)) group by `invoice`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_quotation`
--
DROP TABLE IF EXISTS `v_quotation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_quotation`  AS  select `quotation`.`id` AS `id`,`quotation`.`quot_number` AS `quot_number`,`quotation`.`quot_date` AS `quot_date`,`quotation`.`due_date` AS `due_date`,`quotation`.`customer_id` AS `customer_id`,`quotation`.`cust_code` AS `cust_code`,`quotation`.`cust_name` AS `cust_name`,`quotation`.`npwp` AS `npwp`,`quotation`.`telp` AS `telp`,`quotation`.`billing_address` AS `billing_address`,`quotation`.`shipping_address` AS `shipping_address`,`quotation`.`ppn` AS `ppn`,`quotation`.`refrence` AS `refrence`,`quotation`.`cancel` AS `cancel`,`quotation`.`quot_note` AS `quot_note`,`quotation`.`shipping_cost` AS `shipping_cost`,`quotation`.`other_cost` AS `other_cost`,`quotation`.`input_date` AS `input_date`,`quotation`.`input_by` AS `input_by`,`user`.`username` AS `username` from (`quotation` join `user` on(`user`.`id` = `quotation`.`input_by`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_order`
--
DROP TABLE IF EXISTS `v_sales_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_order`  AS  select `sales_order`.`id` AS `id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`delivery_date` AS `delivery_date`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`customer_id` AS `customer_id`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order`.`npwp` AS `npwp`,`sales_order`.`telp` AS `telp`,`sales_order`.`total_dp` AS `total_dp`,`sales_order`.`billing_address` AS `billing_address`,`sales_order`.`shipping_address` AS `shipping_address`,`sales_order`.`ppn` AS `ppn`,`sales_order`.`cancel` AS `cancel`,`sales_order`.`posted` AS `posted`,`sales_order`.`shipping_cost` AS `shipping_cost`,`sales_order`.`other_cost` AS `other_cost`,`sales_order`.`quotation_id` AS `quotation_id`,`sales_order`.`input_date` AS `input_date`,`sales_order`.`input_by` AS `input_by`,`user`.`username` AS `username` from (`sales_order` join `user` on(`user`.`id` = `sales_order`.`input_by`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_order_for_delivery_order`
--
DROP TABLE IF EXISTS `v_sales_order_for_delivery_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_order_for_delivery_order`  AS  select `sales_order`.`id` AS `so_id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`delivery_date` AS `delivery_date`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`customer_id` AS `customer_id`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order`.`npwp` AS `npwp`,`sales_order`.`telp` AS `telp`,`sales_order`.`billing_address` AS `billing_address`,`sales_order`.`shipping_address` AS `shipping_address`,`sales_order`.`ppn` AS `ppn`,`sales_order`.`total_dp` AS `total_dp`,`sales_order`.`cancel` AS `cancel`,`sales_order`.`posted` AS `posted`,`sales_order`.`quotation_id` AS `quotation_id`,`sales_order`.`input_date` AS `input_date`,`sales_order`.`input_by` AS `input_by`,`user`.`username` AS `so_username`,`sales_order_item`.`id` AS `sales_order_item_id`,`sales_order_item`.`item_id` AS `item_id`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,`sales_order_item`.`item_description` AS `item_description`,`sales_order_item`.`unit` AS `unit`,ifnull(sum(`sales_order_item`.`qty`),0) AS `total_delivery`,ifnull(sum(`delivery_order_item`.`qty`),0) AS `total_delivered`,ifnull(sum(`sales_order_item`.`qty`),0) - ifnull(sum(`delivery_order_item`.`qty`),0) AS `total_outstanding`,ifnull(concat(`delivery_order`.`do_number`),'') AS `do_number` from ((((`sales_order_item` join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id` and `sales_order`.`cancel` = 0 and `sales_order`.`posted` = 1)) join `user` on(`user`.`id` = `sales_order`.`input_by`)) left join `delivery_order_item` on(`delivery_order_item`.`sales_order_item_id` = `sales_order_item`.`id`)) left join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id` and `delivery_order`.`cancel` = 0)) group by `sales_order_item`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cust_code` (`cust_code`);

--
-- Indexes for table `delivery_order`
--
ALTER TABLE `delivery_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `do_number` (`do_number`);

--
-- Indexes for table `delivery_order_item`
--
ALTER TABLE `delivery_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_number` (`iv_number`);

--
-- Indexes for table `invoice_item`
--
ALTER TABLE `invoice_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_payment`
--
ALTER TABLE `invoice_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_code` (`item_code`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `quotation_number` (`quot_number`);

--
-- Indexes for table `quotation_item`
--
ALTER TABLE `quotation_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `quotation_number` (`so_number`);

--
-- Indexes for table `sales_order_item`
--
ALTER TABLE `sales_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `temp_data`
--
ALTER TABLE `temp_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `delivery_order`
--
ALTER TABLE `delivery_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `delivery_order_item`
--
ALTER TABLE `delivery_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `invoice_item`
--
ALTER TABLE `invoice_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `invoice_payment`
--
ALTER TABLE `invoice_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `quotation_item`
--
ALTER TABLE `quotation_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sales_order_item`
--
ALTER TABLE `sales_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `temp_data`
--
ALTER TABLE `temp_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=317;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
