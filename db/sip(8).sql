-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2020 at 02:23 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sip`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `rep_item_support` ()  BEGIN
DECLARE v_total_item INT DEFAULT 0;
DECLARE v_total_tr INT DEFAULT 0;
DECLARE v_counter INT UNSIGNED DEFAULT 0;

SELECT COUNT(id) INTO v_total_tr FROM sales_order WHERE cancel = 0;
SELECT COUNT(id) INTO v_total_item FROM item;

select 
	GROUP_CONCAT(item_code) AS items,
	SUM(subtotal) AS nilai_penjualan,
    COUNT(DISTINCT sales_order_id) AS total_transaksi_yang_mengandung,
    v_total_tr AS total_transaksi,
	COUNT(DISTINCT sales_order_id) / v_total_tr AS nilai_support,
	COUNT(DISTINCT item_id) AS size
from
	sales_order_item
GROUP BY 
	sales_order_id;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `GET_TOTAL_ITEM_INVOICE` (`p_invoice_id` INT) RETURNS DOUBLE BEGIN
DECLARE res DOUBLE DEFAULT 0;

SELECT IFNULL(SUM((qty * unit_price)), 0) INTO res FROM invoice_item WHERE invoice_id = p_invoice_id;

RETURN res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `GET_TOTAL_PAYMENT` (`p_invoice_id` INT) RETURNS DOUBLE BEGIN
DECLARE res DOUBLE DEFAULT 0;

SELECT IFNULL(SUM(amount), 0) INTO res FROM invoice_payment WHERE invoice_id = p_invoice_id AND cancel = 0;

RETURN res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `GET_TOTAL_SALES_AMOUNT` (`p_item_id` INT) RETURNS DOUBLE BEGIN
DECLARE res DOUBLE DEFAULT 0;

SELECT IFNULL(SUM(qty * unit_price), 0) INTO res FROM invoice_item 
INNER JOIN invoice ON invoice.id = invoice_item.invoice_id
WHERE 
invoice.cancel = 0
AND 
invoice.posted = 1
AND
invoice.closed = 1
AND invoice_item.item_id = p_item_id
;

RETURN res;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `MENU_ACCESS` (`p_user_id` INT, `p_menu_id` INT) RETURNS INT(11) BEGIN
DECLARE _res boolean default false;

SELECT 
CASE WHEN (COUNT(id)>=1) THEN TRUE ELSE FALSE END INTO _res
FROM user_menu 
WHERE
user_id = p_user_id AND menu_id = p_menu_id;

RETURN _res;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `default_billing_address` text NOT NULL,
  `default_shipping_address` text NOT NULL,
  `obsolete` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `cust_code`, `cust_name`, `npwp`, `telp`, `default_billing_address`, `default_shipping_address`, `obsolete`, `input_date`, `input_by`) VALUES
(1, 'C0001', 'PT. TORABIKA EKA SEMESTA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(4, 'C0003', 'PT. INDAH KARGO', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(5, 'C0004', 'PT. BUKALAPAK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(6, 'C0005', 'PT. INDOMOBI', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(7, 'C0006', 'PT. LAUTAN STELL', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(9, 'C0007', 'PT. INDAH MAYA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(10, 'C0008', 'PT. PERTAMINA, TBK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(11, 'C0009', 'PT. AQUA INDONESIA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(12, 'C0010', 'PT. UNILEVER INDONESIA (BSD)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1),
(13, 'C0011', 'PT. UNILEVER INDONESIA (BOGOR)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, '2020-01-12 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order`
--

CREATE TABLE `delivery_order` (
  `id` int(11) NOT NULL,
  `do_number` varchar(10) NOT NULL,
  `do_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `vehicle_type` varchar(45) DEFAULT NULL,
  `vehicle_number` varchar(45) DEFAULT NULL,
  `vehicle_driver` varchar(45) DEFAULT NULL,
  `sales_order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery_order`
--

INSERT INTO `delivery_order` (`id`, `do_number`, `do_date`, `delivery_date`, `customer_id`, `cust_code`, `cust_name`, `telp`, `billing_address`, `shipping_address`, `cancel`, `posted`, `input_date`, `input_by`, `vehicle_type`, `vehicle_number`, `vehicle_driver`, `sales_order_id`) VALUES
(1, '20020001', '2020-02-15', '0000-00-00', 11, 'C0009', 'PT. AQUA INDONESIA', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:44:33', 1, NULL, 'B 2020 NEW', 'Bpk Ahmad (0812 1010 2020)', 1),
(2, '20020002', '2020-02-15', '0000-00-00', 11, 'C0009', 'PT. AQUA INDONESIA', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:45:06', 1, NULL, 'B 2021 NEW', 'Bpk. Ahmad AB', 1),
(3, '20020003', '2020-02-19', '0000-00-00', 10, 'C0008', 'PT. PERTAMINA, TBK', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:45:52', 1, NULL, 'B 2022 NEW', 'Bpk Ahmad (0812 1010 2020)', 2),
(4, '20020004', '2020-02-19', '0000-00-00', 1, 'C0001', 'PT. TORABIKA EKA SEMESTA', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:46:25', 1, NULL, 'B 2020 NEW', 'Bpk Ahmad (0812 1010 2020)', 3),
(5, '20020005', '2020-02-15', '0000-00-00', 11, 'C0009', 'PT. AQUA INDONESIA', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:46:56', 1, NULL, 'B 2021 NEW', 'Bpk Ahmad (0812 1010 2020)', 5),
(6, '20020006', '2020-02-15', '0000-00-00', 3, 'C0002', 'PT. MAYORA INDAH', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:47:24', 1, NULL, 'B 2022 NEW', 'Bpk. Ahmad AB', 4),
(7, '20020007', '2020-02-15', '0000-00-00', 3, 'C0002', 'PT. MAYORA INDAH', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:47:45', 1, NULL, 'B 2021 NEW', 'Bpk A.', 4),
(8, '20020008', '2020-02-15', '0000-00-00', 13, 'C0011', 'PT. UNILEVER INDONESIA (BOGOR)', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:48:28', 1, NULL, 'B 2022 NEW', 'Bpk Ahmad (0812 1010 2020)', 6),
(9, '20020009', '2020-02-15', '0000-00-00', 7, 'C0006', 'PT. LAUTAN STELL', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:49:02', 1, NULL, 'B 2020 NEW', 'Bpk Ahmad (0812 1010 2020)', 7),
(10, '20020010', '2020-02-15', '0000-00-00', 7, 'C0006', 'PT. LAUTAN STELL', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:49:35', 1, NULL, 'B 2020 NEW', 'Bpk. Ahmad AB', 7),
(11, '20020011', '2020-02-15', '0000-00-00', 5, 'C0004', 'PT. BUKALAPAK', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 1, '2020-02-15 05:50:39', 1, NULL, 'B 2020 NEW', 'Bpk A.', 8),
(12, '20020012', '2020-02-18', '0000-00-00', 9, 'C0007', 'PT. INDAH MAYA', '021 123456789', NULL, 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 0, '2020-02-15 07:47:26', 1, NULL, 'B 2021 NEW', 'Bpk. Widodo', 9);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_order_item`
--

CREATE TABLE `delivery_order_item` (
  `id` int(11) NOT NULL,
  `delivery_order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `sales_order_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `delivery_order_item`
--

INSERT INTO `delivery_order_item` (`id`, `delivery_order_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `sales_order_item_id`) VALUES
(2, 1, 6, 'GB-X06', 'GEOBAG BESAR', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 3 mtr', 500, 'pcs', 1),
(4, 2, 4, 'GB-X04', 'GEOBAG Sedang', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 1 mtr', 100, 'pcs', 2),
(8, 3, 6, 'GB-X06', 'GEOBAG BESAR', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 3 mtr', 500, 'pcs', 3),
(9, 3, 4, 'GB-X04', 'GEOBAG Sedang', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 1 mtr', 100, 'pcs', 4),
(10, 3, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1 m', 5000, 'pcs', 5),
(12, 4, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 50 x 75 cm', 15000, 'pcs', 6),
(14, 5, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.5 m', 8000, 'pcs', 9),
(16, 6, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 45 x 60 cm', 7500, 'pcs', 7),
(18, 7, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 50 x 40 cm', 8000, 'pcs', 8),
(20, 8, 6, 'GB-X06', 'GEOBAG X06', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1.4 x 2.4 mtr', 5000, 'pcs', 10),
(23, 9, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 35 x 60 cm', 5000, 'pcs', 11),
(24, 9, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 40 x 80 cm', 3000, 'pcs', 12),
(28, 10, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.5 m', 5000, 'pcs', 14),
(29, 10, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 75 x 120 cm', 10000, 'pcs', 13),
(30, 10, 5, 'GB-X05', 'GEOBAG X05', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 2.2 m', 8000, 'pcs', 15),
(32, 11, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 40 x 70 cm', 20000, 'pcs', 16),
(34, 12, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.3 m', 2500, 'pcs', 17);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `iv_number` varchar(10) NOT NULL,
  `iv_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(4) NOT NULL DEFAULT 0,
  `sales_order_id` int(11) NOT NULL,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `closed` tinyint(4) NOT NULL DEFAULT 0,
  `iv_note` longtext DEFAULT NULL,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0,
  `deduction_dp` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `iv_number`, `iv_date`, `delivery_date`, `customer_id`, `cust_code`, `cust_name`, `npwp`, `telp`, `billing_address`, `ppn`, `cancel`, `posted`, `sales_order_id`, `input_date`, `input_by`, `due_date`, `closed`, `iv_note`, `shipping_cost`, `other_cost`, `deduction_dp`) VALUES
(1, '20020001', '2020-02-15', '0000-00-00', 11, 'C0009', 'PT. AQUA INDONESIA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 1, '2020-02-15 05:53:15', 1, '2020-02-22', 1, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 5000000, 0, 0),
(2, '20020002', '2020-02-15', '0000-00-00', 10, 'C0008', 'PT. PERTAMINA, TBK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 2, '2020-02-15 05:54:13', 1, '2020-02-22', 1, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 30000000),
(3, '20020003', '2020-02-15', '0000-00-00', 1, 'C0001', 'PT. TORABIKA EKA SEMESTA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 3, '2020-02-15 05:54:43', 1, '0000-00-00', 1, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 50000000),
(4, '20020004', '2020-02-15', '0000-00-00', 3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 4, '2020-02-15 05:56:25', 1, '0000-00-00', 1, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 20000000),
(5, '20020005', '2020-02-15', '0000-00-00', 11, 'C0009', 'PT. AQUA INDONESIA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 5, '2020-02-15 05:56:39', 1, '0000-00-00', 1, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 50000000),
(6, '20020006', '2020-02-15', '0000-00-00', 13, 'C0011', 'PT. UNILEVER INDONESIA (BOGOR)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 6, '2020-02-15 05:56:51', 1, '0000-00-00', 0, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 100000000),
(7, '20020007', '2020-02-15', '0000-00-00', 13, 'C0011', 'PT. UNILEVER INDONESIA (BOGOR)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 1, 0, 6, '2020-02-15 05:57:06', 1, '0000-00-00', 0, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 100000000),
(8, '20020008', '2020-02-15', '0000-00-00', 7, 'C0006', 'PT. LAUTAN STELL', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 7, '2020-02-15 05:57:49', 1, '2020-02-22', 0, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 0, 0, 500000000),
(9, '20020009', '2020-02-15', '0000-00-00', 5, 'C0004', 'PT. BUKALAPAK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 8, '2020-02-15 05:58:12', 1, '2020-02-17', 0, '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7 <br></li></ol><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>', 15000000, 0, 10000000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_item`
--

CREATE TABLE `invoice_item` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL,
  `delivery_order_item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_item`
--

INSERT INTO `invoice_item` (`id`, `invoice_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `unit_price`, `subtotal`, `delivery_order_item_id`) VALUES
(1, 1, 6, 'GB-X06', 'GEOBAG BESAR', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 3 mtr', 500, 'pcs', 150000, 75000000, 2),
(2, 1, 4, 'GB-X04', 'GEOBAG Sedang', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 1 mtr', 100, 'pcs', 100000, 10000000, 4),
(3, 2, 6, 'GB-X06', 'GEOBAG BESAR', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 3 mtr', 500, 'pcs', 150000, 75000000, 8),
(4, 2, 4, 'GB-X04', 'GEOBAG Sedang', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 1 mtr', 100, 'pcs', 100000, 10000000, 9),
(5, 2, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1 m', 5000, 'pcs', 50000, 250000000, 10),
(6, 3, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 50 x 75 cm', 15000, 'pcs', 35000, 525000000, 12),
(7, 4, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 45 x 60 cm', 7500, 'pcs', 25000, 187500000, 16),
(8, 4, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 50 x 40 cm', 8000, 'pcs', 20000, 160000000, 18),
(9, 5, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.5 m', 8000, 'pcs', 95000, 760000000, 14),
(10, 6, 6, 'GB-X06', 'GEOBAG X06', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1.4 x 2.4 mtr', 5000, 'pcs', 180000, 900000000, 20),
(11, 7, 6, 'GB-X06', 'GEOBAG X06', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1.4 x 2.4 mtr', 5000, 'pcs', 180000, 900000000, 20),
(12, 8, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 35 x 60 cm', 5000, 'pcs', 15000, 75000000, 23),
(13, 8, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 40 x 80 cm', 3000, 'pcs', 25000, 75000000, 24),
(14, 8, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.5 m', 5000, 'pcs', 100000, 500000000, 28),
(15, 8, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 75 x 120 cm', 10000, 'pcs', 55000, 550000000, 29),
(16, 8, 5, 'GB-X05', 'GEOBAG X05', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 2.2 m', 8000, 'pcs', 150000, 1200000000, 30),
(17, 9, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 40 x 70 cm', 20000, 'pcs', 25000, 500000000, 32);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payment`
--

CREATE TABLE `invoice_payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_type` varchar(45) NOT NULL,
  `payment_proof` varchar(100) NOT NULL,
  `amount` double NOT NULL,
  `cancel` tinyint(4) DEFAULT 0,
  `input_date` datetime DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  `remark` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `invoice_payment`
--

INSERT INTO `invoice_payment` (`id`, `invoice_id`, `payment_date`, `payment_type`, `payment_proof`, `amount`, `cancel`, `input_date`, `input_by`, `remark`) VALUES
(7, 26, '2020-02-28', 'TF', 'TF211321', 122650000, 0, '2020-02-03 00:00:00', 1, 'Test'),
(8, 27, '2020-02-22', 'TF', 'TF', 13400000, 0, '2020-02-03 00:00:00', 1, ''),
(9, 1, '2020-02-18', 'Trf', 'Trf Mandiri', 98500000, 0, '2020-02-15 00:00:00', 1, 'Lunas Semua'),
(10, 2, '2020-02-18', 'Trf ke BRI', 'BRI-002', 338500000, 0, '2020-02-15 00:00:00', 1, 'Lunas Semua'),
(11, 3, '2020-02-16', 'Trf ke BRI', 'BRI-003', 527500000, 0, '2020-02-15 00:00:00', 1, ''),
(12, 4, '2020-02-18', 'Trf ke BRI', 'BRI-004', 362250000, 0, '2020-02-15 00:00:00', 1, ''),
(13, 5, '2020-02-16', 'Trf ke BRI', 'BRI-005', 786000000, 0, '2020-02-15 00:00:00', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `unit_price` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `obsolete` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `item_code`, `item_name`, `item_description`, `unit_price`, `unit`, `obsolete`, `input_date`, `input_by`) VALUES
(1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 5000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 25000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 15000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 78000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(5, 'GB-X05', 'GEOBAG X05', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 104000, 'Roll', 0, '2020-01-12 00:32:41', 1),
(6, 'GB-X06', 'GEOBAG X06', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 25800, 'Roll', 0, '2020-01-12 00:32:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(3) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `map` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `sort_number` int(11) NOT NULL,
  `parent_id` int(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `name`, `uri`, `map`, `icon`, `sort_number`, `parent_id`) VALUES
(10, 'Dashboard', 'dashboard', 'dashboard', 'dashboard', 'fa fa-dashboard fa-fw', 1, 0),
(20, 'Setting', 'setting', '#', 'setting', 'fa fa-cogs fa-fw', 2, 0),
(21, 'Umum', 'general_setting', 'setting/general_setting', 'setting.general_setting', 'fa fa-circle-o', 1, 20),
(22, 'User', 'user', 'setting/user', 'setting.user', 'fa fa-circle-o', 2, 20),
(30, 'Sales', 'sales', '#', 'sales', 'fa fa-shopping-cart fa-fw', 3, 0),
(31, 'Customer', 'customer', 'sales/customer', 'sales.customer', 'fa fa-circle-o', 1, 30),
(32, 'Quotation', 'quotation', 'sales/quotation', 'sales.quotation', 'fa fa-circle-o', 2, 30),
(33, 'Sales Order', 'sales_order', 'sales/sales_order', 'sales.sales_order', 'fa fa-circle-o', 3, 30),
(40, 'Procurement', 'procurement', '#', 'procurement', 'fa fa-building fa-fw', 4, 0),
(41, 'Item', 'item', 'procurement/item', 'procurement.item', 'fa fa-circle-o', 1, 40),
(42, 'Supplier', 'supplier', 'procurement/supplier', 'procurement.supplier', 'fa fa-circle-o', 2, 40),
(43, 'Purchase Order', 'purchase_order', 'procurement/purchase_order', 'procurement.purchase_order', 'fa fa-circle-o', 3, 40),
(44, 'Delivery Order', 'delivery_order', 'procurement/delivery_order', 'procurement.delivery_order', 'fa fa-circle-o', 4, 40),
(50, 'Finance', 'finance', '#', 'finance', 'fa fa-money fa-fw', 5, 0),
(51, 'Invoice', 'invoice', 'finance/invoice', 'finance.invoice', 'fa fa-circle-o', 1, 50),
(52, 'Payment', 'payment', 'finance/payment', 'finance.payment', 'fa fa-circle-o', 2, 50),
(60, 'Report', 'report', '#', 'report', 'fa fa-table fa-fw', 6, 0),
(61, 'Item Sales', 'rep_item', 'report/rep_item', 'report.rep_item', 'fa fa-circle-o', 1, 60),
(62, 'Datamining', 'rep_datamining', 'report/rep_datamining', 'report.rep_datamining', 'fa fa-circle-o', 2, 60),
(63, 'Outsanding', 'rep_sales_order', 'report/rep_sales_order', 'report.rep_sales_order', 'fa fa-circle-o', 3, 60);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `po_number` varchar(10) NOT NULL,
  `po_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supp_code` varchar(5) NOT NULL,
  `supp_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(1) NOT NULL DEFAULT 0,
  `sales_order_id` int(11) NOT NULL,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `total_dp` double DEFAULT 0,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0,
  `po_note` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `po_date`, `delivery_date`, `supplier_id`, `supp_code`, `supp_name`, `npwp`, `telp`, `address`, `ppn`, `cancel`, `posted`, `sales_order_id`, `input_date`, `input_by`, `total_dp`, `shipping_cost`, `other_cost`, `po_note`) VALUES
(10, '20020001', '2020-02-23', '0000-00-00', 14, 'SP001', 'Jumbo Power International', '123.12421.15', '(021) 2314636', 'Jl. Lautze No.22-K, Ps. Baru, Kecamatan Sawah Besar, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10710', 10, 0, 0, 1, '2020-02-23 03:44:17', 1, 0, 500, 0, '<p>\r\n</p><div>\r\nTERM &amp; CONDITIONAL : <br></div><div><br></div><div><ol><li>Harap Melakukan Pengiriman Tepat Waktu sesuai PO yang di Kirim</li><li>Setiap Pengiriman harus menyertakan Faktur dan Surat Jalan sesuai dengan PO</li><li>Semua Pembayaran akan dilakukan secara Transfer<br></li></ol></div><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_item`
--

CREATE TABLE `purchase_order_item` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchase_order_item`
--

INSERT INTO `purchase_order_item` (`id`, `purchase_order_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `unit_price`, `subtotal`) VALUES
(21, 10, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran Roll   : 4 x 100m', 12, 'Roll', 15000, 180000);

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `quot_number` int(10) NOT NULL,
  `quot_date` date NOT NULL,
  `due_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text NOT NULL,
  `shipping_address` text NOT NULL,
  `ppn` int(11) NOT NULL,
  `refrence` varchar(50) NOT NULL,
  `cancel` tinyint(4) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `quot_note` longtext DEFAULT NULL,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_item`
--

CREATE TABLE `quotation_item` (
  `id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `id` int(11) NOT NULL,
  `so_number` varchar(10) NOT NULL,
  `so_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_code` varchar(5) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `billing_address` text DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `ppn` int(11) NOT NULL,
  `cancel` tinyint(1) NOT NULL DEFAULT 0,
  `posted` tinyint(1) NOT NULL DEFAULT 0,
  `quotation_id` int(11) NOT NULL,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL,
  `po_number` varchar(100) DEFAULT NULL,
  `total_dp` double DEFAULT 0,
  `shipping_cost` double DEFAULT 0,
  `other_cost` double DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales_order`
--

INSERT INTO `sales_order` (`id`, `so_number`, `so_date`, `delivery_date`, `customer_id`, `cust_code`, `cust_name`, `npwp`, `telp`, `billing_address`, `shipping_address`, `ppn`, `cancel`, `posted`, `quotation_id`, `input_date`, `input_by`, `po_number`, `total_dp`, `shipping_cost`, `other_cost`) VALUES
(1, '20020001', '2020-02-15', '2020-02-17', 11, 'C0009', 'PT. AQUA INDONESIA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 0, 0, 1, 0, '2020-02-15 05:24:49', 1, 'AQUA/PO-001/II/2020', 0, 0, 0),
(2, '20020002', '2020-02-15', '2020-02-20', 10, 'C0008', 'PT. PERTAMINA, TBK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:30:08', 1, 'PRT/PO-001/II/2020', 30000000, 0, 0),
(3, '20020003', '2020-02-15', '2020-02-18', 1, 'C0001', 'PT. TORABIKA EKA SEMESTA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:31:33', 1, 'TES/PO-001/II/2020', 50000000, 0, 0),
(4, '20020004', '2020-02-15', '2020-02-22', 3, 'C0002', 'PT. MAYORA INDAH', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:33:40', 1, 'MYI/PO-001/II/2020', 20000000, 0, 0),
(5, '20020005', '2020-02-15', '2020-02-25', 11, 'C0009', 'PT. AQUA INDONESIA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:34:59', 1, 'AQUA/PO-002/II/2020', 50000000, 0, 0),
(6, '20020006', '2020-02-15', '2020-02-16', 13, 'C0011', 'PT. UNILEVER INDONESIA (BOGOR)', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:37:11', 1, 'UIB/PO-001/II/2020', 100000000, 0, 0),
(7, '20020007', '2020-02-15', '2020-02-25', 7, 'C0006', 'PT. LAUTAN STELL', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:41:50', 1, 'LST/PO-001/II/2020', 500000000, 0, 0),
(8, '20020008', '2020-02-15', '2020-02-16', 5, 'C0004', 'PT. BUKALAPAK', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 05:43:37', 1, 'BL/PO-001/II/2020', 10000000, 15000000, 0),
(9, '20020009', '2020-02-15', '2020-02-18', 9, 'C0007', 'PT. INDAH MAYA', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 0, 1, 0, '2020-02-15 07:45:59', 1, 'IDHM/PO-001/II/2020', 56000000, 3500000, 0),
(10, '20020010', '2020-03-23', '2020-03-28', 4, 'C0003', 'PT. INDAH KARGO', '99.999.999.9-999.999', '021 123456789', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 'Jalan Raya Serang KM.12,5, Bitung Jaya Village, Cikupa, Sukadamai, Cikupa, Sukadamai, Kec. Cikupa, Tangerang, Banten 15710', 10, 1, 0, 0, '2020-02-29 11:04:42', 1, '1658651', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_item`
--

CREATE TABLE `sales_order_item` (
  `id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_code` varchar(8) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` text NOT NULL,
  `qty` double NOT NULL,
  `unit` varchar(50) NOT NULL,
  `unit_price` double NOT NULL,
  `subtotal` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales_order_item`
--

INSERT INTO `sales_order_item` (`id`, `sales_order_id`, `item_id`, `item_code`, `item_name`, `item_description`, `qty`, `unit`, `unit_price`, `subtotal`) VALUES
(1, 1, 6, 'GB-X06', 'GEOBAG BESAR', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 3 mtr', 500, 'pcs', 150000, 75000000),
(2, 1, 4, 'GB-X04', 'GEOBAG Sedang', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 1 mtr', 100, 'pcs', 100000, 10000000),
(3, 2, 6, 'GB-X06', 'GEOBAG BESAR', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 3 mtr', 500, 'pcs', 150000, 75000000),
(4, 2, 4, 'GB-X04', 'GEOBAG Sedang', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 1 mtr', 100, 'pcs', 100000, 10000000),
(5, 2, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1 m', 5000, 'pcs', 50000, 250000000),
(6, 3, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 50 x 75 cm', 15000, 'pcs', 35000, 525000000),
(7, 4, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 45 x 60 cm', 7500, 'pcs', 25000, 187500000),
(8, 4, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 50 x 40 cm', 8000, 'pcs', 20000, 160000000),
(9, 5, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.5 m', 8000, 'pcs', 95000, 760000000),
(10, 6, 6, 'GB-X06', 'GEOBAG X06', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1.4 x 2.4 mtr', 5000, 'pcs', 180000, 900000000),
(11, 7, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 35 x 60 cm', 5000, 'pcs', 15000, 75000000),
(12, 7, 2, 'GB-X02', 'GEOBAG X02', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 40 x 80 cm', 3000, 'pcs', 25000, 75000000),
(13, 7, 3, 'GB-X03', 'GEOBAG X03', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 75 x 120 cm', 10000, 'pcs', 55000, 550000000),
(14, 7, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.5 m', 5000, 'pcs', 100000, 500000000),
(15, 7, 5, 'GB-X05', 'GEOBAG X05', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 2 x 2.2 m', 8000, 'pcs', 150000, 1200000000),
(16, 8, 1, 'GB-X01', 'GEOBAG X01', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 40 x 70 cm', 20000, 'pcs', 25000, 500000000),
(17, 9, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.3 m', 2500, 'pcs', 110000, 275000000),
(19, 10, 4, 'GB-X04', 'GEOBAG X04', 'Material        : Polyester\r\nColour          : White\r\nUkuran : 1 x 1.3 m', 2500, 'pcs', 110000, 275000000);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `name` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`name`, `title`, `type`, `value`) VALUES
('company_name', 'Company Name', 'text', 'PT. PRIMA GEOTEX INDO'),
('company_slogan', 'Company Slogan', 'text', 'Distributor Geotextile Indonesia | Balaraja - Indonesia'),
('contact_email', 'Email', 'text', 'primageotexindo@gmail.com'),
('contact_telp', 'Telp', 'text', '021 1234 12345'),
('delivery_order_note', 'Delivery Order Note', 'text', ''),
('factory_address', 'Factory Address', 'text', 'Jln.Raya Serang Km 28\r\nBalaraja , Tangerang - Banten 15610'),
('icon', 'Icon', 'file', 'http://localhost/admin.belajarasik.com/C:\\xampp\\htdocs\\admin.belajarasik.com\\files/icon.png'),
('invoice_note', 'Invoice Note', 'text', '<p><b>\r\nPayment Info :</b></p><p>Rekening PT. PRIMA GEOTEX INDO <br></p><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-2</li><li>BNI-Cabang Bumi Serpong Damai<br>A/C : 0489216208</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7&nbsp;</li></ol><br><p>\r\nIf you have any questions about this invoice, please contact Muhammad Firman (081281039202). <br><i>Thank You For Your Business!\r\n\r\n</i><br></p><br><br>'),
('office_address', 'Office Address', 'text', 'Jln.Raya Serang Km 28\r\nBalaraja , Tangerang - Banten 15610'),
('purchase_order_note', 'Purchase Order Note', 'text', '<p>\r\n</p><div>\r\nTERM &amp; CONDITIONAL : <br></div><div><ol><li>Harga Sudah Termasuk PPN 10%</li><li>Harga Free Ongkir ke BSD</li><li>Pembayaran Full Payment</li><li>Telp./Fax : (021) 5945 0128 <br></li><li>E-Mail : primageotexindo@gmail.com</li></ol><p>\r\nPembayaran dapat ditransfer via :\r\n\r\n<br></p></div><div><b>Rek : PT. PRIMA GEOTEX INDO</b> <br></div><div><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-22.</li><li>BNI - Cabang Bumi Serpong Damai<br>A/C : 04892162083.</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7\r\n\r\n</li></ol></div><br>\r\n\r\n<br><p></p>'),
('quotation_note', 'Quotation Note', 'text', '<div>\r\nTERM &amp; CONDITIONAL : <br></div><div><ol><li>Harga Sudah Termasuk PPN 10%</li><li>Harga Free Ongkir ke BSD</li><li>Pembayaran Full Payment</li><li>Telp./Fax : (021) 5945 0128 <br></li><li>E-Mail : primageotexindo@gmail.com</li></ol><p>\r\nPembayaran dapat ditransfer via :\r\n\r\n<br></p></div><div><b>Rek : PT. PRIMA GEOTEX INDO</b> <br></div><div><ol><li>MANDIRI - KCP Tangerang Balaraja<br>A/C : 176-00-0108517-22.</li><li>BNI - Cabang Bumi Serpong Damai<br>A/C : 04892162083.</li><li>BRI - KC BALARAJA<br>A/C : 0437-01-001546-56-7\r\n\r\n</li></ol></div><br>'),
('sales_order_note', 'Sales Order Note', 'text', '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supp_code` varchar(5) NOT NULL,
  `supp_name` varchar(100) NOT NULL,
  `npwp` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `obsolete` tinyint(1) NOT NULL DEFAULT 0,
  `input_date` datetime NOT NULL,
  `input_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `supp_code`, `supp_name`, `npwp`, `telp`, `address`, `obsolete`, `input_date`, `input_by`) VALUES
(14, 'SP001', 'Jumbo Power International', '123.12421.15', '(021) 2314636', 'Jl. Lautze No.22-K, Ps. Baru, Kecamatan Sawah Besar, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10710', 0, '2020-02-23 01:42:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `temp_data`
--

CREATE TABLE `temp_data` (
  `id` int(11) NOT NULL,
  `username` varchar(12) NOT NULL,
  `key` varchar(50) NOT NULL,
  `data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `temp_data`
--

INSERT INTO `temp_data` (`id`, `username`, `key`, `data`) VALUES
(438, 'sa', 'procurement_purchase_order_edit', '{\"id\":\"21\",\"purchase_order_id\":\"10\",\"item_id\":\"3\",\"item_code\":\"GB-X03\",\"item_name\":\"GEOBAG X03\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran Roll   : 4 x 100m\",\"qty\":\"12\",\"unit\":\"Roll\",\"unit_price\":\"15000\",\"subtotal\":\"180000\"}'),
(439, 'sa', 'sales_sales_order_edit', '{\"id\":\"18\",\"sales_order_id\":\"10\",\"item_id\":\"4\",\"item_code\":\"GB-X04\",\"item_name\":\"GEOBAG X04\",\"item_description\":\"Material        : Polyester\\r\\nColour          : White\\r\\nUkuran : 1 x 1.3 m\",\"qty\":\"2500\",\"unit\":\"pcs\",\"unit_price\":\"110000\",\"subtotal\":\"275000000\"}');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_rep_item_support`
--

CREATE TABLE `tmp_rep_item_support` (
  `id` int(5) UNSIGNED NOT NULL,
  `items` text DEFAULT NULL,
  `size` int(11) NOT NULL,
  `frequent` int(11) DEFAULT NULL,
  `support` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` longtext NOT NULL,
  `level` varchar(25) NOT NULL,
  `last_login` datetime NOT NULL,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `obsolete`) VALUES
(1, 'sa', 'fba44451a795873e1b7921e9e9fe8c95f408e0328cef57a385995c6117391729d469ff0fe8a1d4ec603d34e1d0a9efb18336e9f7de5769303b5f4465bf4809df9FbsxcLgJeDz0CPrYveg0NHLL/rA6vCHWkFObGFAcSk=', 'sa', '2020-03-05 13:53:28', 0),
(2, 'de', '8B3Uif1DlZyzFtc0GdbCZYx0KJjbGj/Kfwl3YUbGsPAQzRSoWhKShbWyEKRdVzZW6q+7b8agCNeEq0UOrx0rHQ==', 'de', '2019-07-17 17:52:12', 0),
(3, 'test', 'mrZFJp5qZ5Jgz0BgZ2mXjuKMLqNS1seITCaRDLb5Y/yGFnu/2SjzlOyxgipZzRj1UMqEVqhuV3Wv1zudGtudrQ==', 'sa', '2019-07-15 14:08:18', 1),
(4, 'tset', '9n58UTnp7RtwmxZwnc259dBVAqZfgH7TPZvQUV8UNc7Djphg+J1UG2n8wS28nQNYr0TOwhsM+9DSM6eLueE9og==', 'sa', '0000-00-00 00:00:00', 1),
(5, 'sdfgsdfg', '9n58UTnp7RtwmxZwnc259dBVAqZfgH7TPZvQUV8UNc7Djphg+J1UG2n8wS28nQNYr0TOwhsM+9DSM6eLueE9og==', 'sa', '2019-07-10 19:36:36', 1),
(6, 'sales', 'd95234bef7362caf343381667de451a4f25ffcebfa974238efa8da50aafcfa8719b1bba051725927b887deb779b9238f01e3c85ce3d59085214dcb9c53335a59bWWKZ8RNkc3G/sDn9vdUqYp6M1o8jNOfcbUzc7y3y6s=', 'de', '0000-00-00 00:00:00', 0),
(7, 'procurement', 'db703791d4857be2a4cd40ea212c6db5957d4575eb9fd2c709890eaa35d12339aac1b4f3b23a5f02d0cb34f6061e2cb2787b560fbd5aac39ef40f5b8b888e498zqKfAnEeXLeWBzgJ4kChPJuuLtZYiVOb6UkFhskKEZo=', 'de', '0000-00-00 00:00:00', 0),
(8, 'finance', 'c6b58ae1d5e9f098e058e09eb4c1a955c7ea24d583c30ccf4da4fc38e2a9d2d3087455e30d42dead70fc3afa1a462a21f12cd716ccc743c11cafa941203fe21dZ37hdxPsloRzcWrVhSHUABZnYL1Ref5A6KQj8lpR5gY=', 'de', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` varchar(25) NOT NULL,
  `user_id` int(11) NOT NULL,
  `menu_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `user_id`, `menu_id`) VALUES
('1.10', 1, 10),
('1.20', 1, 20),
('1.21', 1, 21),
('1.22', 1, 22),
('1.30', 1, 30),
('1.31', 1, 31),
('1.32', 1, 32),
('1.33', 1, 33),
('1.40', 1, 40),
('1.41', 1, 41),
('1.43', 1, 43),
('1.50', 1, 50),
('1.51', 1, 51),
('1.60', 1, 60),
('2.1', 2, 1),
('2.10', 2, 10),
('2.11', 2, 11),
('2.12', 2, 12),
('2.13', 2, 13),
('2.14', 2, 14),
('2.15', 2, 15),
('2.2', 2, 2),
('2.6', 2, 6),
('2.7', 2, 7),
('2.8', 2, 8),
('2.9', 2, 9),
('6.10', 6, 10),
('6.30', 6, 30),
('6.31', 6, 31),
('6.32', 6, 32),
('6.33', 6, 33),
('7.10', 7, 10),
('7.40', 7, 40),
('7.41', 7, 41),
('7.42', 7, 42),
('8.10', 8, 10),
('8.40', 8, 40),
('8.42', 8, 42),
('8.43', 8, 43),
('8.50', 8, 50),
('8.53', 8, 53);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_datamining`
-- (See below for the actual view)
--
CREATE TABLE `v_datamining` (
`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`so_cancel` tinyint(1)
,`so_posted` tinyint(1)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`so_qty` double
,`so_unit` varchar(50)
,`so_unit_price` double
,`do_number` varchar(10)
,`do_date` date
,`do_cancel` tinyint(1)
,`do_posted` tinyint(1)
,`do_shipping_address` text
,`vehicle_number` varchar(45)
,`vehicle_driver` varchar(45)
,`do_qty` double
,`iv_number` varchar(10)
,`iv_date` date
,`iv_cancel` tinyint(1)
,`iv_posted` tinyint(4)
,`iv_billing_address` text
,`payment_date` date
,`payment_type` varchar(45)
,`payment_proof` varchar(100)
,`payment_amount` double
,`payment_remark` varchar(45)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order` (
`id` int(11)
,`do_number` varchar(10)
,`do_date` date
,`delivery_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`vehicle_number` varchar(45)
,`vehicle_driver` varchar(45)
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`input_date` datetime
,`input_by` int(11)
,`username` varchar(12)
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order_for_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order_for_invoice` (
`id` int(11)
,`do_number` varchar(10)
,`do_date` date
,`delivery_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`input_date` datetime
,`input_by` int(11)
,`vehicle_type` varchar(45)
,`vehicle_number` varchar(45)
,`vehicle_driver` varchar(45)
,`do_username` varchar(12)
,`sales_order_id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
,`iv_numbers` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order_item_for_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order_item_for_invoice` (
`id` int(11)
,`delivery_order_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`qty` double
,`unit` varchar(50)
,`unit_price` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_delivery_order_outstanding`
-- (See below for the actual view)
--
CREATE TABLE `v_delivery_order_outstanding` (
`so_id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`unit` varchar(50)
,`unit_price` double
,`so_qty` double
,`do_qty` double
,`so_outstanding` double
,`do_numbers` mediumtext
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice` (
`id` int(11)
,`iv_number` varchar(10)
,`iv_date` date
,`delivery_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`ppn` int(11)
,`shipping_cost` double
,`other_cost` double
,`deduction_dp` double
,`cancel` tinyint(1)
,`posted` tinyint(4)
,`sales_order_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`due_date` date
,`closed` tinyint(4)
,`iv_note` longtext
,`username` varchar(12)
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_item_detail`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_item_detail` (
`id` int(11)
,`invoice_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`qty` double
,`unit` varchar(50)
,`unit_price` double
,`subtotal` double
,`delivery_order_item_id` int(11)
,`do_number` varchar(10)
,`do_date` date
,`so_number` varchar(10)
,`so_date` date
,`po_number` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_outstanding`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_outstanding` (
`so_number` varchar(10)
,`so_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`po_number` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`do_number` varchar(10)
,`do_date` date
,`do_shipping_address` text
,`vehicle_number` varchar(45)
,`vehicle_driver` varchar(45)
,`do_qty` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_payment`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_payment` (
`id` int(11)
,`iv_number` varchar(10)
,`iv_date` date
,`so_number` varchar(10)
,`so_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`cancel` tinyint(1)
,`posted` tinyint(4)
,`closed` tinyint(4)
,`shipping_cost` double
,`other_cost` double
,`deduction_dp` double
,`total_ppn` double
,`total_invoice` double
,`grand_total_invoice` double
,`total_payment` double
,`total_outstanding` double
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_sales`
-- (See below for the actual view)
--
CREATE TABLE `v_item_sales` (
`id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`total_qty` double
,`total_transaction` bigint(21)
,`frequent` bigint(21)
,`so_year` int(4)
,`so_month` int(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_sales2`
-- (See below for the actual view)
--
CREATE TABLE `v_item_sales2` (
`sales_order_id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`item_id` mediumtext
,`item_code` mediumtext
,`item_name` mediumtext
,`frequent` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_sales_dataset`
-- (See below for the actual view)
--
CREATE TABLE `v_item_sales_dataset` (
`id` int(11)
,`sales_order_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`qty` double
,`unit` varchar(50)
,`unit_price` double
,`subtotal` double
,`so_number` varchar(10)
,`so_date` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_item_sales_frequent`
-- (See below for the actual view)
--
CREATE TABLE `v_item_sales_frequent` (
`id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`frequent` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_purchase_order`
-- (See below for the actual view)
--
CREATE TABLE `v_purchase_order` (
`id` int(11)
,`po_number` varchar(10)
,`po_date` date
,`delivery_date` date
,`supplier_id` int(11)
,`supp_code` varchar(5)
,`supp_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`address` text
,`ppn` int(11)
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`sales_order_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`total_dp` double
,`shipping_cost` double
,`other_cost` double
,`po_note` longtext
,`username` varchar(12)
,`so_number` varchar(10)
,`so_date` date
,`cust_code` varchar(5)
,`cust_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_quotation`
-- (See below for the actual view)
--
CREATE TABLE `v_quotation` (
`id` int(11)
,`quot_number` int(10)
,`quot_date` date
,`due_date` date
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`ppn` int(11)
,`refrence` varchar(50)
,`cancel` tinyint(4)
,`quot_note` longtext
,`shipping_cost` double
,`other_cost` double
,`input_date` datetime
,`input_by` int(11)
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_item3`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_item3` (
`item_id` mediumtext
,`item_code` mediumtext
,`item_name` mediumtext
,`size` bigint(21)
,`frequent` bigint(21)
,`support` decimal(24,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_order`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_order` (
`id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`delivery_date` date
,`po_number` varchar(100)
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`total_dp` double
,`billing_address` text
,`shipping_address` text
,`ppn` int(11)
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`shipping_cost` double
,`other_cost` double
,`quotation_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`username` varchar(12)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_order_for_delivery_order`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_order_for_delivery_order` (
`id` int(11)
,`sales_order_item_id` int(11)
,`sales_order_id` int(11)
,`item_id` int(11)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`item_description` text
,`unit` varchar(50)
,`so_number` varchar(10)
,`so_date` date
,`delivery_date` date
,`po_number` varchar(100)
,`customer_id` int(11)
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`npwp` varchar(100)
,`telp` varchar(100)
,`billing_address` text
,`shipping_address` text
,`ppn` int(11)
,`total_dp` double
,`cancel` tinyint(1)
,`posted` tinyint(1)
,`quotation_id` int(11)
,`input_date` datetime
,`input_by` int(11)
,`so_username` varchar(12)
,`total_delivery` double
,`total_delivered` double
,`total_outstanding` double
,`do_number` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_order_outstanding`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_order_outstanding` (
`so_id` int(11)
,`so_number` varchar(10)
,`so_date` date
,`cust_code` varchar(5)
,`cust_name` varchar(100)
,`item_code` varchar(8)
,`item_name` varchar(255)
,`so_qty` double
,`do_qty` double
,`so_outstanding` double
,`do_numbers` mediumtext
);

-- --------------------------------------------------------

--
-- Structure for view `v_datamining`
--
DROP TABLE IF EXISTS `v_datamining`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_datamining`  AS  select `sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`customer_id` AS `customer_id`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order`.`npwp` AS `npwp`,`sales_order`.`telp` AS `telp`,`sales_order`.`cancel` AS `so_cancel`,`sales_order`.`posted` AS `so_posted`,`sales_order_item`.`item_id` AS `item_id`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,`sales_order_item`.`item_description` AS `item_description`,`sales_order_item`.`qty` AS `so_qty`,`sales_order_item`.`unit` AS `so_unit`,`sales_order_item`.`unit_price` AS `so_unit_price`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`delivery_order`.`cancel` AS `do_cancel`,`delivery_order`.`posted` AS `do_posted`,`delivery_order`.`shipping_address` AS `do_shipping_address`,`delivery_order`.`vehicle_number` AS `vehicle_number`,`delivery_order`.`vehicle_driver` AS `vehicle_driver`,`delivery_order_item`.`qty` AS `do_qty`,`invoice`.`iv_number` AS `iv_number`,`invoice`.`iv_date` AS `iv_date`,`invoice`.`cancel` AS `iv_cancel`,`invoice`.`posted` AS `iv_posted`,`invoice`.`billing_address` AS `iv_billing_address`,`invoice_payment`.`payment_date` AS `payment_date`,`invoice_payment`.`payment_type` AS `payment_type`,`invoice_payment`.`payment_proof` AS `payment_proof`,`invoice_payment`.`amount` AS `payment_amount`,`invoice_payment`.`remark` AS `payment_remark` from ((((((`sales_order_item` left join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) left join `delivery_order_item` on(`delivery_order_item`.`sales_order_item_id` = `sales_order_item`.`id`)) left join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) left join `invoice_item` on(`invoice_item`.`delivery_order_item_id` = `delivery_order_item`.`id`)) left join `invoice` on(`invoice`.`id` = `invoice_item`.`invoice_id`)) left join `invoice_payment` on(`invoice_payment`.`invoice_id` = `invoice`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order`
--
DROP TABLE IF EXISTS `v_delivery_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order`  AS  select `delivery_order`.`id` AS `id`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`delivery_order`.`delivery_date` AS `delivery_date`,`delivery_order`.`customer_id` AS `customer_id`,`delivery_order`.`cust_code` AS `cust_code`,`delivery_order`.`cust_name` AS `cust_name`,`delivery_order`.`telp` AS `telp`,`delivery_order`.`billing_address` AS `billing_address`,`delivery_order`.`shipping_address` AS `shipping_address`,`delivery_order`.`vehicle_number` AS `vehicle_number`,`delivery_order`.`vehicle_driver` AS `vehicle_driver`,`delivery_order`.`cancel` AS `cancel`,`delivery_order`.`posted` AS `posted`,`delivery_order`.`input_date` AS `input_date`,`delivery_order`.`input_by` AS `input_by`,`user`.`username` AS `username`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number` from ((`delivery_order` join `user` on(`user`.`id` = `delivery_order`.`input_by`)) join `sales_order` on(`sales_order`.`id` = `delivery_order`.`sales_order_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order_for_invoice`
--
DROP TABLE IF EXISTS `v_delivery_order_for_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order_for_invoice`  AS  select `delivery_order`.`id` AS `id`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`delivery_order`.`delivery_date` AS `delivery_date`,`delivery_order`.`customer_id` AS `customer_id`,`delivery_order`.`cust_code` AS `cust_code`,`delivery_order`.`cust_name` AS `cust_name`,`delivery_order`.`telp` AS `telp`,`delivery_order`.`billing_address` AS `billing_address`,`delivery_order`.`shipping_address` AS `shipping_address`,`delivery_order`.`cancel` AS `cancel`,`delivery_order`.`posted` AS `posted`,`delivery_order`.`input_date` AS `input_date`,`delivery_order`.`input_by` AS `input_by`,`delivery_order`.`vehicle_type` AS `vehicle_type`,`delivery_order`.`vehicle_number` AS `vehicle_number`,`delivery_order`.`vehicle_driver` AS `vehicle_driver`,`user`.`username` AS `do_username`,`sales_order`.`id` AS `sales_order_id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number`,ifnull(concat(`invoice`.`iv_number`),'') AS `iv_numbers` from (((((`delivery_order_item` join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) join `user` on(`user`.`id` = `delivery_order`.`input_by`)) join `sales_order` on(`sales_order`.`id` = `delivery_order`.`sales_order_id`)) left join `invoice_item` on(`invoice_item`.`delivery_order_item_id` = `delivery_order_item`.`id`)) left join `invoice` on(`invoice`.`id` = `sales_order`.`id` and `invoice`.`cancel` = 0)) where `delivery_order`.`cancel` = 0 and `delivery_order`.`posted` = 1 group by `delivery_order`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order_item_for_invoice`
--
DROP TABLE IF EXISTS `v_delivery_order_item_for_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order_item_for_invoice`  AS  select `delivery_order_item`.`id` AS `id`,`delivery_order_item`.`delivery_order_id` AS `delivery_order_id`,`delivery_order_item`.`item_id` AS `item_id`,`delivery_order_item`.`item_code` AS `item_code`,`delivery_order_item`.`item_name` AS `item_name`,`delivery_order_item`.`item_description` AS `item_description`,`delivery_order_item`.`qty` AS `qty`,`delivery_order_item`.`unit` AS `unit`,`sales_order_item`.`unit_price` AS `unit_price` from (`delivery_order_item` join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_delivery_order_outstanding`
--
DROP TABLE IF EXISTS `v_delivery_order_outstanding`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_delivery_order_outstanding`  AS  select `sales_order`.`id` AS `so_id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,`sales_order_item`.`item_description` AS `item_description`,`sales_order_item`.`unit` AS `unit`,`sales_order_item`.`unit_price` AS `unit_price`,sum(`sales_order_item`.`qty`) AS `so_qty`,sum(`delivery_order_item`.`qty`) AS `do_qty`,sum(`sales_order_item`.`qty`) - sum(`delivery_order_item`.`qty`) AS `so_outstanding`,group_concat(`delivery_order`.`do_number` separator ',') AS `do_numbers` from (((`delivery_order_item` join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) where `sales_order`.`cancel` = 0 and `sales_order`.`posted` = 1 group by `sales_order`.`id`,`sales_order_item`.`id` having `so_outstanding` > 0 ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice`
--
DROP TABLE IF EXISTS `v_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice`  AS  select `invoice`.`id` AS `id`,`invoice`.`iv_number` AS `iv_number`,`invoice`.`iv_date` AS `iv_date`,`invoice`.`delivery_date` AS `delivery_date`,`invoice`.`customer_id` AS `customer_id`,`invoice`.`cust_code` AS `cust_code`,`invoice`.`cust_name` AS `cust_name`,`invoice`.`npwp` AS `npwp`,`invoice`.`telp` AS `telp`,`invoice`.`billing_address` AS `billing_address`,`invoice`.`ppn` AS `ppn`,`invoice`.`shipping_cost` AS `shipping_cost`,`invoice`.`other_cost` AS `other_cost`,`invoice`.`deduction_dp` AS `deduction_dp`,`invoice`.`cancel` AS `cancel`,`invoice`.`posted` AS `posted`,`invoice`.`sales_order_id` AS `sales_order_id`,`invoice`.`input_date` AS `input_date`,`invoice`.`input_by` AS `input_by`,`invoice`.`due_date` AS `due_date`,`invoice`.`closed` AS `closed`,`invoice`.`iv_note` AS `iv_note`,`user`.`username` AS `username`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number` from ((`invoice` join `user` on(`user`.`id` = `invoice`.`input_by`)) join `sales_order` on(`sales_order`.`id` = `invoice`.`sales_order_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_item_detail`
--
DROP TABLE IF EXISTS `v_invoice_item_detail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_item_detail`  AS  select `invoice_item`.`id` AS `id`,`invoice_item`.`invoice_id` AS `invoice_id`,`invoice_item`.`item_id` AS `item_id`,`invoice_item`.`item_code` AS `item_code`,`invoice_item`.`item_name` AS `item_name`,`invoice_item`.`item_description` AS `item_description`,`invoice_item`.`qty` AS `qty`,`invoice_item`.`unit` AS `unit`,`invoice_item`.`unit_price` AS `unit_price`,`invoice_item`.`subtotal` AS `subtotal`,`invoice_item`.`delivery_order_item_id` AS `delivery_order_item_id`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`po_number` AS `po_number` from (((((`invoice_item` join `invoice` on(`invoice`.`id` = `invoice_item`.`invoice_id`)) join `delivery_order_item` on(`delivery_order_item`.`id` = `invoice_item`.`delivery_order_item_id`)) join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_outstanding`
--
DROP TABLE IF EXISTS `v_invoice_outstanding`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_outstanding`  AS  select `sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`customer_id` AS `customer_id`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`npwp` AS `npwp`,`sales_order`.`telp` AS `telp`,`sales_order`.`cancel` AS `cancel`,`sales_order`.`posted` AS `posted`,`sales_order_item`.`item_id` AS `item_id`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,`sales_order_item`.`item_description` AS `item_description`,`delivery_order`.`do_number` AS `do_number`,`delivery_order`.`do_date` AS `do_date`,`delivery_order`.`shipping_address` AS `do_shipping_address`,`delivery_order`.`vehicle_number` AS `vehicle_number`,`delivery_order`.`vehicle_driver` AS `vehicle_driver`,`delivery_order_item`.`qty` AS `do_qty` from (((((`delivery_order_item` left join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) left join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) left join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) left join `invoice_item` on(`invoice_item`.`delivery_order_item_id` = `delivery_order_item`.`id`)) left join `invoice` on(`invoice`.`id` = `invoice_item`.`invoice_id`)) where `invoice_item`.`id` is null and `sales_order`.`cancel` = 0 and `sales_order`.`posted` = 1 and `delivery_order`.`cancel` = 0 and `delivery_order`.`posted` = 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_payment`
--
DROP TABLE IF EXISTS `v_invoice_payment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_payment`  AS  select `invoice`.`id` AS `id`,`invoice`.`iv_number` AS `iv_number`,`invoice`.`iv_date` AS `iv_date`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`invoice`.`customer_id` AS `customer_id`,`invoice`.`cust_code` AS `cust_code`,`invoice`.`cust_name` AS `cust_name`,`invoice`.`cancel` AS `cancel`,`invoice`.`posted` AS `posted`,`invoice`.`closed` AS `closed`,`invoice`.`shipping_cost` AS `shipping_cost`,`invoice`.`other_cost` AS `other_cost`,`invoice`.`deduction_dp` AS `deduction_dp`,ifnull(`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) * (`invoice`.`ppn` / 100),0) AS `total_ppn`,`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) AS `total_invoice`,ifnull(`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) * (`invoice`.`ppn` / 100) + `invoice`.`shipping_cost` + `invoice`.`other_cost` - `invoice`.`deduction_dp`,0) + `GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) AS `grand_total_invoice`,`GET_TOTAL_PAYMENT`(`invoice`.`id`) AS `total_payment`,ifnull(`GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) * (`invoice`.`ppn` / 100) + `invoice`.`shipping_cost` + `invoice`.`other_cost` - `invoice`.`deduction_dp`,0) + `GET_TOTAL_ITEM_INVOICE`(`invoice`.`id`) - `GET_TOTAL_PAYMENT`(`invoice`.`id`) AS `total_outstanding`,`user`.`username` AS `username` from ((((`invoice` left join `invoice_item` on(`invoice_item`.`invoice_id` = `invoice`.`id`)) left join `sales_order` on(`sales_order`.`id` = `invoice`.`sales_order_id`)) left join `invoice_payment` on(`invoice_payment`.`invoice_id` = `invoice`.`id`)) join `user` on(`user`.`id` = `invoice`.`input_by`)) group by `invoice`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_item_sales`
--
DROP TABLE IF EXISTS `v_item_sales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_sales`  AS  select `item`.`id` AS `id`,`item`.`item_code` AS `item_code`,`item`.`item_name` AS `item_name`,sum(`sales_order_item`.`qty`) AS `total_qty`,count(distinct `sales_order_item`.`sales_order_id`) AS `total_transaction`,count(distinct `sales_order_item`.`sales_order_id`) AS `frequent`,year(`sales_order`.`so_date`) AS `so_year`,month(`sales_order`.`so_date`) AS `so_month` from ((`item` left join `sales_order_item` on(`sales_order_item`.`item_id` = `item`.`id`)) left join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id` and `sales_order`.`posted` = 1)) group by `item`.`id`,year(`sales_order`.`so_date`),month(`sales_order`.`so_date`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_item_sales2`
--
DROP TABLE IF EXISTS `v_item_sales2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_sales2`  AS  select `sales_order`.`id` AS `sales_order_id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,group_concat(`sales_order_item`.`item_id` separator ',') AS `item_id`,group_concat(`sales_order_item`.`item_code` separator ',') AS `item_code`,group_concat(`sales_order_item`.`item_name` separator ',') AS `item_name`,count(distinct `sales_order_item`.`sales_order_id`) AS `frequent` from (`sales_order` left join `sales_order_item` on(`sales_order_item`.`sales_order_id` = `sales_order`.`id`)) where `sales_order`.`posted` = 1 and `sales_order`.`cancel` = 0 group by `sales_order`.`id` order by `sales_order_item`.`item_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_item_sales_dataset`
--
DROP TABLE IF EXISTS `v_item_sales_dataset`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_sales_dataset`  AS  select `sales_order_item`.`id` AS `id`,`sales_order_item`.`sales_order_id` AS `sales_order_id`,`sales_order_item`.`item_id` AS `item_id`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,`sales_order_item`.`item_description` AS `item_description`,`sales_order_item`.`qty` AS `qty`,`sales_order_item`.`unit` AS `unit`,`sales_order_item`.`unit_price` AS `unit_price`,`sales_order_item`.`subtotal` AS `subtotal`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date` from (`sales_order_item` join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id` and `sales_order`.`posted` = 1)) where `sales_order`.`cancel` = 0 ;

-- --------------------------------------------------------

--
-- Structure for view `v_item_sales_frequent`
--
DROP TABLE IF EXISTS `v_item_sales_frequent`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_item_sales_frequent`  AS  select `item`.`id` AS `id`,`item`.`item_code` AS `item_code`,`item`.`item_name` AS `item_name`,count(distinct `sales_order_item`.`sales_order_id`) AS `frequent` from (`item` left join `sales_order_item` on(`sales_order_item`.`item_id` = `item`.`id`)) group by `item`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_purchase_order`
--
DROP TABLE IF EXISTS `v_purchase_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_purchase_order`  AS  select `purchase_order`.`id` AS `id`,`purchase_order`.`po_number` AS `po_number`,`purchase_order`.`po_date` AS `po_date`,`purchase_order`.`delivery_date` AS `delivery_date`,`purchase_order`.`supplier_id` AS `supplier_id`,`purchase_order`.`supp_code` AS `supp_code`,`purchase_order`.`supp_name` AS `supp_name`,`purchase_order`.`npwp` AS `npwp`,`purchase_order`.`telp` AS `telp`,`purchase_order`.`address` AS `address`,`purchase_order`.`ppn` AS `ppn`,`purchase_order`.`cancel` AS `cancel`,`purchase_order`.`posted` AS `posted`,`purchase_order`.`sales_order_id` AS `sales_order_id`,`purchase_order`.`input_date` AS `input_date`,`purchase_order`.`input_by` AS `input_by`,`purchase_order`.`total_dp` AS `total_dp`,`purchase_order`.`shipping_cost` AS `shipping_cost`,`purchase_order`.`other_cost` AS `other_cost`,`purchase_order`.`po_note` AS `po_note`,`user`.`username` AS `username`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name` from ((`purchase_order` join `user` on(`user`.`id` = `purchase_order`.`input_by`)) left join `sales_order` on(`sales_order`.`id` = `purchase_order`.`sales_order_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_quotation`
--
DROP TABLE IF EXISTS `v_quotation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_quotation`  AS  select `quotation`.`id` AS `id`,`quotation`.`quot_number` AS `quot_number`,`quotation`.`quot_date` AS `quot_date`,`quotation`.`due_date` AS `due_date`,`quotation`.`customer_id` AS `customer_id`,`quotation`.`cust_code` AS `cust_code`,`quotation`.`cust_name` AS `cust_name`,`quotation`.`npwp` AS `npwp`,`quotation`.`telp` AS `telp`,`quotation`.`billing_address` AS `billing_address`,`quotation`.`shipping_address` AS `shipping_address`,`quotation`.`ppn` AS `ppn`,`quotation`.`refrence` AS `refrence`,`quotation`.`cancel` AS `cancel`,`quotation`.`quot_note` AS `quot_note`,`quotation`.`shipping_cost` AS `shipping_cost`,`quotation`.`other_cost` AS `other_cost`,`quotation`.`input_date` AS `input_date`,`quotation`.`input_by` AS `input_by`,`user`.`username` AS `username` from (`quotation` join `user` on(`user`.`id` = `quotation`.`input_by`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_item3`
--
DROP TABLE IF EXISTS `v_sales_item3`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_item3`  AS  select group_concat(`v_item_sales_dataset`.`item_id` separator ',') AS `item_id`,group_concat(`v_item_sales_dataset`.`item_code` separator ',') AS `item_code`,group_concat(`v_item_sales_dataset`.`item_name` separator ',') AS `item_name`,count(`v_item_sales_dataset`.`item_id`) AS `size`,count(distinct `v_item_sales_dataset`.`sales_order_id`) AS `frequent`,count(distinct `v_item_sales_dataset`.`sales_order_id`) / (select count(0) from `sales_order`) AS `support` from `v_item_sales_dataset` group by `v_item_sales_dataset`.`sales_order_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_order`
--
DROP TABLE IF EXISTS `v_sales_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_order`  AS  select `sales_order`.`id` AS `id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`delivery_date` AS `delivery_date`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`customer_id` AS `customer_id`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order`.`npwp` AS `npwp`,`sales_order`.`telp` AS `telp`,`sales_order`.`total_dp` AS `total_dp`,`sales_order`.`billing_address` AS `billing_address`,`sales_order`.`shipping_address` AS `shipping_address`,`sales_order`.`ppn` AS `ppn`,`sales_order`.`cancel` AS `cancel`,`sales_order`.`posted` AS `posted`,`sales_order`.`shipping_cost` AS `shipping_cost`,`sales_order`.`other_cost` AS `other_cost`,`sales_order`.`quotation_id` AS `quotation_id`,`sales_order`.`input_date` AS `input_date`,`sales_order`.`input_by` AS `input_by`,`user`.`username` AS `username` from (`sales_order` join `user` on(`user`.`id` = `sales_order`.`input_by`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_order_for_delivery_order`
--
DROP TABLE IF EXISTS `v_sales_order_for_delivery_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_order_for_delivery_order`  AS  select `sales_order_item`.`id` AS `id`,`sales_order_item`.`id` AS `sales_order_item_id`,`sales_order_item`.`sales_order_id` AS `sales_order_id`,`sales_order_item`.`item_id` AS `item_id`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,`sales_order_item`.`item_description` AS `item_description`,`sales_order_item`.`unit` AS `unit`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`delivery_date` AS `delivery_date`,`sales_order`.`po_number` AS `po_number`,`sales_order`.`customer_id` AS `customer_id`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order`.`npwp` AS `npwp`,`sales_order`.`telp` AS `telp`,`sales_order`.`billing_address` AS `billing_address`,`sales_order`.`shipping_address` AS `shipping_address`,`sales_order`.`ppn` AS `ppn`,`sales_order`.`total_dp` AS `total_dp`,`sales_order`.`cancel` AS `cancel`,`sales_order`.`posted` AS `posted`,`sales_order`.`quotation_id` AS `quotation_id`,`sales_order`.`input_date` AS `input_date`,`sales_order`.`input_by` AS `input_by`,`user`.`username` AS `so_username`,ifnull(`sales_order_item`.`qty`,0) AS `total_delivery`,ifnull(sum(`delivery_order_item`.`qty`),0) AS `total_delivered`,ifnull(`sales_order_item`.`qty`,0) - ifnull(sum(`delivery_order_item`.`qty`),0) AS `total_outstanding`,ifnull(concat(`delivery_order`.`do_number`),'') AS `do_number` from ((((`sales_order_item` join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id` and `sales_order`.`cancel` = 0 and `sales_order`.`posted` = 1)) join `user` on(`user`.`id` = `sales_order`.`input_by`)) left join `delivery_order_item` on(`delivery_order_item`.`sales_order_item_id` = `sales_order_item`.`id`)) left join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id` and `delivery_order`.`cancel` = 0)) group by `sales_order_item`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_order_outstanding`
--
DROP TABLE IF EXISTS `v_sales_order_outstanding`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_order_outstanding`  AS  select `sales_order`.`id` AS `so_id`,`sales_order`.`so_number` AS `so_number`,`sales_order`.`so_date` AS `so_date`,`sales_order`.`cust_code` AS `cust_code`,`sales_order`.`cust_name` AS `cust_name`,`sales_order_item`.`item_code` AS `item_code`,`sales_order_item`.`item_name` AS `item_name`,sum(`sales_order_item`.`qty`) AS `so_qty`,sum(`delivery_order_item`.`qty`) AS `do_qty`,sum(`sales_order_item`.`qty`) - sum(`delivery_order_item`.`qty`) AS `so_outstanding`,group_concat(`delivery_order`.`do_number` separator ',') AS `do_numbers` from (((`delivery_order_item` join `delivery_order` on(`delivery_order`.`id` = `delivery_order_item`.`delivery_order_id`)) join `sales_order_item` on(`sales_order_item`.`id` = `delivery_order_item`.`sales_order_item_id`)) join `sales_order` on(`sales_order`.`id` = `sales_order_item`.`sales_order_id`)) group by `sales_order`.`id`,`sales_order_item`.`id` having `so_outstanding` > 0 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cust_code` (`cust_code`);

--
-- Indexes for table `delivery_order`
--
ALTER TABLE `delivery_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `do_number` (`do_number`);

--
-- Indexes for table `delivery_order_item`
--
ALTER TABLE `delivery_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_number` (`iv_number`);

--
-- Indexes for table `invoice_item`
--
ALTER TABLE `invoice_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_payment`
--
ALTER TABLE `invoice_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_code` (`item_code`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_item`
--
ALTER TABLE `purchase_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `quotation_number` (`quot_number`);

--
-- Indexes for table `quotation_item`
--
ALTER TABLE `quotation_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `quotation_number` (`so_number`);

--
-- Indexes for table `sales_order_item`
--
ALTER TABLE `sales_order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `supp_code` (`supp_code`);

--
-- Indexes for table `temp_data`
--
ALTER TABLE `temp_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_rep_item_support`
--
ALTER TABLE `tmp_rep_item_support`
  ADD KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `delivery_order`
--
ALTER TABLE `delivery_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `delivery_order_item`
--
ALTER TABLE `delivery_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `invoice_item`
--
ALTER TABLE `invoice_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `invoice_payment`
--
ALTER TABLE `invoice_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `purchase_order_item`
--
ALTER TABLE `purchase_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_item`
--
ALTER TABLE `quotation_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sales_order_item`
--
ALTER TABLE `sales_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `temp_data`
--
ALTER TABLE `temp_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;

--
-- AUTO_INCREMENT for table `tmp_rep_item_support`
--
ALTER TABLE `tmp_rep_item_support`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
