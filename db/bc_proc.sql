CREATE DEFINER=`root`@`localhost` PROCEDURE `rep_item_support`()
BEGIN
DECLARE v_total_item INT DEFAULT 0;
DECLARE v_total_tr INT DEFAULT 0;
DECLARE v_counter INT UNSIGNED DEFAULT 0;

SELECT COUNT(id) INTO v_total_tr FROM sales_order WHERE cancel = 0;
SELECT COUNT(id) INTO v_total_item FROM item;

-- Drop Temp Table
DROP TABLE IF EXISTS tmp_rep_item_support;

SET @dCol = '`id` INT NOT NULL AUTO_INCREMENT';
SET @tempTableItem = '';
SET @tempTableItemInsert = '';

SELECT GROUP_CONCAT(CONCAT('item_', `id`, ' INT NULL') SEPARATOR ', '), GROUP_CONCAT(CONCAT('item_', `id`) SEPARATOR ', ') INTO @tempTableItem, @tempTableItemInsert FROM item;

SET @q1 = CONCAT('CREATE TABLE `tmp_rep_item_support` (',
	'`id` INT NOT NULL AUTO_INCREMENT, ',
	'`size` INT NULL, ',
	'`support` INT NULL, ',
	@tempTableItem,
    ', PRIMARY KEY (`id`)'
')');

PREPARE stmt FROM @q1;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

SET @q1 = CONCAT('INSERT INTO `tmp_rep_item_support` (',
	'`size`, ',
	'`support`, ',
	@tempTableItemInsert,
')');

SET v_counter = 1;
WHILE v_counter <= v_total_item DO

	INSERT INTO `tmp_rep_item_support`(id, size) VALUES (v_counter, 1);

	SET v_counter = v_counter + 1;
END WHILE;



select
	GROUP_CONCAT(item_code) AS items,
	SUM(subtotal) AS nilai_penjualan,
    COUNT(DISTINCT sales_order_id) AS total_transaksi_yang_mengandung,
    v_total_tr AS total_transaksi,
	COUNT(DISTINCT sales_order_id) / v_total_tr AS nilai_support,
	COUNT(DISTINCT item_id) AS size
from
	sales_order_item
GROUP BY
	sales_order_id;
END
