
alertify.defaults.transition = "zoom";
alertify.defaults.theme.ok = "ui positive button";
alertify.defaults.theme.cancel = "ui black button";

function getHistory(doc_name, doc_id){
  $.ajax({
    url:baseUrl + 'doc_history/modal_content',
    data:{
      doc_name:doc_name,
      doc_id:doc_id
    },
    type:"post",
    success:function(response){
      alertify.dialog('alert').set({
        transition:'zoom',
        padding:false,
        height:"600px",
        maxHeight:"600px",
        resizable:true,
        title:"Riwayat Data",
        message: response
      }).show();
    }
  });
}

class LoadingSpinner{
  show(){
    var html = '<div class="lds lds-container"><div class="lds-dual-ring"></div></div>';
    $("body").append(html)
  }
  hide(){
    $(".lds").remove();
  }
}
var loadingSpinner = new LoadingSpinner();
