<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    if(!$this->input->get("alert")) set_alert("info", "Start your session.");

    $this->load->view("login");
  }

  public function validate()
  {
    // Set Session only
    $un = $this->input->post("un");
    $pw = $this->input->post("pw");

    // Check Login
    // ->check_login($username, $password);
    $check_login = $this->user_model->check_login($un, $pw);

    if($check_login)
    {
      redirect("dashboard");
    }
    else
    {
      set_alert("danger", "Auth failed, username or password wrong.");
      redirect("login?alert=error_auth_failed");
    }
  }

  public function logout()
  {
    $logout = $this->user_model->logout();

    if($logout)
    {
      $this->session->sess_destroy();
      redirect("login?alert=logout");
    }
    else
    {
      redirect("404");
    }
  }
}
