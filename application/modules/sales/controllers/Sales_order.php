<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_order extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    validate_access();

    $this->load->model("sales_order_model");
    $this->load->model("customer_model");
    $this->load->model("item_model");
    $this->load->model("temp_data_model");
  }

  public function index()
  {
    $data = array();
    $this->load->view("sales_order/index", $data);
  }

  public function add()
  {
    $id = $this->sales_order_model->insert_draft();

    redirect("sales/sales_order/edit?id=" . $id);
  }

  public function edit()
  {
    // Clear Temp
    $this->temp_data_model->delete_by_key("sales_sales_order_edit");

    $id = $this->input->get("id");

    $data["new_number"] = $this->sales_order_model->new_number();
    $data["list_customer"] = $this->customer_model->get_list_active();
    $data["list_item"] = $this->item_model->get_list_active();

    $edit = $this->sales_order_model->get_data($id);
    if($edit->posted == false)
    {
      $data["edit"] = $edit;
      // Insert Temp
      $this->temp_data_model->delete_by_key("sales_sales_order_edit");
      foreach($edit->items as $item)
      {
        $_temp_data["data"] = json_encode($item);
        $_temp_data["key"] = "sales_sales_order_edit";
        $this->temp_data_model->insert($_temp_data);
      }

      $this->load->view("sales_order/edit", $data);
    }
    else
    {
      set_alert("danger", "The document number \"".$edit->so_number."\" can't be edit, because the document has been Posted!");
      redirect("sales/sales_order");
    }

  }

  public function print_preview()
  {
    $this->load->library("dompdf_lib");
    $id = $this->input->get("id");

    $data = array();
    $res = $this->sales_order_model->get_data($id);
    $data["data"] = $res;
    $this->dompdf_lib->setPaper('A4', 'potrait');
    $this->dompdf_lib->generate('sales_order/print_preview', $data, "SO" . $res->so_number . "-".$res->cust_name.".pdf");
  }

  public function datatable()
  {
    $config["table"] = "v_sales_order";
    $config["where_all"] = [
      "cancel = 0"
    ];
    $this->datatable->generate($config);
  }

  // Action
  public function update()
  {
    $id = $this->input->post("id");
    $customer_id = $this->input->post("customer_id");

    $customer = $this->customer_model->get_data($customer_id);

    $cust_code  = $customer->cust_code;
    $cust_name  = $customer->cust_name;
    $npwp       = $customer->npwp;
    $telp       = $customer->telp;
    $billing_address = $this->input->post("billing_address");
    $shipping_address = $this->input->post("shipping_address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = get_session("uid");
    $ppn = $this->input->post("ppn");
    $total_dp = $this->input->post("total_dp");
    $so_date = $this->input->post("so_date");
    $delivery_date = $this->input->post("delivery_date");
    $po_number = $this->input->post("po_number");
    $shipping_cost = $this->input->post("shipping_cost");
    $other_cost = $this->input->post("other_cost");

    $data["customer_id"] = $customer_id;
    $data["so_date"] = $so_date;
    $data["po_number"] = $po_number;
    $data["delivery_date"] = $delivery_date;
    $data["ppn"] = unmask_money($ppn);
    $data["total_dp"] = unmask_money($total_dp);
    $data["shipping_cost"] = unmask_money($shipping_cost);
    $data["other_cost"] = unmask_money($other_cost);
    $data["cust_code"] = $cust_code;
    $data["cust_name"] = $cust_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["billing_address"] = $billing_address;
    $data["shipping_address"] = $shipping_address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->sales_order_model->update($data, $id);

    $this->sales_order_model->delete_item_by_header($id);

    $detail = $this->temp_data_model->get_by_key("sales_sales_order_edit");

    if($detail !== FALSE)
    {
      foreach($detail as $_row)
      {
        $row = json_decode($_row->data);

        $data_item["sales_order_id"] = $id;
        $data_item["item_id"] = $row->item_id;
        $data_item["item_code"] = $row->item_code;
        $data_item["item_name"] = $row->item_name;
        $data_item["item_description"] = $row->item_description;
        $data_item["qty"] = $row->qty;
        $data_item["unit"] = $row->unit;
        $data_item["unit_price"] = $row->unit_price;
        $data_item["subtotal"] = ($row->qty * $row->unit_price);

        $this->sales_order_model->insert_item($data_item);
      }
    }
    redirect("sales/sales_order");
  }

  public function cancel()
  {
    $id = $this->input->post_get("id");
    $this->sales_order_model->cancel($id);
    $response["status"] = true;
    $response["message"] = "Document Canceled!";
    json_file($response);
  }

  public function posted()
  {
    $id = $this->input->post_get("id");
    $act = $this->sales_order_model->posted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because document is canceled!";
    }
    json_file($response);
  }

  public function unposted()
  {
    $id = $this->input->post_get("id");
    $act = $this->sales_order_model->unposted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Un-posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because there is a related delivery order!";
    }
    json_file($response);
  }

  public function temp_save()
  {
    $temp_id = $this->input->post("temp_id");

    $temp_item_id = $this->input->post("temp_item_id");
    $temp_item_code = $this->input->post("temp_item_code");
    $temp_item_name = $this->input->post("temp_item_name");
    $temp_item_qty = $this->input->post("temp_item_qty");
    $temp_item_description = $this->input->post("temp_item_description");
    $temp_item_unit = $this->input->post("temp_item_unit");
    $temp_item_unit_price = $this->input->post("temp_item_unit_price");

    $temp_data["item_id"] = $temp_item_id;
    $temp_data["item_code"] = $temp_item_code;
    $temp_data["item_name"] = $temp_item_name;
    $temp_data["qty"] = unmask_money($temp_item_qty);
    $temp_data["item_description"] = $temp_item_description;
    $temp_data["unit"] = $temp_item_unit;
    $temp_data["unit_price"] = unmask_money($temp_item_unit_price);

    $data["data"] = json_encode($temp_data);
    $data["key"] = "sales_sales_order_edit";

    if($temp_id)
    {
      $res = $this->temp_data_model->update($data, $temp_id);
    }
    else
    {
      $res = $this->temp_data_model->insert($data);
    }


    if($res)
    {
      $response["status"] = TRUE;
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Failed!";
    }
    json_file($response);
  }

  public function temp_data()
  {
    $id = $this->input->post_get("id");

    $data = $this->temp_data_model->get_by_id($id);

    $response["status"] = TRUE;
    $response["id"] = $id;
    $response["data"] = json_decode($data);
    $response["message"] = "Success!";
    json_file($response);
  }

  public function temp_table()
  {
    $res = $this->temp_data_model->get_by_key("sales_sales_order_edit");
    if($res !== FALSE)
    {
      $id = $this->input->get("id");
      $edit = $this->sales_order_model->get_data($id);
      $data["edit"] = $edit;
      $data["temp_data"] = $res;
      $response["status"] = TRUE;
      $response["html"] = $this->load->view("sales/sales_order/temp_table", $data, TRUE);
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["html"] = "";
      $response["message"] = "Not Found!";
    }
    json_file($response);
  }

  public function temp_delete()
  {
    $id = $this->input->post_get("id");

    $this->temp_data_model->delete_by_id($id);

    $response["status"] = TRUE;
    $response["message"] = "Success!";
    json_file($response);
  }

}
