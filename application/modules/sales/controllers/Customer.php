<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("customer_model");
  }

  public function index()
  {
    $data["table_data"] = $this->customer_model->table_data();
    $this->load->view("customer/index", $data);
  }

  public function add()
  {
    $this->load->view("customer/add");
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->customer_model->get_data($id);
    $this->load->view("customer/edit", $data);
  }

  // Action
  public function save()
  {
    $cust_code = $this->input->post("cust_code");
    $cust_name = $this->input->post("cust_name");
    $npwp = $this->input->post("npwp");
    $telp = $this->input->post("telp");
    $default_billing_address = $this->input->post("default_billing_address");
    $default_shipping_address = $this->input->post("default_shipping_address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = $this->session->userdata("uid");

    $data["cust_code"] = $cust_code;
    $data["cust_name"] = $cust_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["default_billing_address"] = $default_billing_address;
    $data["default_shipping_address"] = $default_shipping_address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->customer_model->insert($data);
    redirect("sales/customer");
  }

  public function update()
  {
    $cust_name = $this->input->post("cust_name");
    $npwp = $this->input->post("npwp");
    $telp = $this->input->post("telp");
    $default_billing_address = $this->input->post("default_billing_address");
    $default_shipping_address = $this->input->post("default_shipping_address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = $this->session->userdata("uid");

    $id = $this->input->post("id");

    $data["cust_name"] = $cust_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["default_billing_address"] = $default_billing_address;
    $data["default_shipping_address"] = $default_shipping_address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->customer_model->update($data, $id);
    redirect("sales/customer");
  }

  public function obsolete()
  {
    $id = $this->input->get("id");
    $this->customer_model->obsolete($id);
    redirect("sales/customer");
  }

}
