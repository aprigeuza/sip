<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    validate_access();

    $this->load->model("quotation_model");
    $this->load->model("customer_model");
    $this->load->model("item_model");
    $this->load->model("temp_data_model");

  }

  public function index()
  {
    $data = array();
    $this->load->view("quotation/index", $data);
  }

  public function add()
  {
    $id = $this->quotation_model->insert_draft();

    redirect("sales/quotation/edit?id=" . $id);
  }

  public function edit()
  {
    // Clear Temp
    $this->temp_data_model->delete_by_key("sales_quotation_edit");

    $id = $this->input->get("id");

    $data["new_number"] = $this->quotation_model->new_number();
    $data["list_customer"] = $this->customer_model->get_list_active();
    $data["list_item"] = $this->item_model->get_list_active();

    $edit = $this->quotation_model->get_data($id);
    $data["edit"] = $edit;
    // Insert Temp
    $this->temp_data_model->delete_by_key("sales_quotation_edit");
    foreach($edit->items as $item)
    {
      $_temp_data["data"] = json_encode($item);
      $_temp_data["key"] = "sales_quotation_edit";
      $this->temp_data_model->insert($_temp_data);
    }

    $this->load->view("quotation/edit", $data);
  }

  public function print_preview()
  {
    $this->load->library("dompdf_lib");
    $id = $this->input->get("id");

    $data = array();
    $res = $this->quotation_model->get_data($id);
    $data["data"] = $res;
    $this->dompdf_lib->setPaper('A4', 'potrait');
    $this->dompdf_lib->generate('quotation/print_preview', $data, "IV" . $res->quot_number . "-".$res->cust_name.".pdf");
  }

  public function datatable()
  {
    $config["table"] = "v_quotation";
    $config["where_all"] = [
      "cancel = 0"
    ];
    $this->datatable->generate($config);
  }

  // Action
  public function update()
  {
    $id = $this->input->post("id");
    $customer_id = $this->input->post("customer_id");

    $customer = $this->customer_model->get_data($customer_id);

    $cust_code  = $customer->cust_code;
    $cust_name  = $customer->cust_name;
    $npwp       = $customer->npwp;
    $telp       = $customer->telp;
    $quot_note = $this->input->post("quot_note");
    $billing_address = $this->input->post("billing_address");
    $shipping_address = $this->input->post("shipping_address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = get_session("uid");
    $ppn = $this->input->post("ppn");
    $refrence = $this->input->post("refrence");
    $quot_date = $this->input->post("quot_date");
    $due_date = $this->input->post("due_date");
    $shipping_cost = $this->input->post("shipping_cost");
    $other_cost = $this->input->post("other_cost");

    $data["customer_id"] = $customer_id;
    $data["quot_date"] = $quot_date;
    $data["due_date"] = $due_date;
    $data["ppn"] = unmask_money($ppn);
    $data["refrence"] = $refrence;
    $data["cust_code"] = $cust_code;
    $data["cust_name"] = $cust_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["quot_note"] = $quot_note;
    $data["billing_address"] = $billing_address;
    $data["shipping_address"] = $shipping_address;
    $data["shipping_cost"] = unmask_money($shipping_cost);
    $data["other_cost"] = unmask_money($other_cost);
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->quotation_model->update($data, $id);

    $this->quotation_model->delete_item_by_header($id);

    $detail = $this->temp_data_model->get_by_key("sales_quotation_edit");

    if($detail !== FALSE)
    {
      foreach($detail as $_row)
      {
        $row = json_decode($_row->data);

        $data_item["quotation_id"] = $id;
        $data_item["item_id"] = $row->item_id;
        $data_item["item_code"] = $row->item_code;
        $data_item["item_name"] = $row->item_name;
        $data_item["item_description"] = $row->item_description;
        $data_item["qty"] = $row->qty;
        $data_item["unit"] = $row->unit;
        $data_item["unit_price"] = $row->unit_price;
        $data_item["subtotal"] = ($row->qty * $row->unit_price);

        $this->quotation_model->insert_item($data_item);
      }
    }
    redirect("sales/quotation");
  }

  public function cancel()
  {
    $id = $this->input->post_get("id");
    $data["cancel"] = true;
    $this->quotation_model->update($data, $id);
    $response["status"] = true;
    $response["message"] = "Document Canceled!";
    json_file($response);
  }

  public function temp_save()
  {
    $temp_id = $this->input->post("temp_id");

    $temp_item_id = $this->input->post("temp_item_id");
    $temp_item_code = $this->input->post("temp_item_code");
    $temp_item_name = $this->input->post("temp_item_name");
    $temp_item_qty = $this->input->post("temp_item_qty");
    $temp_item_description = $this->input->post("temp_item_description");
    $temp_item_unit = $this->input->post("temp_item_unit");
    $temp_item_unit_price = $this->input->post("temp_item_unit_price");

    $temp_data["item_id"] = $temp_item_id;
    $temp_data["item_code"] = $temp_item_code;
    $temp_data["item_name"] = $temp_item_name;
    $temp_data["qty"] = unmask_money($temp_item_qty);
    $temp_data["item_description"] = $temp_item_description;
    $temp_data["unit"] = $temp_item_unit;
    $temp_data["unit_price"] = unmask_money($temp_item_unit_price);

    $data["data"] = json_encode($temp_data);
    $data["key"] = "sales_quotation_edit";

    if($temp_id)
    {
      $res = $this->temp_data_model->update($data, $temp_id);
    }
    else
    {
      $res = $this->temp_data_model->insert($data);
    }


    if($res)
    {
      $response["status"] = TRUE;
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Failed!";
    }
    json_file($response);
  }

  public function temp_data()
  {
    $id = $this->input->post_get("id");

    $data = $this->temp_data_model->get_by_id($id);

    $response["status"] = TRUE;
    $response["id"] = $id;
    $response["data"] = json_decode($data);
    $response["message"] = "Success!";
    json_file($response);
  }

  public function temp_table()
  {
    $res = $this->temp_data_model->get_by_key("sales_quotation_edit");
    if($res !== FALSE)
    {
      $data["temp_data"] = $res;
      $response["status"] = TRUE;
      $response["html"] = $this->load->view("sales/quotation/temp_table", $data, TRUE);
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["html"] = "";
      $response["message"] = "Not Found!";
    }
    json_file($response);
  }

  public function temp_delete()
  {
    $id = $this->input->post_get("id");

    $this->temp_data_model->delete_by_id($id);

    $response["status"] = TRUE;
    $response["message"] = "Success!";
    json_file($response);
  }

  public function generate_sales_order()
  {
    $id = $this->input->post("id");
    $posted = $this->input->post("posted");


  }

}
