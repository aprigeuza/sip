<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Sales - Customer - Add</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customer
        <small>Sales</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-database"></i> Sales</a></li>
        <li><a href="<?php echo set_url("sales", "customer"); ?>">Customer</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <form action="<?php echo set_url("sales", "customer", "save"); ?>" method="post">

            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Add Customer</h3>

                <div class="box-tools pull-right">
                  <a href="<?php echo set_url("sales", "customer", "add"); ?>" class="btn btn-box-tool" title="Refresh"><i class="fa fa-refresh"></i></a>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Customer Code *</label>
                      <input type="text" class="form-control" id="cust_code" name="cust_code" placeholder="" autofocus required>
                    </div>
                    <div class="form-group">
                      <label>Customer Name *</label>
                      <input type="text" class="form-control" id="cust_name" name="cust_name" placeholder="" required>
                    </div>
                    <div class="form-group">
                      <label>NPWP</label>
                      <input type="text" class="form-control" id="npwp" name="npwp" placeholder="">
                    </div>
                    <div class="form-group">
                      <label>Telp</label>
                      <input type="text" class="form-control" id="telp" name="telp" placeholder="">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Default Billing Address</label>
                      <textarea class="form-control" id="default_billing_address" name="default_billing_address" rows="4" cols="20"></textarea>
                    </div>
                    <div class="form-group">
                      <label>Default Shipping Address</label>
                      <textarea class="form-control" id="default_shipping_address" name="default_shipping_address" rows="4" cols="20"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("sales", "customer"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  function validation(){

  }
  $(function () {

  })
</script>

</body>
</html>
