<div class="table-responsive">
  <table class="table table-bordered table-responsive" id="tmp_item_table">
    <thead>
      <tr>
        <th style="width:120px">#</th>
        <th style="min-width:200px">Item Description</th>
        <th style="min-width:100px">Qty</th>
        <th style="min-width:100px">Unit</th>
        <th style="min-width:100px">Unit Price</th>
        <th style="min-width:100px">Subtotal</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $subtotal_all = 0;
      if($temp_data):
      foreach ($temp_data as $_row):
        $row=json_decode($_row->data);
        $subtotal_all = $subtotal_all + ($row->qty * $row->unit_price);
        ?>
      <tr>
        <td>
          <button class="btn btn-primary" type="button" name="button" onclick="editTmpItem('<?php echo $_row->id; ?>');"><i class="fa fa-edit"></i></button>
          <button class="btn btn-danger" type="button" name="button" onclick="deleteTmpItem('<?php echo $_row->id; ?>');"><i class="fa fa-times"></i></button>
        </td>
        <td><b><?php echo $row->item_code; ?></b><br><?php echo $row->item_name; ?><br><?php echo str_replace("\n", "<br>", $row->item_description); ?></td>
        <td class="text-right"><?php echo mask_money($row->qty); ?></td>
        <td><?php echo $row->unit; ?></td>
        <td class="text-right"><?php echo mask_money($row->unit_price); ?></td>
        <td class="text-right"><?php echo mask_money(($row->qty * $row->unit_price)); ?></td>
      </tr>
      <?php endforeach;
    endif; ?>
    </tbody>
  </table>
</div>
<input type="hidden" id="temp_subtotal_all" value="<?php echo $subtotal_all; ?>">
