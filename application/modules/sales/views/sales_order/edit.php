<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Sales - Sales Order - Edit</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Order
        <small>Sales</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-shopping-cart"></i> Sales</a></li>
        <li><a href="<?php echo set_url("sales", "sales_order"); ?>">Sales Order</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <form action="<?php echo set_url("sales", "sales_order", "update"); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Sales Order</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>SO Number</label>
                      <input type="text" class="form-control" id="so_number" name="so_number" placeholder="" value="<?php echo $edit->so_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>SO Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" id="so_date" name="so_date" placeholder="" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask value="<?php  echo ($edit->so_date != "0000-00-00")? $edit->so_date : ""; ?>">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Delivery Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" id="delivery_date" name="delivery_date" placeholder="" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask value="<?php  echo ($edit->delivery_date != "0000-00-00")? $edit->delivery_date : ""; ?>">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>PO</label>
                      <input type="text" class="form-control" id="po_number" name="po_number" placeholder="" value="<?php echo $edit->po_number; ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Customer *</label>
                      <select class="form-control" name="customer_id" id="customer_id" style="width: 100%;">
                        <option value="0">Select Customer</option>
                        <?php foreach($list_customer->result() as $row): ?>
                        <option
                          value="<?php echo $row->id; ?>"
                          data-billing_address="<?php echo $row->default_billing_address; ?>"
                          data-shipping_address="<?php echo $row->default_shipping_address; ?>"
                          <?php if($edit->customer_id == $row->id) echo " selected"; ?>><?php echo $row->cust_code . " | " . $row->cust_name; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Billing Address</label>
                      <textarea class="form-control" id="billing_address" name="billing_address" rows="4" cols="20"><?php echo $edit->billing_address; ?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Shipping Address</label>
                      <textarea class="form-control" id="shipping_address" name="shipping_address" rows="4" cols="20"><?php echo $edit->shipping_address; ?></textarea>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <select class="form-control" name="tmp_items_id" id="tmp_items_id" style="width: 100%;">
                            <option value="0">Select Item</option>
                            <?php foreach($list_item->result() as $row): ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->item_code . " | " . $row->item_name; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <button type="button" name="button" class="btn btn-default" onclick="addTmpItem();"><i class="fa fa-plus"></i> Add</button>
                      </div>
                    </div>

                    <div id="temp_table"></div>


                    <div class="row">
                      <div class="col-md-6">

                      </div>
                      <div class="col-md-6">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th style="line-height: 33px;">Subtotal</th>
                              <td>
                                <input type="text" name="subtotal_all" id="subtotal_all" class="form-control text-right" value="" readonly>
                              </td>
                            </tr>
                            <tr>
                              <th class="col-md-6" style="line-height: 33px;">

                                <div class="row">
                                  <div class="col-md-6">
                                    PPN
                                  </div>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                      <input type="text" name="ppn" id="ppn" class="form-control text-right" value="<?php echo mask_money($edit->ppn); ?>">
                                      <span class="input-group-addon">%</span>
                                    </div>
                                  </div>
                                </div>
                              </th>
                              <td class="col-md-6">
                                <input type="text" name="ppn_total" id="ppn_total" class="form-control text-right" value="" readonly>
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Shipping Cost</th>
                              <td>
                                <input type="text" name="shipping_cost" id="shipping_cost" class="form-control text-right" value="<?php echo mask_money($edit->shipping_cost); ?>">
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Other Cost</th>
                              <td>
                                <input type="text" name="other_cost" id="other_cost" class="form-control text-right" value="<?php echo mask_money($edit->other_cost); ?>">
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">DP</th>
                              <td>
                                <div class="input-group">
                                  <span class="input-group-addon">(</span>
                                  <input type="text" name="total_dp" id="total_dp" class="form-control text-right" value="<?php echo mask_money($edit->total_dp); ?>">
                                  <span class="input-group-addon">)</span>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Grand Total</th>
                              <td>
                                <input type="text" name="total" id="total" class="form-control text-right" value="" readonly>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("sales", "sales_order"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<div class="modal" id="modal_define_item" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Define Item</h4>
      </div>
      <div class="modal-body">
        <form id="fm_temp_item">
          <input type="hidden" id="temp_id" name="temp_id" value="">
          <input type="hidden" id="temp_item_id" name="temp_item_id" value="">
          <div class="form-group">
            <label>Item Code</label>
            <input type="text" class="form-control" id="temp_item_code" name="temp_item_code" readonly>
          </div>
          <div class="form-group">
            <label>Item Name</label>
            <input type="text" class="form-control" id="temp_item_name" name="temp_item_name">
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" id="temp_item_description" name="temp_item_description" rows="4" cols="80"></textarea>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Qty</label>
                <input type="text" class="form-control text-right" id="temp_item_qty" name="temp_item_qty">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Unit</label>
                <input type="text" class="form-control" id="temp_item_unit" name="temp_item_unit">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Item Price</label>
            <input type="text" class="form-control text-right" id="temp_item_unit_price" name="temp_item_unit_price">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveTmpItem()">Save</button>
      </div>
    </div>
  </div>
</div>

<script>
$('#so_date').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
$('#delivery_date').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});

$(document).ready(function() {
  var $customer_id = $("#customer_id").select2()
  $customer_id.on("select2:select", function (e) {
    var billing_address = e.params.data.element.dataset.billing_address;
    var shipping_address = e.params.data.element.dataset.shipping_address;
    $("#billing_address").val(billing_address);
    $("#shipping_address").val(shipping_address);
  });

  var $tmp_items_id = $("#tmp_items_id").select2()
  $tmp_items_id.on("select2:select", function (e) {

  });
});
</script>
<script type="text/javascript">

  function getTempTable(){
    var temp_table = $("#temp_table");
    var id = '<?php echo $edit->id; ?>';
    $.ajax({
      url: '<?php echo set_url("sales", "sales_order", "temp_table"); ?>',
      type: 'GET',
      dataType: 'JSON',
      data:{id:id},
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          temp_table.html(response.html);
        } else {
          temp_table.html("");
        }

        $("#subtotal_all").numberFormat("setValue", $("#temp_subtotal_all").val());

        calculateSubtotal();

        loadingSpinner.hide();
      }
    });
  }

  function resetTemp(){
    $("#temp_id").val("");
    $("#temp_item_id").val("");
    $("#temp_item_code").val("");
    $("#temp_item_name").val("");
    $("#temp_item_qty").val("");
    $("#temp_item_description").val("");
    $("#temp_item_unit").val("");
    $("#temp_item_unit_price").val("");

    $("#temp_item_qty").numberFormat();
    $("#temp_item_unit_price").numberFormat();

    $("#fm_temp_item").find("input, textarea").focusin(function(event) {
      $(this).select();
    });

    // $("#fm_temp_item").on("keypress", function(e){
    //   if(e.keyCode==13){
    //     saveTmpItem();
    //   }
    // });
  }

  $(document).ready(function() {
    getTempTable();
    resetTemp();
  });

  function addTmpItem(){
    var tmp_items_id = $("#tmp_items_id").val();
    if(tmp_items_id == 0) return alertify.error("Select Item Please.");
    $.ajax({
      url: '<?php echo set_url("api", "get_item"); ?>',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: {id: tmp_items_id},
      success: function(response){
        if(response.status==true){
          var data = response.data;
          $("#temp_id").val(0);
          $("#temp_item_id").val(data.id);
          $("#temp_item_code").val(data.item_code);
          $("#temp_item_name").val(data.item_name);
          $("#temp_item_qty").val(0);
          $("#temp_item_description").val(data.item_description);
          $("#temp_item_unit").val(data.unit);
          $("#temp_item_unit_price").numberFormat("setValue", data.unit_price);

          $("#modal_define_item").modal("show");

          $("#tmp_items_id").select2("val", 0);
        } else {
          resetTemp();
          alertify.error(response.message);
        }

        loadingSpinner.hide();
      }
    });
  }

  function editTmpItem(id){
    $.ajax({
      url: '<?php echo set_url("sales", "sales_order", "temp_data"); ?>',
      type: 'GET',
      dataType: 'JSON',
      data: {id: id},
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          var data = response.data;
          $("#temp_id").val(response.id);
          $("#temp_item_id").val(data.item_id);
          $("#temp_item_code").val(data.item_code);
          $("#temp_item_name").val(data.item_name);
          $("#temp_item_qty").numberFormat("setValue", data.qty);
          $("#temp_item_description").val(data.item_description);
          $("#temp_item_unit").val(data.unit);
          $("#temp_item_unit_price").numberFormat("setValue", data.unit_price);

          $("#modal_define_item").modal("show");

          $("#tmp_items_id").select2("val", 0);
        } else {
          resetTemp();
          alertify.error(response.message);
        }
        loadingSpinner.hide();
      }
    });
  }

  function deleteTmpItem(id){
    if(confirm('Delete selected item?')){
      $.ajax({
        url: '<?php echo set_url("sales", "sales_order", "temp_delete"); ?>',
        type: 'GET',
        dataType: 'JSON',
        data: {id: id},
        beforeSend: function(){
          loadingSpinner.show();
        },
        success: function(response){
          if(response.status==true){
            resetTemp();
            alertify.success(response.message);
          } else {
            resetTemp();
            alertify.error(response.message);
          }
          getTempTable();
          loadingSpinner.hide();
        }
      });
    }
  }

  function saveTmpItem(){
    var fmTemp = $("#fm_temp_item");
    $.ajax({
      url: '<?php echo set_url("sales", "sales_order", "temp_save"); ?>',
      type: 'POST',
      dataType: 'JSON',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: fmTemp.serializeArray(),
      success: function(response){
        if(response.status==true){
          resetTemp();
          $("#modal_define_item").modal("hide");
          alertify.success(response.message);
        } else {
          resetTemp();
          $("#modal_define_item").modal("hide");
          alertify.error(response.message);
        }
        getTempTable();
        loadingSpinner.hide();

        $("#tmp_items_id").select2("val", 0);
      }
    });
  }

  function calculateSubtotal(){
    var subtotal_all = 0;
    var grand_total = 0;
    var ppn = 0;
    var ppn_total = 0;
    var shipping_cost = 0;
    var other_cost = 0;
    var total_dp = 0;

    if ($("#subtotal_all").length) {
      subtotal_all = $("#subtotal_all").numberFormat("getValue");
    }
    if ($("#ppn").length) {
      ppn = $("#ppn").numberFormat("getValue");
      ppn_total = (subtotal_all * (ppn/100));
    }
    if ($("#shipping_cost").length) {
      shipping_cost = $("#shipping_cost").numberFormat("getValue");
    }
    if ($("#other_cost").length) {
      other_cost = $("#other_cost").numberFormat("getValue");
    }
    if ($("#total_dp").length) {
      total_dp = $("#total_dp").numberFormat("getValue");
    }

    $("#ppn_total").numberFormat("setValue", ppn_total);

    grand_total =  ((subtotal_all + ppn_total) + (shipping_cost + other_cost)) - (total_dp);

    $("#total").numberFormat("setValue", grand_total);

    $("#ppn_total").on('focusin', function(event) { $(this).select(); });

    $("#ppn").numberFormat();
    $("#ppn_total").numberFormat();
    $("#subtotal_all").numberFormat();
    $("#shipping_cost").numberFormat();
    $("#other_cost").numberFormat();
    $("#total_dp").numberFormat();
    $("#total").numberFormat();
  }

  $("input").on('change', function(event) {
    calculateSubtotal();
  });

  $(document).ready(function() {
    calculateSubtotal();
  });
</script>

</body>
</html>
