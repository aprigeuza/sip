<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Quotation - Sales</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quotation
        <small>Sales</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-shopping-cart"></i> Sales</a></li>
        <li class="active">Quotation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <div class="row">
            <div class="col-md-12">
              <a href="<?php echo set_url("sales", "quotation", "add"); ?>" class="btn btn-primary" title="add new"><i class="fa fa-file-o"></i> Add New</a>
              <a href="<?php echo set_url("sales", "quotation"); ?>" class="btn btn-info" title="Reload"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
          </div>
          <hr class="line-separator">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table1">
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  var table1;
  $(function () {
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("sales", "quotation", "datatable"); ?>',
      "columns":[
        { "data": "quot_number", "title": "Quotation#"},
        { "data": "cust_code", "title": "Customer#"},
        { "data": "cust_name", "title": "Customer"},
        { "data": "username", "title": "Created By"},
        { "data": "input_date", "title": "Created Date"},
        {
          data: null,
          title: "Options",
          className: "text-center",
          "createdCell": function (td, cellData, rowData, row, col) {
              var html = "";
              html += '<a href="<?php echo set_url("sales", "quotation", "edit"); ?>?id='+rowData.id+'" class="btn btn-xs btn-primary">Edit</a> ';
              html += '<a href="<?php echo set_url("sales", "quotation", "print_preview"); ?>?id='+rowData.id+'" class="btn btn-xs btn-info" target="_blank">Print</a> ';
              html += '<a href="javascript:;" onclick="setCancel(\''+rowData.id+'\');" class="btn btn-xs btn-danger">Cancel</a> ';
              $(td).html(html);
          },
        }
      ],
    });
  })
</script>
<script>
  function setCancel(id){
    if(confirm('Cancel Document?')){
      $.ajax({
        url: '<?php echo set_url("sales", "quotation", "cancel"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data: {id: id},
        success: function(response){
          if(response.status == true){
            alertify.success(response.message);
          }else{
            alertify.error(response.message);
          }
          table1.ajax.reload();
        }
      });
    }
  }
</script>

</body>
</html>
