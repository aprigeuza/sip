<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    validate_access();
  }

  public function index()
  {
    $data["table_data"] = $this->user_model->get_list();

    $this->load->view("user/index", $data);
  }

  public function add()
  {
    $data=array();
    $this->load->view("user/add", $data);
  }

  public function edit()
  {
    $id = $this->input->get("id");
    if($id)
    {
      $check = $this->user_model->check_data($id);

      if($check)
      {
        $data["edit"] = $this->user_model->get_data($id);
        $data["user_menu"] = $this->user_model->user_menu($id);
        $this->load->view("user/edit", $data);
      }
      else
      {
        set_alert("danger", "Data not found!");
        redirect("setting/user");
      }
    }
  }

  public function reset()
  {
    $id = $this->input->get("id");
    if($id)
    {
      $check = $this->user_model->check_data($id);

      if($check)
      {
        $data["edit"] = $this->user_model->get_data($id);
        $this->load->view("user/reset", $data);
      }
      else
      {
        set_alert("danger", "Data not found!");
        redirect("setting/user");
      }
    }
  }

  public function save()
  {
    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('level', 'Level', 'required');

    if ($this->form_validation->run() == FALSE)
    {
      set_alert("danger", validation_errors());
      redirect("user/add");
    }
    else
    {
      $username = $this->input->post("username");
      $password = $this->input->post("password");
      $level    = $this->input->post("level");

      $check_username = $this->user_model->check_username($username);
      if(!$check_username)
      {
        $data["username"] = $username;
        $data["password"] = $this->encryption->encrypt($password);
        $data["level"] = $level;
        $insert = $this->user_model->insert($data);

        if($insert)
        {
          set_alert("success", "Berhasil menyimpan data!");
          redirect("setting/user");
        }
        else
        {
          set_alert("danger", "Tidak dapat menyimpan data!");
          redirect("setting/user");
        }
      }
      else
      {
        set_alert("danger", "Username sudah ada!");
        redirect("setting/user/add");
      }
    }
  }

  public function update()
  {
    $this->form_validation->set_rules('id', 'Id', 'required');
    $this->form_validation->set_rules('level', 'Level', 'required');

    if ($this->form_validation->run() == FALSE)
    {
      set_alert("danger", validation_errors());
      redirect("setting/user");
    }
    else
    {
      $id = $this->input->post("id");
      $level = $this->input->post("level");
      $obsolete = $this->input->post("obsolete");

      $data["level"] = $level;
      $data["obsolete"] = $obsolete;
      $update = $this->user_model->update($data, $id);

      $menu = $this->input->post("menu");
      $this->user_model->user_menu_prepare_update($id);
      foreach ($menu as $menu_id)
      {
        $this->user_model->user_menu_update($id, $menu_id);
      }

      if($update)
      {
        set_alert("success", "Berhasil menyimpan data!");
        redirect("setting/user");
      }
      else
      {
        set_alert("danger", "Tidak dapat menyimpan data!");
        redirect("setting/user");
      }
    }
  }

  public function update_password()
  {
    $this->form_validation->set_rules('id', 'Id', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('re_password', 'Re-Password', 'required');

    if ($this->form_validation->run() == FALSE)
    {
      set_alert("danger", validation_errors());
      redirect("setting/user");
    }
    else
    {
      $id = $this->input->post("id");
      $password = $this->input->post("password");
      $re_password = $this->input->post("re_password");

      if($password == $re_password)
      {
        $data["password"] = $this->encryption->encrypt($password);
        $update = $this->user_model->update($data, $id);

        $menu = $this->input->post("menu");
        $this->user_model->user_menu_prepare_update($id);
        foreach ($menu as $menu_id)
        {
          $this->user_model->user_menu_update($id, $menu_id);
        }

        if($update)
        {
          set_alert("success", "Berhasil menyimpan data!");
          redirect("setting/user");
        }
        else
        {
          set_alert("danger", "Tidak dapat menyimpan data!");
          redirect("setting/user");
        }
      }
      else
      {
        set_alert("danger", "Password tidak sama!");
        redirect("setting/user/reset?id=" . $id);
      }
    }
  }

  public function obsolete()
  {
    $id = $this->input->get("id");

    if($id)
    {
      $d = $this->user_model->get_data($id);
      $data["obsolete"] = !$d->obsolete;
      $update = $this->user_model->update($data, $id);

      if($update)
      {
        set_alert("success", "Berhasil obsolete data!");
        redirect("setting/user");
      }
      else
      {
        set_alert("danger", "Tidak dapat obsolete data!");
        redirect("setting/user");
      }
    }
    else
    {
      set_alert("danger", "Id not found!");
      redirect("setting/user");
    }
  }


  public function change_password()
  {
    $old_password = $this->input->post("old_password");
    $new_password = $this->input->post("new_password");
    $new_password2 = $this->input->post("new_password2");

    $ses_password = $this->session->userdata("pw");

    $ses_password_decrypted = $this->encryption->decrypt($ses_password);

    if($old_password == $ses_password_decrypted)
    {

      if($new_password == $new_password2)
      {
        $this->user_model->change_password($new_password);
        set_alert("success", "Password has changed!");
        redirect("logout");
      }
      else
      {
        set_alert("danger", "New Password do not match!");
        redirect("dashboard");
      }

    }
    else
    {
      set_alert("danger", "Old Password Wrong!");
      redirect("dashboard");
    }

  }
}
