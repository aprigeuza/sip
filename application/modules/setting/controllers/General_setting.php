<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_setting extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("setting_model");
  }

  public function index()
  {
    $this->load->view("general_setting/index");
  }

  public function save()
  {
    $post_data = array_merge($_POST, $_FILES);

    foreach ($post_data as $name => $value)
    {
      $set = $this->setting_model->get_data($name);
      if($set)
      {

        if($set->type == 'file')
        {
          if(isset($_FILES[$name]["name"]) && $_FILES[$name]["name"] != "")
          {
            $_file_name = explode(".", $_FILES[$name]["name"]);
            $file_exstention = $_file_name[(count($_file_name) - 1)];
            $config['upload_path'] = 'files/';
            $config['overwrite'] = TRUE;
            $config['file_name'] = $name;
            $config['allowed_types'] = $file_exstention;
            $this->load->library('upload', $config);
            $this->upload->do_upload($name);
            $value = base_url($config['upload_path'].$config['file_name'].".".$config['allowed_types']);
            $this->setting_model->update($name, $value);
          }
        }

        if($set->type == "json")
        {
          if(is_array($value))
          {
            $value = json_encode($value);
          }
          $this->setting_model->update($name, $value);
        }

        if($set->type == "text")
        {
          $this->setting_model->update($name, trim($value));
        }

        if($set->type == "number")
        {
          $this->setting_model->update($name, trim($value));
        }
      }
    }

    set_alert("success", "Berhasil menyimpan data!");
    redirect("setting/general_setting");
  }

  function upload($name)
  {
  }

  public function generate()
  {
    $submit = $this->input->get("submit");

    if($submit == "save")
    {
      $name = $this->input->get("name");
      $title = $this->input->get("title");
      $type = $this->input->get("type");
      $value = $this->input->get("value");

      $data['name'] = $name;
      $data['title'] = $title;
      $data['type'] = $type;
      $data['value'] = $value;

      $this->setting_model->replace($data);
    }

    if($submit == "delete")
    {
      $name = $this->input->get("name");
      $this->setting_model->delete($name);
    }

    $this->load->library('table');
    $template = array(
            'table_open'            => '<table border="1" cellpadding="4" cellspacing="0">',

            'thead_open'            => '<thead>',
            'thead_close'           => '</thead>',

            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th>',
            'heading_cell_end'      => '</th>',

            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',

            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td>',
            'cell_end'              => '</td>',

            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td>',
            'cell_alt_end'          => '</td>',

            'table_close'           => '</table>'
    );
    $this->table->set_template($template);
    $d = $this->db->get("setting");
    $data["tb"] = $this->table->generate($d);
    $this->load->view("general_setting/generate", $data);
  }

  public function field()
  {
    $t = $this->input->get("t");

    $fields = $this->db->list_fields($t);

    foreach($fields as $field)
    {
      echo $field;
      echo "<br>";
    }
  }
}
