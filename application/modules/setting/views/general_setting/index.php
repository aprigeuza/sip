<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Setting - Umum</title>
  <?php echo assets_top(); ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"); ?>">

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php echo page_header(); ?>
    <?php echo page_sidebar(); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Umum
          <small>Setting</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-settings"></i> Setting</a></li>
          <li class="active">Umum</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <?php echo show_alert(); ?>
        <div class="row">
          <div class="col-md-12">
            <form action="<?php echo set_url("setting", "general_setting", "save"); ?>" method="post" enctype="multipart/form-data">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Informasi Umum</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" id="company_name" name="company_name" placeholder="" value="<?php echo get_setting('company_name'); ?>">
                      </div>
                      <div class="form-group">
                        <label>Office Address</label>
                        <textarea class="form-control" name="office_address" id="office_address" rows="4" cols="80"><?php echo get_setting("office_address"); ?></textarea>
                      </div>
                      <div class="form-group">
                        <label>Factory Addsress</label>
                        <textarea class="form-control" name="factory_address" id="factory_address" rows="4" cols="80"><?php echo get_setting("factory_address"); ?></textarea>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Telp</label>
                        <input type="text" class="form-control" id="contact_telp" name="contact_telp" placeholder="" value="<?php echo get_setting('contact_telp'); ?>">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="" value="<?php echo get_setting('contact_email'); ?>">
                      </div>
                      <div class="form-group">
                        <label>Company Slogan</label>
                        <input type="text" class="form-control" id="company_slogan" name="company_slogan" placeholder="" value="<?php echo get_setting('company_slogan'); ?>">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <button type="submit" name="button" class="btn btn-primary">Update</button>
                  <button type="reset" name="reset" class="btn btn-default">Reset</button>
                </div>
              </div>
            </form>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <form action="<?php echo set_url("setting", "general_setting", "save"); ?>" method="post" enctype="multipart/form-data">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Setting Quotation</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Note</label>
                        <textarea name="quotation_note" id="quotation_note" class="textarea" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo get_setting("quotation_note"); ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <button type="submit" name="button" class="btn btn-primary">Update</button>
                  <button type="reset" name="reset" class="btn btn-default">Reset</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6">
            <form action="<?php echo set_url("setting", "general_setting", "save"); ?>" method="post" enctype="multipart/form-data">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Setting Invoice</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Note</label>
                        <textarea name="invoice_note" id="invoice_note" class="textarea" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo get_setting("invoice_note"); ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <button type="submit" name="button" class="btn btn-primary">Update</button>
                  <button type="reset" name="reset" class="btn btn-default">Reset</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
        <div class="col-md-6">
          <form action="<?php echo set_url("setting", "general_setting", "save"); ?>" method="post" enctype="multipart/form-data">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Setting Purchase Order</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Note</label>
                      <textarea name="purchase_order_note" id="purchase_order_note" class="textarea" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo get_setting("purchase_order_note"); ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Update</button>
                <button type="reset" name="reset" class="btn btn-default">Reset</button>
              </div>
            </div>
          </form>
        </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php echo page_footer(); ?>

    <?php echo page_sidebar_control(); ?>
  </div>
  <!-- ./wrapper -->
  <?php echo assets_bottom(); ?>

  <!-- CK Editor -->
  <script src="<?php echo base_url("assets/bower_components/ckeditor/ckeditor.js"); ?>"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="<?php echo base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"); ?>"></script>

  <script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>

</body>
</html>
