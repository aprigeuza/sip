<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title></title>
  <style media="screen">
  body{
    padding-left: 50px;
    padding-right: 50px;
  }
  input,select{
    width: 200px;
    padding: 5px;
  }
  button{
    padding: 5px;
    width: 60px;
  }
  table{
    width: 600px;
  }
  </style>
</head>
<body>
  <form action="<?php echo current_url(); ?>" method="get">
    <h1>Generate</h1>
    <label for="">Name</label> :
    <br>
    <input type="text" name="name" value="">
    <br>
    <label for="">Title</label> :
    <br>
    <input type="text" name="title" value="">
    <br>
    <label for="">Type</label> :
    <br>
    <select name="type">
      <option value="text">text</option>
      <option value="number">number</option>
      <option value="file">file</option>
      <option value="img">img</option>
      <option value="json">json</option>
    </select>
    <br>
    <label for="">Value</label> :
    <br>
    <input type="text" name="value" value="">
    <br>
    <br>
    <button type="submit" name="submit" value="save">Save</button>
    <button type="submit" name="submit" value="delete">Delete</button>
  </form>
  <br>
  <?php
  echo $tb;
  ?>
</body>
</html>
