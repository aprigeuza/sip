<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Setting - User - Edit</title>
  <?php echo assets_top(); ?>

  <style media="screen">
    #ul1{
      list-style-type: none;
      padding: 0;
      margin-left: 0;
    }
    #ul1 ul{
      list-style-type: none;
      margin: 0px;
    }
    li label{
      cursor: pointer;
    }
    .list-container{
      max-height: 500px;
      overflow:auto;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>Setting</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-settings"></i> Setting</a></li>
        <li><a href="<?php echo base_url("setting", "user"); ?>">User</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <form action="<?php echo set_url("setting", "user", "update"); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit User</h3>

                <div class="box-tools pull-right">
                  <a href="<?php echo set_url("setting", "user", "edit")."?id=".$edit->id; ?>" class="btn btn-box-tool" title="Refresh"><i class="fa fa-refresh"></i></a>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" class="form-control" id="username" placeholder="" autofocus readonly value="<?php echo $edit->username; ?>">
                    </div>
                    <div class="form-group">
                      <label>Level</label>
                      <select class="form-control" name="level" required>
                        <?php foreach($this->config->item("user_level_list") as $k => $v): ?>
                        <option value="<?php echo $k; ?>" <?php if($k==$edit->level) echo ' selected'; ?>><?php echo $v; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Obsolete</label>
                      <select class="form-control" name="obsolete" required>
                        <option value="1" <?php if($edit->obsolete == 1) echo ' selected'; ?>>YES</option>
                        <option value="0" <?php if($edit->obsolete == 0) echo ' selected'; ?>>NO</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="list-container">
                      <?php
                      function sub_list_menu($menus)
                      {
                        $CI =& get_instance();
                        $res = "";
                        if(count($menus))
                        {
                          $res .= '<ul>';

                          foreach($menus as $menu)
                          {
                            $res .= '<li>';
                            $res .= '<label class="checkbox-inline">';
                            $res .= '<input';
                            $res .= ' type="checkbox"';
                            $res .= ' name="menu[]"';
                            $res .= ' id="list_menu"'.$menu->id;
                            $res .= ' class="checkbox-inline icheck"';
                            $res .= ' value="'.$menu->id.'"';
                            if($menu->checked) $res .= ' checked="checked"';
                            $res .= ' > ';
                            $res .= ' <i class="'.$menu->icon.'"></i> ';
                            $res .= $menu->title;
                            $res .= '</label>';
                            $res .= sub_list_menu($menu->child);
                            $res .= '</li>';
                          }
                          $res .= '</ul>';
                        }
                        return $res;
                      }

                      $list_menu = $user_menu;
                      $res = '';
                      $res .= '<ul id="ul1">';
                      foreach ($list_menu as $menu)
                      {
                        $res .= '<li>';
                        $res .= '<label class="checkbox-inline">';
                        $res .= '<input';
                        $res .= ' type="checkbox"';
                        $res .= ' name="menu[]"';
                        $res .= ' id="list_menu"'.$menu->id;
                        $res .= ' class="checkbox-inline icheck"';
                        $res .= ' value="'.$menu->id.'"';
                        if($menu->checked) $res .= ' checked="checked"';
                        $res .= ' > ';
                        $res .= ' <i class="'.$menu->icon.'"></i> ';
                        $res .= $menu->title;
                        $res .= '</label>';
                        if (count($menu->child)>0)
                        {
                          $res .= sub_list_menu($menu->child);
                        }
                        $res .= '</li>';
                      }
                      $res .= '</ul>';
                      echo $res;
                      ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("setting", "user"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$("input[type=checkbox]").on("change", function(){
  $(this).parents("li label .icheck").attr("checked", "checked");
});
</script>

</body>
</html>
