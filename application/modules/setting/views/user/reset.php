<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Setting - User - Add</title>
  <?php echo assets_top(); ?>

  <style media="screen">
    #ul1{
      list-style-type: none;
      padding: 0;
      margin-left: 0;
    }
    #ul1 ul{
      list-style-type: none;
      margin: 0px;
    }
    li label{
      cursor: pointer;
    }
    .list-container{
      max-height: 500px;
      overflow:auto;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>Setting</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-settings"></i> Setting</a></li>
        <li><a href="<?php echo base_url("setting", "user"); ?>">User</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <form action="<?php echo set_url("setting", "user", "update_password"); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit User</h3>

                <div class="box-tools pull-right">
                  <a href="<?php echo set_url("setting", "user", "reset")."?id=".$edit->id; ?>" class="btn btn-box-tool" title="Refresh"><i class="fa fa-refresh"></i></a>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" class="form-control" id="username" placeholder="" autofocus readonly value="<?php echo $edit->username; ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Password *</label>
                      <input class="form-control" type="password" value="" id="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                      <label>Re-Password *</label>
                      <input class="form-control" type="password" value="" id="re_password" name="re_password" placeholder="Re-Password" required>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("setting", "user"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$("input[type=checkbox]").on("change", function(){
  $(this).parents("li label .icheck").attr("checked", "checked");
});
</script>

</body>
</html>
