<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Setting - User</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>Setting</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-settings"></i> Setting</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <div class="row">
            <div class="col-md-12">
              <a href="<?php echo set_url("setting", "user", "add"); ?>" class="btn btn-primary" title="add new"><i class="fa fa-file-o"></i> Add New</a>
              <a href="<?php echo set_url("setting", "user"); ?>" class="btn btn-info" title="Reload"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
          </div>
          <hr class="line-separator">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table1">
                <thead>
                  <tr>
                    <th>#</th>
                    <th class="col-md-6">Username</th>
                    <th>Level</th>
                    <th>Last Login</th>
                    <th>Obsolete</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($table_data->result() as $row): ?>
                  <tr<?php if($row->obsolete) echo ' class="text-danger"'; ?>>
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo $row->username; ?></td>
                    <td><?php echo $this->config->item("user_level_list")[$row->level]; ?></td>
                    <td><?php echo $row->last_login; ?></td>
                    <td class="text-center"><?php echo ($row->obsolete)?'YES':'NO'; ?></td>
                    <td>
                      <a href="<?php echo set_url("setting", "user", "edit")."?id=".$row->id; ?>" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo set_url("setting", "user", "reset")."?id=".$row->id; ?>" class="btn btn-warning btn-sm" title="Reset Password"><i class="fa fa-refresh"></i></a>
                      <?php if($row->obsolete): ?>
                      <a href="<?php echo set_url("setting", "user", "obsolete")."?id=".$row->id; ?>" class="btn btn-success btn-sm" title="Un-Obsolete" onclick="return confirm('Un-Obsolete <?php echo $row->username; ?>?');"><i class="fa fa-check"></i></a>
                      <?php else: ?>
                      <a href="<?php echo set_url("setting", "user", "obsolete")."?id=".$row->id; ?>" class="btn btn-danger btn-sm" title="Obsolete" onclick="return confirm('Obsolete <?php echo $row->username; ?>?');"><i class="fa fa-ban"></i></a>
                      <?php endif; ?>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  $(function () {
    $('#table1').DataTable()
  })
</script>

</body>
</html>
