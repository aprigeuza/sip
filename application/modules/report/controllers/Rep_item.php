<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rep_item extends MX_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    validate_access();

    $this->load->model("rep_item_model");
  }

  public function index()
  {
    $data["items"] = $this->rep_item_model->get_items();
    $this->load->view("rep_item/index", $data);
  }

  public function datatable_item_sales()
  {
    $config["table"] = "v_item_sales_year";
    if($this->input->post_get("filter_month")) $config["table"] = "v_item_sales_month";
    $where_all[] = "so_year = " . (int)$this->input->post_get("filter_year");
    if($this->input->post_get("filter_month")) $where_all[] = "so_month = " . (int)$this->input->post_get("filter_month");
    $where_all[] = "frequent >= " . (int)$this->input->post_get("filter_minimal_frequent");
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function get_support()
  {
    $item_id = $this->input->post("item_id");
    $filter_year = (int)$this->input->post_get("filter_year");
    $filter_month = (int)$this->input->post_get("filter_month");
    $filter_minimal_frequent = (int)$this->input->post_get("filter_minimal_frequent");

    $response["status"] = true;
    $response["support"] = $this->rep_item_model->get_support($item_id, $filter_year, $filter_month, $filter_minimal_frequent);

    json_file($response);
  }

  public function datatable_item_sales_dataset()
  {
    $config["table"] = "v_item_sales_dataset";
    $where_all[] = "YEAR(so_date) = " . (int)$this->input->post_get("filter_year");
    if($this->input->post_get("filter_month")) $where_all[] = "MONTH(so_date) = " . (int)$this->input->post_get("filter_month");
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function process_analisists()
  {
    $data = array();

    $items = $this->input->post("items");
    $filter_year = (int)$this->input->post_get("filter_year");
    $filter_month = (int)$this->input->post_get("filter_month");
    $filter_minimal_frequent = (int)$this->input->post_get("filter_minimal_frequent");

    if(!$items)
    {
      $items = array();
      $y = $this->rep_item_model->get_filter_items(NULL, $filter_year, $filter_month, $filter_minimal_frequent);

      foreach($y->result() as $row)
      {
        $items[] = $row->id;
      }
    }

    // $_items = array_unique($items);
    if (COUNT($items) >= 2)
    {
      $dimentional_result = $this->rep_item_model->process_analisists($items, $filter_year, $filter_month, $filter_minimal_frequent);

      $data["items"] = $items;
      $data["dimentional_result"] = $dimentional_result;

      $response["items"] = $items;
      $response["status"] = true;
      $response["dimentional_result"] = $dimentional_result;
      $response["html"] = $this->load->view("rep_item/result_analisists", $data, true);
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "Select item min : 2.";
    }


    json_file($response);
  }

  public function get_filter_years()
  {
    $response = array();

    $items = array();

    $search = $this->input->get("search");
    $y = $this->rep_item_model->get_filter_years($search);

    foreach($y->result() as $row)
    {
      $items[] = array(
        "id" => $row->so_year,
        "text" => $row->so_year . " (".$row->total_sales.")"
      );
    }

    $response["status"] = true;
    $response["items"] = $items;

    json_file($response);
  }

  public function get_filter_months()
  {
    $response = array();

    $items = array();

    $search = $this->input->get("search");
    $filter_year = $this->input->get("filter_year");
    $y = $this->rep_item_model->get_filter_months($search, $filter_year);

    $items[] = array(
      "id" => 0,
      "text" => "ALL"
    );
    foreach($y->result() as $row)
    {
      $items[] = array(
        "id" => $row->so_month,
        "text" => $row->so_month_name . " (".$row->total_sales.")"
      );
    }

    $response["status"] = true;
    $response["items"] = $items;

    json_file($response);
  }

  public function get_filter_items()
  {
    $response = array();

    $items = array();

    $search = $this->input->get("search");
    $filter_year = $this->input->get("filter_year");
    $filter_month = $this->input->get("filter_month");
    $filter_minimal_frequent = $this->input->get("filter_minimal_frequent");
    $y = $this->rep_item_model->get_filter_items($search, $filter_year, $filter_month, $filter_minimal_frequent);

    foreach($y->result() as $row)
    {
      $items[] = array(
        "id" => $row->id,
        "text" => $row->item_name . " (".$row->frequent.")"
      );
    }

    $response["status"] = true;
    $response["items"] = $items;

    json_file($response);
  }
}
