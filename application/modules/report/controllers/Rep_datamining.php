<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rep_datamining extends MX_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("rep_datamining_model");
  }

  public function index()
  {
    $data = array();

    $data["customers"] = $this->rep_datamining_model->get_customers();
    $this->load->view("rep_datamining/index", $data);
  }

  public function datatable()
  {
    $where_all = array();

    $filter = $this->input->post_get("filter");

    if(is_array($filter))
    {
      foreach($filter as $f)
      {
        if(!empty($f) && strlen($f["value"]) > 0)
        {
          $s = strpos($f["name"], " ", 0);
          if($s)
          {
            $where_all[] = $f["name"] . " " . "'".$f["value"]."'";
          }
          else
          {
            $where_all[] = $f["name"] . " = " . "'".$f["value"]."'";
          }
        }
      }
    }

    $config["table"] = "v_datamining";
    if(count($where_all) >= 1) $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function export()
  {
    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=datamining.xls");  //File name extension was wrong
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);

    $this->load->library('table');

    $template = array(
          'table_open'            => '<table border="1" cellpadding="4" cellspacing="0">',

          'thead_open'            => '<thead>',
          'thead_close'           => '</thead>',

          'heading_row_start'     => '<tr>',
          'heading_row_end'       => '</tr>',
          'heading_cell_start'    => '<th>',
          'heading_cell_end'      => '</th>',

          'tbody_open'            => '<tbody>',
          'tbody_close'           => '</tbody>',

          'row_start'             => '<tr>',
          'row_end'               => '</tr>',
          'cell_start'            => '<td>',
          'cell_end'              => '</td>',

          'row_alt_start'         => '<tr>',
          'row_alt_end'           => '</tr>',
          'cell_alt_start'        => '<td valign="top">',
          'cell_alt_end'          => '</td>',

          'table_close'           => '</table>'
    );

    $this->table->set_template($template);

    $this->table->set_heading(
      'Cust#',
      'Customer',
      'NPWP',
      'Telp',
      'Item#',
      'Item Name',
      'SO#',
      'SO Date',
      'SO Cancel',
      'SO Posted',
      'SO Qty',
      'SO Unit Price',
      'DO#',
      'DO Date',
      'DO Cancel',
      'DO Posted',
      'Shipping Address',
      'Vehicle#',
      'Driver',
      'DO Qty',
      'IV#',
      'IV Date',
      'IV Cancel',
      'IV Posted',
      'Billing Address',
      "Payment Date",
      "Payment Type",
      "Payment Proof",
      "Payment Amount",
      "Payment Remark"
    );

    $filter = $this->input->get();

    $export_data = $this->rep_datamining_model->export_data($filter);

    foreach($export_data->result() as $row)
    {
      $this->table->add_row(
        $row->cust_code,
        $row->cust_name,
        $row->npwp,
        $row->telp,
        $row->item_code,
        $row->item_name,
        $row->so_number,
        $row->so_date,
        ($row->so_cancel) ? "Y" : "N",
        ($row->so_posted) ? "Y" : "N",
        $row->so_qty,
        $row->so_unit_price,
        $row->do_number,
        $row->do_date,
        ($row->do_cancel) ? "Y" : "N",
        ($row->do_posted) ? "Y" : "N",
        $row->do_shipping_address,
        $row->vehicle_number,
        $row->vehicle_driver,
        $row->do_qty,
        $row->iv_number,
        $row->iv_date,
        ($row->iv_cancel) ? "Y" : "N",
        ($row->iv_posted) ? "Y" : "N",
        $row->payment_date,
        $row->payment_type,
        $row->payment_proof,
        $row->payment_amount,
        $row->payment_remark
      );
    }
    echo $this->table->generate();
  }

}
