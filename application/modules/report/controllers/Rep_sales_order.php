<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rep_sales_order extends MX_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

  }

  public function index()
  {
    $data = array();

    $this->load->view("rep_sales_order/index", $data);
  }

  public function datatable_do_outsanding()
  {
    $where_all = array();

    $config["table"] = "v_delivery_order_outstanding";
    if(count($where_all) >= 1) $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function datatable_iv_outsanding()
  {
    $where_all = array();

    $config["table"] = "v_invoice_outstanding";
    if(count($where_all) >= 1) $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function datatable_payment_outstanding()
  {
    $config["table"] = "v_invoice_payment";
    $where_all[] = "posted = 1";
    $where_all[] = "closed = 0";
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

}
