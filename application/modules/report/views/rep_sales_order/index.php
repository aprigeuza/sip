<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Sales Outstanding - Report</title>
  <?php echo assets_top(); ?>
  <style media="screen">
    table {
      font-size: 12px;
    }
    table th {
      white-space: nowrap;
    }
    table td {
      white-space: nowrap;
      padding-top: 3px;
      padding-bottom: 3px;
    }
  </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Outstanding
        <small>Report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report</a></li>
        <li class="active">Sales Outstanding</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Delivery Oustanding</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="table1">
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Invoice Oustanding</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="table2">
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Payment Oustanding</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="table3">
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  var table1;
  $(function () {
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("report", "rep_sales_order", "datatable_do_outsanding"); ?>',
      "columns":
        [
          { "data": "cust_code", "title": "Cust#"},
          { "data": "cust_name", "title": "Customer"},
          { "data": "po_number", "title": "PO#"},
          { "data": "so_number", "title": "SO#"},
          { "data": "so_date", "title": "SO Date"},
          { "data": "item_code", "title": "Item#"},
          { "data": "item_name", "title": "Item Name"},
          { "data": "so_qty", "title": "SO Qty", className: "text-right",},
          { "data": "unit", "title": "Unit", className: "text-center",},
          { "data": "unit_price", "title": "Unit Price", className: "text-center",},
          { "data": "do_qty", "title": "DO Qty", className: "text-right",},
          { "data": "so_outstanding", "title": "Outstanding", className: "text-right",},
        ]
    });
  })
</script>

<script>
  var table2;
  $(function () {
    table2 = $('#table2').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("report", "rep_sales_order", "datatable_iv_outsanding"); ?>',
      "columns":
        [
          { "data": "cust_code", "title": "Cust#"},
          { "data": "cust_name", "title": "Customer"},
          { "data": "po_number", "title": "PO#"},
          { "data": "so_number", "title": "SO#"},
          { "data": "so_date", "title": "SO Date"},
          { "data": "do_number", "title": "DO#"},
          { "data": "do_date", "title": "DO Date"},
          { "data": "item_code", "title": "Item#"},
          { "data": "item_name", "title": "Item Name"},
          { "data": "do_qty", "title": "Qty", className: "text-right",},
          { "data": "do_unit", "title": "Unit", className: "text-center",},
        ]
    });
  })
</script>

<script>
  var table3;
  $(function () {
    table3 = $('#table3').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("report", "rep_sales_order", "datatable_payment_outstanding"); ?>',
      "columns":
        [
          { "data": "so_number", "title": "SO#"},
          { "data": "so_date", "title": "SO Date"},
          { "data": "cust_code", "title": "Customer#"},
          { "data": "cust_name", "title": "Customer"},
          { "data": "iv_number", "title": "Invoice#"},
          { "data": "iv_date", "title": "Invoice Date"},
          { "data": "total_invoice", "title": "Total Invoice", className: "text-right",},
          { "data": "total_ppn", "title": "Total PPN", className: "text-right",},
          { "data": "shipping_cost", "title": "Shipping Cost", className: "text-right",},
          { "data": "other_cost", "title": "Other Cost", className: "text-right",},
          { "data": "deduction_dp", "title": "Deduction DP", className: "text-right",},
          { "data": "grand_total_invoice", "title": "Grand Total Invoice", className: "text-right",},
          { "data": "total_payment", "title": "Total Payment", className: "text-right",},
          { "data": "total_outstanding", "title": "Total Outstanding", className: "text-right",},
        ]
    });
  })
</script>
</body>
</html>
