<table class="table table-bordered" id="temptable1">
  <thead>
    <tr>
      <?php
      for($i=0;$i<2;$i++): ?>
      <th>Item <?php echo ($i + 1); ?></th>
      <?php endfor; ?>
      <th>Size</th>
      <th>Frequent</th>
      <th>Support (%)</th>
      <th>Confidence (%)</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($dimentional_result->items as $item): ?>
    <tr>
      <?php
      $item_code = explode(",", $item->item_code);
      for($ii=0;$ii<2;$ii++): ?>
      <td>
        <?php echo (isset($item_code[$ii]))? $item_code[$ii] : ""; ?>
      </td>
      <?php endfor; ?>
      <td class="text-center">
        <?php echo $item->size; ?>
      </td>
      <td class="text-center">
        <?php echo $item->frequent; ?>
      </td>
      <td class="text-center">
        <?php echo number_format(($item->support * 100), 4); ?>
      </td>
      <td class="text-center">
        <?php echo number_format(($item->confidence * 100), 4); ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<script type="text/javascript">
  $(document).ready(function() {
    $("#temptable1").DataTable();
  });
</script>
