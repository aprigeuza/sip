<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Item - Report</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Item
        <small>Report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report</a></li>
        <li class="active">Item</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Filter Box</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Select Year</label>
                    <select class="form-control" name="filter_year" id="filter_year" style="width:100%">
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Select Month</label>
                    <select class="form-control" name="filter_month" id="filter_month" style="width:100%">
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label>Minimal Frequent</label>
                    <input class="form-control" type="number" name="filter_minimal_frequent" id="filter_minimal_frequent" value="">
                  </div>
                </div>
              </div>
            </div>
            <div class="box-footer">
              <button type="button" name="button" class="btn btn-primary" onclick="filter_all();">Filter</button>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Item Sales</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table1">
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Dataset</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table2">
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Sales Analisits</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Filter Specified Items</label>
                    <select class="form-control" name="items[]" multiple id="items" style="width:100%;">
                    </select>
                  </div>
                  <button type="button" name="button" class="btn btn-primary" onclick="processAnalisists();">Process</button>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Results</label>
                  </div>
                  <div class="well well-sm" style="background-color:#fff;">
                    <div class="result_analisists">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$(document).ready(function() {
  $('#filter_year').select2({
    ajax: {
      url: '<?php echo set_url("report", "rep_item", "get_filter_years"); ?>',
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term,
        }

        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (data) {
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: data.items
        };
      }
    }
  });

  $('#filter_month').select2({
    ajax: {
      url: '<?php echo set_url("report", "rep_item", "get_filter_months"); ?>',
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term,
          filter_year: $("#filter_year").val()
        }

        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (data) {
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: data.items
        };
      }
    }
  });

  $('#items').select2({
    ajax: {
      url: '<?php echo set_url("report", "rep_item", "get_filter_items"); ?>',
      dataType: 'json',
      data: function (params) {
        var query = {
          search: params.term,
          filter_year: $("#filter_year").val(),
          filter_month: $("#filter_month").val(),
          filter_minimal_frequent: $("#filter_minimal_frequent").val(),
        }

        // Query parameters will be ?search=[term]&type=public
        return query;
      },
      processResults: function (data) {
        // Transforms the top-level key of the response object from 'items' to 'results'
        return {
          results: data.items
        };
      }
    }
  });
});
</script>

<script>
  var table1;
  function loadItemSales(){
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax":{
        "url": '<?php echo set_url("report", "rep_item", "datatable_item_sales"); ?>',
        "data": {
          "filter_year": $("#filter_year").val(),
          "filter_month": $("#filter_month").val(),
          "filter_minimal_frequent": $("#filter_minimal_frequent").val()
        },
      },
      "columns":
        [
          { "data": "item_code", "title": "Item#"},
          { "data": "item_name", "title": "Item Name"},
          { "data": "total_qty", "title": "Summary", className: "text-right",},
          { "data": "frequent", "title": "Frequent", className: "text-right",},
          { "data": null, "title": "Support (%)", className: "text-right", "createdCell": function (td, cellData, rowData, row, col) {

            $.ajax({
              url: '<?php echo base_url("report/rep_item/get_support"); ?>',
              type: 'POST',
              dataType: 'JSON',
              data: {
                "item_id": rowData.id,
                "filter_year": $("#filter_year").val(),
                "filter_month": $("#filter_month").val(),
                "filter_minimal_frequent": $("#filter_minimal_frequent").val()
              },
              success: function(response){
                $(td).html(number_format((response.support*100), 4));
              }
            });
          }},
        ]
    });
  }
</script>

<script>
  function filter_all(){
    loadItemSales();
    loadItemDataset();
    processAnalisists();
  }
</script>

<script>
  var table2;
  function loadItemDataset(){
    table2 = $('#table2').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax":{
        "url": '<?php echo set_url("report", "rep_item", "datatable_item_sales_dataset"); ?>',
        "data": {
          "filter_year": $("#filter_year").val(),
          "filter_month": $("#filter_month").val(),
          "filter_minimal_frequent": $("#filter_minimal_frequent").val()
        },
      },
      "columns":
        [
          { "data": "item_code", "title": "Item#"},
          { "data": "item_name", "title": "Item Name"},
          { "data": "qty", "title": "Qty", className: "text-right",},
          { "data": "so_number", "title": "SO#", className: "text-left",},
          { "data": "so_date", "title": "SO Date", className: "text-left",},
        ]
    });
  }
</script>

<script>

  function processAnalisists(){
    $.ajax({
      url: '<?php echo set_url("report", "rep_item", "process_analisists"); ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {
        items: $("#items").val(),
        filter_year: $("#filter_year").val(),
        filter_month: $("#filter_month").val(),
        filter_minimal_frequent: $("#filter_minimal_frequent").val(),
      },
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          $(".result_analisists").html(response.html);
        } else {
          alertify.error(response.message);
          $(".result_analisists").html("");
        }

        loadingSpinner.hide();
      }
    });
  }
</script>

</body>
</html>
