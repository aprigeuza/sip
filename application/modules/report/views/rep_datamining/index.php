<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Datamining - Report</title>
  <?php echo assets_top(); ?>
  <style media="screen">
    #table1 {
      font-size: 12px;
    }
    #table1 th {
      white-space: nowrap;
    }
    #table1 td {
      white-space: nowrap;
      padding-top: 3px;
      padding-bottom: 3px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Datamining
        <small>Report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report</a></li>
        <li class="active">Datamining</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-4">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Filter Box</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="row">
                <div class="col-md-12">
                  <form id="filterform" method="post">
                    <div class="form-group">
                      <label>Select Customer</label>
                      <select class="form-control" name="cust_code" id="cust_code" style="width:100%;">
                        <option value="">Select Customer</option>
                        <?php foreach($customers->result() as $customer): ?>
                        <option value="<?php echo $customer->cust_code; ?>"><?php echo $customer->cust_code . " - " . $customer->cust_name; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <h3>SO Date</h3>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period Start</label>
                          <input type="text" class="form-control datepicker" id="so_date" name="so_date >=" placeholder="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period End</label>
                          <input type="text" class="form-control datepicker" id="so_date" name="so_date <=" placeholder="">
                        </div>
                      </div>
                    </div>
                    <h3>DO Date</h3>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period Start</label>
                          <input type="text" class="form-control datepicker" id="do_date" name="so_date >=" placeholder="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period End</label>
                          <input type="text" class="form-control datepicker" id="do_date" name="so_date <=" placeholder="">
                        </div>
                      </div>
                    </div>
                    <h3>IV Date</h3>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period Start</label>
                          <input type="text" class="form-control datepicker" id="iv_date" name="so_date >=" placeholder="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period End</label>
                          <input type="text" class="form-control datepicker" id="iv_date" name="so_date <=" placeholder="">
                        </div>
                      </div>
                    </div>
                  </form>

                  <button type="button" id="btn_process" class="btn btn-primary">Process</button>
                  <button type="button" id="btn_export" class="btn btn-success">Export</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Result</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="table1">
                </table>
              </div>
            </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  var table1;
  function loadTable(){
    var filterform = $("#filterform");
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "responsive": true,
      "ajax": {
        "url" : '<?php echo set_url("report", "rep_datamining", "datatable"); ?>',
        "data" : {
          filter : filterform.serializeArray()
        },
        "type" : "POST"
      },
      "columns":
        [
          { "data": "cust_code", "title": "Cust#"},
          { "data": "cust_name", "title": "Customer"},
          { "data": "npwp", "title": "NPWP"},
          { "data": "so_number", "title": "SO#"},
          { "data": "so_date", "title": "SO Date"},
          { "data": "so_cancel", "title": "SO Cancel", "createdCell" : function (td, cellData, rowData, row, col) {
            if(cellData==true){
              $(td).html("YES");
            }else{
              $(td).html("NO");
            }
          }},
          { "data": "so_posted", "title": "SO Posted", "createdCell" : function (td, cellData, rowData, row, col) {
            if(cellData==true){
              $(td).html("YES");
            }else{
              $(td).html("NO");
            }
          }},
          { "data": "cust_code", "title": "Cust#", },
          { "data": "cust_name", "title": "Customer", },
          { "data": "npwp", "title": "NPWP", },
          { "data": "telp", "title": "Telp'", },
          { "data": "item_code", "title": "Item#", },
          { "data": "item_name", "title": "Item Name", },
          { "data": "so_number", "title": "SO#", },
          { "data": "so_date", "title": "SO Date", },
          { "data": "so_cancel", "title": "SO Cancel", },
          { "data": "so_posted", "title": "SO Posted", },
          { "data": "so_qty", "title": "SO Qty", },
          { "data": "so_unit_price", "title": "Unit Price", },
          { "data": "do_number", "title": "DO#", },
          { "data": "do_date", "title": "DO Date", },
          { "data": "do_cancel", "title": "DO Cancel", },
          { "data": "do_posted", "title": "DO Posted", },
          { "data": "do_shipping_address", "title": "Shipping Address", },
          { "data": "vehicle_number", "title": "Vehicle#", },
          { "data": "vehicle_driver", "title": "Driver", },
          { "data": "do_qty", "title": "DO Qty", },
          { "data": "iv_number", "title": "IV#", },
          { "data": "iv_date", "title": "IV Date", },
          { "data": "iv_cancel", "title": "IV Cancel", },
          { "data": "iv_posted", "title": "IV Posted", },
          { "data": "payment_date", "title": "Payment Date", },
          { "data": "payment_type", "title": "Payment Type", },
          { "data": "payment_proof", "title": "Payment Proof", },
          { "data": "payment_amount", "title": "Payment Amount", },
          { "data": "payment_remark", "title": "Payment Remark", },
        ]
    });
  }
  $(function () {
    loadTable();
  });
  $("#btn_process").click(function(event) {
    loadTable();
  });
  $("#btn_export").click(function(event) {
    var filterData = $("#filterform").serialize();

    window.location = "<?php echo set_url("report", "rep_datamining", "export"); ?>?" + filterData;
  });
</script>

<script>
  $(document).ready(function() {
    $("#cust_id").select2();
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });
</script>
</body>
</html>
