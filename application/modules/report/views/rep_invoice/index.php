<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Item - Report</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Item
        <small>Report</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-table"></i> Report</a></li>
        <li class="active">Item</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Item Sales</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table1">
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Dataset</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table2">
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Sales Analisits</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="row">
                <div class="col-md-4">
                  <form id="fm_analisists">
                    <div class="form-group">
                      <label>Select Items</label>
                      <select class="form-control" name="items[]" multiple id="items" style="width:100%;">
                        <?php foreach($items->result() as $item): ?>
                        <option value="<?php echo $item->id; ?>"><?php echo $item->item_code; ?> { F : <?php echo number_format($item->frequent); ?>}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period Start</label>
                          <input type="text" class="form-control" id="period_start" name="period_start" placeholder="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Period End</label>
                          <input type="text" class="form-control" id="period_end" name="period_end" placeholder="">
                        </div>
                      </div>
                    </div>
                  </form>

                  <button type="button" name="button" class="btn btn-primary" onclick="processAnalisists();">Process</button>
                </div>
                <div class="col-md-8">
                  <div class="form-group">
                    <label>Results</label>
                  </div>
                  <div class="well well-sm" style="background-color:#fff;">
                    <div class="result_analisists">

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  var table1;
  $(function () {
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("report", "rep_item", "datatable_item_sales"); ?>',
      "columns":
        [
          { "data": "item_code", "title": "Item#"},
          { "data": "item_name", "title": "Item Name"},
          { "data": "total_qty", "title": "Summary", className: "text-right",},
          { "data": "frequent", "title": "Frequent", className: "text-right",},
        ]
    });
  })
</script>

<script>
  var table2;
  $(function () {
    table2 = $('#table2').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("report", "rep_item", "datatable_item_sales_dataset"); ?>',
      "columns":
        [
          { "data": "item_code", "title": "Item#"},
          { "data": "item_name", "title": "Item Name"},
          { "data": "qty", "title": "Qty", className: "text-right",},
          { "data": "so_number", "title": "SO#", className: "text-left",},
          { "data": "so_date", "title": "SO Date", className: "text-left",},
        ]
    });
  })
</script>

<script>

  function processAnalisists(){
    var fm_analisists = $("#fm_analisists");
    $.ajax({
      url: '<?php echo set_url("report", "rep_item", "process_analisists"); ?>',
      type: 'POST',
      dataType: 'JSON',
      data: fm_analisists.serializeArray(),
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          $(".result_analisists").html(response.html);
        } else {
          alertify.error(response.message);
          $(".result_analisists").html("");
        }

        loadingSpinner.hide();
      }
    });
  }

  $(document).ready(function() {
    $("#items").select2({
      maximumSelectionLength: 4
    });

    $('#period_start').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $('#period_end').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });
</script>

</body>
</html>
