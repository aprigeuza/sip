<div class="table-responsive">
  <table class="table table-bordered table-responsive" id="tmp_item_table">
    <thead>
      <tr>
        <th style="min-width:200px">Item Description</th>
        <th style="min-width:100px">Qty</th>
        <th style="min-width:100px">Unit</th>
        <th style="min-width:100px">Unit Price</th>
        <th style="min-width:100px">Subtotal</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $subtotal_all = 0;
      if($temp_data):
      foreach ($temp_data as $_row):
        $row=json_decode($_row->data);
        $subtotal_all = $subtotal_all + ($row->qty * $row->unit_price);
        ?>
      <tr>
        <td><b><?php echo $row->item_code; ?></b><br><?php echo $row->item_name; ?><br><?php echo str_replace("\n", "<br>", $row->item_description); ?></td>
        <td class="text-right"><?php echo mask_money($row->qty); ?></td>
        <td><?php echo $row->unit; ?></td>
        <td class="text-right"><?php echo mask_money($row->unit_price); ?></td>
        <td class="text-right"><?php echo mask_money(($row->qty * $row->unit_price)); ?></td>
      </tr>
      <?php endforeach;
    endif; ?>
    </tbody>
  </table>
</div>
<div class="row">
  <div class="col-md-8">

  </div>
  <div class="col-md-4">
    <table class="table">
      <tbody>
        <tr>
          <th style="line-height: 33px;">Subtotal</th>
          <td>
            <input type="text" name="subtotal_all" id="subtotal_all" class="form-control text-right" value="<?php echo mask_money($subtotal_all); ?>" readonly>
          </td>
        </tr>
        <tr>
          <th style="line-height: 33px;">PPN</th>
          <td>
            <div class="row">
              <div class="col-md-4">
                <div class="input-group">
                  <input type="text" name="ppn" id="ppn" class="form-control text-right" value="10">
                  <span class="input-group-addon">%</span>
                </div>
              </div>
              <div class="col-md-8">
                <input type="text" name="ppn_total" id="ppn_total" class="form-control text-right" value="" readonly>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <th style="line-height: 33px;">Total</th>
          <td>
            <input type="text" name="total" id="total" class="form-control text-right" value="" readonly>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
function calculateSubtotal(){
  var subtotal_all = 0;
  var grand_total = 0;
  var ppn = $("#ppn").numberFormat("getValue");

  subtotal_all = $("#subtotal_all").numberFormat("getValue");
  $("#ppn_total").numberFormat("setValue", (subtotal_all * (ppn/100)));
  grand_total = subtotal_all + (subtotal_all / (ppn/100));

  $("#total").numberFormat("setValue", grand_total);

  $("#ppn_total").on('focusin', function(event) { $(this).select(); });

  $("#ppn").numberFormat();
  $("#ppn_total").numberFormat();
  $("#subtotal_all").numberFormat();
  $("#total").numberFormat();
}

$("input").on('change', function(event) {
  calculateSubtotal();
});

$(document).ready(function() {
  calculateSubtotal();
});
</script>
