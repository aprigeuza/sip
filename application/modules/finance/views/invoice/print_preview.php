<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Preview - Invoice</title>
    <style media="screen">
    @page { margin: 0px; }
      body { margin: 0px; }
      body{
        /* font-family: verdana; */
        font-size: 12px;
      }
      .paper{
        width: 100%;
        height: auto;
        margin-left: 0;
        margin-right: 0;
      }
      .print-container{
        margin-left: 30px;
        margin-right: 30px;
      }
      .print-header{
        padding-top:15px;
        padding-bottom: 15px;
        /* height: 240px; */
      }
      .print-body{
        /* height: 500px; */
      }
      .print-footer{
        height: 200px;
      }
      table {
           border-collapse: collapse;
       }
       table td{
         padding: 5px;
       }
       .unposted{
         width: 300px;
         position: absolute;
         display: block;
         font-size: 40px;
         z-index: 99999999;
         color: #dddd;
         top:50%;
         bottom:50%;
         left: 35%;
         right: 50%;
         margin-left: auto;
         margin-right: auto;
       }
    </style>
  </head>
  <body>
    <div class="paper">
      <?php if($data->posted == false): ?>
      <div class="unposted">
        <span>UN-POSTED</span>
      </div>
      <?php endif; ?>
      <?php if($data->cancel == true): ?>
      <div class="unposted">
        <span>CANCELED</span>
      </div>
      <?php endif; ?>
      <div class="print-container">
        <div class="print-header">
          <table style="width:100%;margin-bottom:15px">
            <tr>
              <td style="width:10%" valign="top">
                <img src="<?php echo "./assets/img/logo.png"; ?>" alt="" style="width:80px">
              </td>
              <td style="width:40%;text-align:left;" valign="center">
                <h1 style="margin-top:5px;margin-bottom:5px;font-size:18px"><?php echo get_setting("company_name"); ?></h1>
                <p style="margin-top:5px;margin-bottom:5px;"><?php echo get_setting("company_slogan"); ?>
                </p>
              </td>
              <td style="width:50%;">
                <h2 style="margin-top:5px;margin-bottom:5px;text-align:right;font-size:18px">INVOICE</h2>
                <table style="font-size:12px;width:100%;margin:0">
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:30%;text-align:right;">Invoice#</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:40%; border:1px solid #000;text-align:center;"><?php echo $data->iv_number; ?></td>
                  </tr>
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:30%;text-align:right;">Invoice Date</td>
                    <td style="padding-top:0px;padding-bottom:0px;border:1px solid #000;text-align:center;"><?php echo indo_date($data->iv_date); ?></td>
                  </tr>
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:30%;text-align:right;">Due Date</td>
                    <td style="padding-top:0px;padding-bottom:0px;border:1px solid #000;text-align:center;"><?php echo indo_date($data->due_date); ?></td>
                  </tr>
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:30%;text-align:right;">PO#</td>
                    <td style="padding-top:0px;padding-bottom:0px;border:1px solid #000;text-align:center;"><?php echo $data->po_number; ?></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table style="width:100%">
            <tr>
              <td valign="top" style="width:45%;text-align:left;line-height:22px;">
                <b>From</b>: <br>
                <?php echo get_setting("company_name"); ?>
                <p style="margin-top:5px;margin-bottom:5px;font-size:12px;line-height:15px">
                  <?php echo str_replace("\n", "<br>", get_setting("office_address")); ?><br>
                  Telp : <?php echo get_setting("contact_telp"); ?> <br>
                  Email : <?php echo get_setting("contact_email"); ?>
                </p>

              </td>
              <td valign="top"></td>
              <td valign="top" style="width:45%;text-align:left;line-height:22px;">
                <b>Bill To</b> :<br>
                <?php echo $data->cust_name; ?>
                <p style="margin-top:5px;margin-bottom:5px;font-size:12px;line-height: 15px;">
                  <?php echo str_replace("\n", "<br>", $data->billing_address); ?>
                  <br>
                  NPWP : <?php echo $data->npwp; ?></p>
              </td>
            </tr>
          </table>
        </div>
        <div class="print-body">
          <table style="width:100%" cellpadding="0" rowpadding="0">
            <tr>
              <th style="width:5%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">NO</th>
              <th style="width:10%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">DO</th>
              <th style="width:35%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">DESCRIPTION</th>
              <th style="width:10%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">QTY</th>
              <th style="width:10%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">UNIT</th>
              <th style="width:15%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">UNIT PRICE</th>
              <th style="width:15%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">TOTAL</th>
            </tr>
            <?php
            $grand_total = 0;
            $subtotal_all = 0;
            $n=1; ?>
            <?php for($i=0; $i<=8;$i++): ?>

            <?php
            $no = "&nbsp;";
            $item_description = "&nbsp;";
            $do = "&nbsp;";
            $qty = "&nbsp;";
            $unit = "&nbsp;";
            $unit_price = "&nbsp;";
            $subtotal = "&nbsp;";
            if(!empty($data->items[$i]))
            {
              $item = $data->items[$i];
              $item_description = $item->item_name . "<br>" . str_replace("\n", "<br>", $item->item_description);
              $do = "<strong>".$item->do_number ."</strong>". "<br>Date : " . indo_date($item->do_date);
              $qty = number_format($item->qty, 2);
              $unit = $item->unit;
              $unit_price = number_format($item->unit_price, 2);
              $subtotal = number_format($item->qty * $item->unit_price, 2);

              $subtotal_all = $subtotal_all + ($item->qty * $item->unit_price);

              $no = $n++;
            }
            ?>
            <tr>
              <td valign="top" style="font-size:12px;text-align: left;border-left:1px solid #000;text-align:center"><?php echo $no; ?></td>
              <td valign="top" style="font-size:12px;text-align: left;border-left:1px solid #000;"><?php echo $do; ?></td>
              <td valign="top" style="font-size:12px;text-align: left;border-left:1px solid #000;"><?php echo $item_description; ?></td>
              <td valign="top" style="font-size:12px;text-align: right;border-left:1px solid #000;"><?php echo $qty; ?></td>
              <td valign="top" style="font-size:12px;text-align: center;border-left:1px solid #000;"><?php echo $unit; ?></td>
              <td valign="top" style="font-size:12px;text-align: right;border-left:1px solid #000;"><?php echo $unit_price; ?></td>
              <td valign="top" style="font-size:12px;text-align: right;border-left:1px solid #000;border-right:1px solid #000;"><?php echo $subtotal; ?></td>
            </tr>
            <?php endfor; ?>
            <?php
            $grand_total = $grand_total + $subtotal_all;
            ?>
            <tr>
              <td style="padding:5px;border-top:1px solid #000;">&nbsp;</td>
              <td style="padding:5px;border-top:1px solid #000;">&nbsp;</td>
              <td style="padding:5px;border-top:1px solid #000;">&nbsp;</td>
              <td style="padding:5px;border-top:1px solid #000;">&nbsp;</td>
              <td colspan="2" style="padding:5px;border-top:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">SUBTOTAL</td>
              <td style="padding:5px;border-top:1px solid #000;border-left:1px solid #000;text-align:right;border-bottom:1px solid #000;border-right:1px solid #000;"><?php echo mask_money($subtotal_all, 2); ?></td>
            </tr>
            <?php if($data->ppn > 0):
              $grand_total = $grand_total + (($subtotal_all * ($data->ppn/100)));
              ?>
            <tr>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td colspan="2" style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">PPN (<?php echo mask_money($data->ppn, 2); ?>)</td>
              <td style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right;border-right:1px solid #000;"><?php echo mask_money((($subtotal_all * ($data->ppn/100))), 2); ?></td>
            </tr>
            <?php endif; ?>
            <?php if($data->shipping_cost > 0):
              $grand_total = $grand_total + $data->shipping_cost;
              ?>
            <tr>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td colspan="2" style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">SHIPPING COST</td>
              <td style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right;border-right:1px solid #000;"><?php echo mask_money($data->shipping_cost); ?></td>
            </tr>
            <?php endif; ?>
            <?php if($data->other_cost > 0):
              $grand_total = $grand_total + $data->other_cost;
              ?>
            <tr>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td colspan="2" style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">OTHER COST</td>
              <td style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right;border-right:1px solid #000;"><?php echo mask_money($data->other_cost); ?></td>
            </tr>
            <?php endif; ?>
            <?php if($data->deduction_dp > 0):
              $grand_total = $grand_total - $data->deduction_dp;
              ?>
            <tr>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td colspan="2" style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">DEDUCTION DP</td>
              <td style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right;border-right:1px solid #000;">( <?php echo mask_money($data->deduction_dp); ?> )</td>
            </tr>
            <?php endif; ?>
            <tr>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td style="padding:5px;border:none">&nbsp;</td>
              <td colspan="2" style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right">GRAND TOTAL</td>
              <td style="padding:5px;border-left:1px solid #000;border-bottom:1px solid #000;text-align:right;border-right:1px solid #000;"><?php echo mask_money($grand_total); ?></td>
            </tr>
          </table>
        </div>
        <div class="print-footer">
          <hr style="border-top:1px solid #000;border-bottom:none;">
          <table style="width:100%">
            <tr>
              <td valign="top" style="width:50%" rowspan="2">
                <div style="border:1px solid #000;width:100%;padding:5px">
                  <?php echo $data->iv_note; ?>
                </div>
              </td>
              <td valign="top" ></td>
              <td valign="top" style="text-align:center;">
                Make all checks payable to
                <br><b><?php echo get_setting("company_name"); ?></b>
                <div style="overflow:visible;height:60px;text-align:center">
                  <img src="<?php echo "./assets/img/cap.png"; ?>" alt="" style="width:180px;position:absolute;top:-20;">
                </div>
              </td>
            </tr>
            <tr>
              <td valign="top" </td>
              <td valign="top" style="text-align:center;"><b>MUHAMMAD FIRMAN</b></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
