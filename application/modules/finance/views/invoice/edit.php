<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Finance - Invoice - Edit</title>
  <?php echo assets_top(); ?>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"); ?>">

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>Finance</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-money"></i> Finance</a></li>
        <li><a href="<?php echo set_url("finance", "invoice"); ?>">Invoice</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <form action="<?php echo set_url("finance", "invoice", "update"); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Invoice</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Invoice#</label>
                      <input type="text" class="form-control" id="iv_number" name="iv_number" placeholder="" value="<?php echo $edit->iv_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>Invoice Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" id="iv_date" name="iv_date" placeholder="" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask value="<?php  echo ($edit->iv_date != "0000-00-00")? $edit->iv_date : ""; ?>">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Due Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" id="due_date" name="due_date" placeholder="" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask value="<?php  echo ($edit->due_date != "0000-00-00")? $edit->due_date : ""; ?>">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Customer *</label>
                      <input type="text" class="form-control" value="<?php echo $edit->cust_name; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>Sales Order#</label>
                      <input type="text" class="form-control" value="<?php echo $edit->so_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>PO#</label>
                      <input type="text" class="form-control" value="<?php echo $edit->po_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>Billing Address</label>
                      <textarea class="form-control" id="billing_address" name="billing_address" rows="4" cols="20"><?php echo $edit->billing_address; ?></textarea>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-responsive" id="tmp_item_table">
                        <thead>
                          <tr>
                            <th style="min-width:200px">Delivery Order</th>
                            <th style="min-width:200px">Item Description</th>
                            <th style="min-width:100px">Qty</th>
                            <th style="min-width:100px">Unit</th>
                            <th style="min-width:100px">Unit Price</th>
                            <th style="min-width:100px">Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $subtotal_all = 0;
                          if($edit->items):
                          foreach ($edit->items as $row):
                            $subtotal_all = $subtotal_all + ($row->qty * $row->unit_price);
                            ?>
                          <tr>
                            <td><?php echo $row->do_number; ?></td>
                            <td>
                              <b><?php echo $row->item_code; ?></b>
                              <br><?php echo $row->item_name; ?>
                              <br><?php echo str_replace("\n", "<br>", $row->item_description); ?>
                            </td>
                            <td class="text-right"><?php echo mask_money($row->qty); ?></td>
                            <td class="text-center"><?php echo $row->unit; ?></td>
                            <td class="text-right"><?php echo mask_money($row->unit_price); ?></td>
                            <td class="text-right"><?php echo mask_money(($row->qty * $row->unit_price)); ?></td>
                          </tr>
                          <?php endforeach;
                        endif; ?>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-md-6">

                      </div>
                      <div class="col-md-6">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th style="line-height: 33px;">Subtotal</th>
                              <td>
                                <input type="text" name="subtotal_all" id="subtotal_all" class="form-control text-right" value="<?php echo mask_money($subtotal_all); ?>" readonly>
                              </td>
                            </tr>
                            <tr>
                              <th class="col-md-6" style="line-height: 33px;">

                                <div class="row">
                                  <div class="col-md-6">
                                    PPN
                                  </div>
                                  <div class="col-md-6">
                                    <div class="input-group">
                                      <input type="text" name="ppn" id="ppn" class="form-control text-right" value="<?php echo mask_money($edit->ppn); ?>">
                                      <span class="input-group-addon">%</span>
                                    </div>
                                  </div>
                                </div>
                              </th>
                              <td class="col-md-6">
                                <input type="text" name="ppn_total" id="ppn_total" class="form-control text-right" value="" readonly>
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Shipping Cost</th>
                              <td>
                                <input type="text" name="shipping_cost" id="shipping_cost" class="form-control text-right" value="<?php echo mask_money($edit->shipping_cost); ?>">
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Other Cost</th>
                              <td>
                                <input type="text" name="other_cost" id="other_cost" class="form-control text-right" value="<?php echo mask_money($edit->other_cost); ?>">
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Deduction DP</th>
                              <td>
                                <div class="input-group">
                                  <span class="input-group-addon">(</span>
                                  <input type="text" name="deduction_dp" id="deduction_dp" class="form-control text-right" value="<?php echo mask_money($edit->deduction_dp); ?>">
                                  <span class="input-group-addon">)</span>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th style="line-height: 33px;">Grand Total</th>
                              <td>
                                <input type="text" name="total" id="total" class="form-control text-right" value="" readonly>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Note</label>
                      <textarea class="form-control textarea" name="iv_note" id="iv_note" rows="8" cols="80"><?php echo $edit->iv_note; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("finance", "invoice"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<!-- CK Editor -->
<script src="<?php echo base_url("assets/bower_components/ckeditor/ckeditor.js"); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"); ?>"></script>

<script>
$(function () {
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})
</script>

<script>
$('#iv_date').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
$('#due_date').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
</script>

<script type="text/javascript">
function calculateSubtotal(){
  var subtotal_all = 0;
  var grand_total = 0;
  var ppn = 0;
  var ppn_total = 0;
  var shipping_cost = 0;
  var other_cost = 0;
  var deduction_dp = 0;

  if ($("#subtotal_all").length) {
    subtotal_all = $("#subtotal_all").numberFormat("getValue");
  }
  if ($("#ppn").length) {
    ppn = $("#ppn").numberFormat("getValue");
    ppn_total = (subtotal_all * (ppn/100));
  }
  if ($("#shipping_cost").length) {
    shipping_cost = $("#shipping_cost").numberFormat("getValue");
  }
  if ($("#other_cost").length) {
    other_cost = $("#other_cost").numberFormat("getValue");
  }
  if ($("#deduction_dp").length) {
    deduction_dp = $("#deduction_dp").numberFormat("getValue");
  }

  $("#ppn_total").numberFormat("setValue", ppn_total);

  grand_total =  (subtotal_all + ppn_total) + (shipping_cost + other_cost) - (deduction_dp);

  $("#total").numberFormat("setValue", grand_total);

  $("#ppn_total").on('focusin', function(event) { $(this).select(); });

  $("#ppn").numberFormat();
  $("#ppn_total").numberFormat();
  $("#subtotal_all").numberFormat();
  $("#shipping_cost").numberFormat();
  $("#other_cost").numberFormat();
  $("#deduction_dp").numberFormat();
  $("#total").numberFormat();
}

$("input").on('change', function(event) {
  calculateSubtotal();
});

$(document).ready(function() {
  calculateSubtotal();
});
</script>
</body>
</html>
