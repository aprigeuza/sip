<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Finance - Payment - Edit</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment
        <small>Finance</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-money"></i> Finance</a></li>
        <li><a href="<?php echo set_url("finance", "payment"); ?>">Payment</a></li>
        <li class="active">Input Payment</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <?php echo show_alert(); ?>
          <form id="fm_input_payment">
            <input type="hidden" name="invoice_id" id="invoice_id" value="<?php echo $invoice->id; ?>">

            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Input Payment</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="form-group">
                  <label>Payment Date *</label>
                  <input type="text" class="form-control" id="payment_date" name="payment_date">
                </div>
                <div class="form-group">
                  <label>Payment Type *</label>
                  <input type="text" class="form-control" id="payment_type" name="payment_type" placeholder="Cash/Transfer">
                </div>
                <div class="form-group">
                  <label>Payment Proof *</label>
                  <input type="text" class="form-control" id="payment_proof" name="payment_proof" placeholder="Bukti Pembayaran">
                </div>
                <div class="form-group">
                  <label>Amount *</label>
                  <input type="text" class="form-control numberFormat text-right" id="amount" name="amount">
                </div>
                <div class="form-group">
                  <label>Remark</label>
                  <input type="text" class="form-control" id="remark" name="remark" placeholder="Remark">
                </div>

                <button type="button" name="button" class="btn btn-primary" onclick="savePayment();">Submit</button>
                <br>
                <br><b>List Of Payment : </b><br>
                <a href="javascript:;" onclick="getPaymentList();">Reload</a> <br>
                <div class="row">
                  <div class="col-md-12">
                    <div id="payment_list">

                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <a href="<?php echo set_url("finance", "payment"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
                <a href="javascript:;" onclick="setClosed('<?php echo $invoice->id; ?>');" class="btn btn-primary">Close Invoice</a>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-6">
          <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-money"></i>

                <h3 class="box-title">Invoice Detail</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <dl class="dl-horizontal">
                  <dt>Invoice ID</dt>
                  <dd><?php echo $invoice->id; ?></dd>
                  <dt>Invoice#</dt>
                  <dd><?php echo $invoice->iv_number; ?></dd>
                  <dt>Invoice Date</dt>
                  <dd><?php echo $invoice->iv_date; ?></dd>
                  <dt>Due Date</dt>
                  <dd><?php echo $invoice->due_date; ?></dd>
                  <dt>Customer</dt>
                  <dd>[ <?php echo $invoice->cust_code; ?> ] <?php echo $invoice->cust_name; ?></dd>
                  <dt>Billing Address</dt>
                  <dd><?php echo str_replace("\n", "<br>", $invoice->billing_address); ?></dd>
                </dl>
                <div class="row">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-bordered table-responsive" id="tmp_item_table">
                        <thead>
                          <tr>
                            <th style="min-width:50px">#</th>
                            <th style="min-width:200px">Order</th>
                            <th style="min-width:200px">Item Description</th>
                            <th style="min-width:100px">Qty</th>
                            <th style="min-width:100px">Unit</th>
                            <th style="min-width:100px">Unit Price</th>
                            <th style="min-width:100px">Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $grand_total = 0;
                          $no=1;
                          $subtotal_all = 0;
                          if($invoice->items):
                          foreach ($invoice->items as $row):
                            $subtotal_all = $subtotal_all + ($row->qty * $row->unit_price);
                            ?>
                          <tr>
                            <td class="text-center"><?php echo $no++; ?></td>
                            <td>
                              SO : <?php echo $row->so_number; ?><br>
                              PO : <?php echo $row->po_number; ?><br>
                              DO : <?php echo $row->do_number; ?></td>
                            <td>
                              <b><?php echo $row->item_code; ?></b>
                              <br><?php echo $row->item_name; ?>
                              <br><?php echo str_replace("\n", "<br>", $row->item_description); ?>
                            </td>
                            <td class="text-right"><?php echo mask_money($row->qty); ?></td>
                            <td class="text-center"><?php echo $row->unit; ?></td>
                            <td class="text-right"><?php echo mask_money($row->unit_price); ?></td>
                            <td class="text-right"><?php echo mask_money(($row->qty * $row->unit_price)); ?></td>
                          </tr>
                          <?php endforeach;
                        endif; ?>
                        </tbody>
                      </table>
                    </div>
                    <hr>
                    <?php
                    $grand_total = $grand_total + $subtotal_all;
                    ?>
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <th class="text-right" style="line-height: 33px;">Subtotal</th>
                          <td class="text-right"><?php echo mask_money($subtotal_all); ?></td>
                        </tr>
                        <?php if($invoice->ppn > 0):
                          $grand_total = $grand_total + (($subtotal_all * ($invoice->ppn/100)));
                          ?>
                        <tr>
                          <th class="text-right" style="line-height: 33px;">PPN (<?php echo mask_money($invoice->ppn); ?> %)</th>
                          <td class="text-right"><?php echo mask_money($subtotal_all * ($invoice->ppn / 100)); ?></td>
                        </tr>
                        <?php endif; ?>
                        <?php if($invoice->shipping_cost > 0):
                          $grand_total = $grand_total + $invoice->shipping_cost;
                          ?>
                        <tr>
                          <th class="text-right" style="line-height: 33px;">Shipping Cost</th>
                          <td class="text-right"><?php echo mask_money($invoice->shipping_cost); ?></td>
                        </tr>
                        <?php endif; ?>
                        <?php if($invoice->other_cost > 0):
                          $grand_total = $grand_total + $invoice->other_cost;
                          ?>
                        <tr>
                          <th class="text-right" style="line-height: 33px;">Other Cost</th>
                          <td class="text-right"><?php echo mask_money($invoice->other_cost); ?></td>
                        </tr>
                        <?php endif; ?>
                        <?php if($invoice->deduction_dp > 0):
                          $grand_total = $grand_total - $invoice->deduction_dp;
                          ?>
                        <tr>
                          <th class="text-right" style="line-height: 33px;">Deduction DP</th>
                          <td class="text-right">( <?php echo mask_money($invoice->deduction_dp); ?> )</td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                          <th class="text-right" style="line-height: 33px;font-size:16px;font-weight:bold">Grand Total</th>
                          <td class="text-right" style="font-size:16px;font-weight:bold"><?php echo mask_money($grand_total); ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$('#payment_date').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
</script>
<script>

  $(document).ready(function() {
    getPaymentList();
  });

  function getPaymentList(){
    var payment_list = $("#payment_list");
    var invoice_id = '<?php echo $invoice->id; ?>';
    $.ajax({
      url: '<?php echo set_url("finance", "payment", "get_payment_list"); ?>',
      type: 'GET',
      dataType: 'JSON',
      data:{invoice_id:invoice_id},
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          payment_list.html(response.html);
        } else {
          payment_list.html("");
        }

        loadingSpinner.hide();
      }
    });
  }

  function savePayment(){
    var fm = $("#fm_input_payment");

    $.ajax({
      url: '<?php echo set_url("finance", "payment", "save"); ?>',
      type: 'POST',
      dataType: 'JSON',
      data:fm.serializeArray(),
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){

        alertify.notify(response.message);

        getPaymentList();

        loadingSpinner.hide();
      }
    });
  }

  function cancelPayment(id){
    if(confirm('Cancel Payment?')){
      $.ajax({
        url: '<?php echo set_url("finance", "payment", "cancel"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data:{id:id},
        beforeSend: function(){
          loadingSpinner.show();
        },
        success: function(response){
          alertify.warning(response.message);
          getPaymentList();

          loadingSpinner.hide();
        }
      });
    }
  }
</script>

<script>
  function setClosed(invoice_id){
    if(confirm('Close Document?')){
      $.ajax({
        url: '<?php echo set_url("finance", "payment", "closed"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data: {invoice_id: invoice_id},
        success: function(response){
          if(response.status == true){
            alert(response.message)
            window.location = '<?php echo set_url("finance", "payment"); ?>';
          }else{
            alertify.error(response.message);
          }
        }
      });
    }
  }
</script>
</body>
</html>
