<div class="table-responsive">
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>#</th>
        <th>Date</th>
        <th>Type</th>
        <th>Proof</th>
        <th>Amount</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      $total_amount = 0;
      foreach($payment_list->result() as $row):
        $total_amount = $total_amount + $row->amount;
        ?>
      <tr>
        <td class="text-center">
          <button type="button" name="button" class="btn btn-xs btn-danger" onclick="cancelPayment('<?php echo $row->id; ?>')"><i class="fa fa-times"></i></button>
        </td>
        <td><?php echo $row->payment_date; ?></td>
        <td><?php echo $row->payment_type; ?></td>
        <td><?php echo $row->payment_proof; ?></td>
        <td class="text-right"><?php echo mask_money($row->amount); ?></td>
      </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-right" style="font-size:16px;font-weight:bold;">Total</td>
        <td class="text-right" style="font-size:16px;font-weight:bold;"><?php echo mask_money($total_amount); ?></td>
      </tr>
    </tfoot>
  </table>

</div>
