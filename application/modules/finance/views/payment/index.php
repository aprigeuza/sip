<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Payment - Finance</title>
  <?php echo assets_top(); ?>

  <style media="screen">
    table td, table th{
      white-space: nowrap;
    }
  </style>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment
        <small>Finance</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-money"></i> Finance</a></li>
        <li class="active">Payment</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Payment Invoice</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="table1" style="width:100%">
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  var table1;
  $(function () {
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("finance", "payment", "datatable_invoice"); ?>',
      "columns":[
        { "data": "iv_number", "title": "Invoice#"},
        { "data": "iv_date", "title": "Invoice Date"},
        { "data": "cust_code", "title": "Customer#"},
        { "data": "cust_name", "title": "Customer"},
        { "data": "total_invoice", "title": "Total Invoice", className: "text-right",},
        { "data": "total_ppn", "title": "Total PPN", className: "text-right",},
        { "data": "shipping_cost", "title": "Shipping Cost", className: "text-right",},
        { "data": "other_cost", "title": "Other Cost", className: "text-right",},
        { "data": "deduction_dp", "title": "Deduction DP", className: "text-right",},
        { "data": "grand_total_invoice", "title": "Grand Total Invoice", className: "text-right",},
        { "data": "total_payment", "title": "Total Payment", className: "text-right",},
        { "data": "total_outstanding", "title": "Total Outstanding", className: "text-right",},
        {
          data: null,
          title: "Options",
          className: "text-center",
          "createdCell": function (td, cellData, rowData, row, col) {
              var html = "";
              html += '<a href="<?php echo set_url("finance", "payment", "input_payment"); ?>?id='+rowData.id+'" class="btn btn-xs btn-primary">Input Payment</a> ';
              html += '<a href="javascript:;" onclick="setClosed(\''+rowData.id+'\')" class="btn btn-xs btn-success">Set Closed</a> ';
              // html += '<a href="<?php echo set_url("finance", "payment", "print_preview"); ?>?id='+rowData.id+'" class="btn btn-xs btn-info" target="_blank">Print</a> ';
              $(td).html(html);
          },
        }
      ],
    });
  })
</script>
<script>
  function setClosed(invoice_id){
    if(confirm('Close Document?')){
      $.ajax({
        url: '<?php echo set_url("finance", "payment", "closed"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data: {invoice_id: invoice_id},
        success: function(response){
          if(response.status == true){
            alertify.success(response.message);
          }else{
            alertify.error(response.message);
          }
          table1.ajax.reload();
        }
      });
    }
  }
</script>

</body>
</html>
