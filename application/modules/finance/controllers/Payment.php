<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("invoice_model");
    $this->load->model("payment_model");
  }

  public function index()
  {
    $data = array();

    $this->load->view("payment/index", $data);
  }

  public function input_payment()
  {
    $data = array();
    $invoice_id = $this->input->get("id");

    $data["invoice"] = $this->invoice_model->get_data($invoice_id);

    $this->load->view("payment/input_payment", $data);
  }

  public function datatable_invoice()
  {
    $config["table"] = "v_invoice_payment";
    $where_all[] = "posted = 1";
    $where_all[] = "closed = 0";
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function get_payment_list()
  {
    $invoice_id = $this->input->get("invoice_id");

    $payment_list = $this->payment_model->get_payment_list($invoice_id);

    $data["payment_list"] = $payment_list;
    $response["status"] = true;
    $response["html"] = $this->load->view("payment/payment_list", $data, TRUE);
    json_file($response);
  }

  public function save()
  {
    $invoice_id = $this->input->post("invoice_id");
    $payment_date = $this->input->post("payment_date");
    if($payment_date == NULL) json_file(["status" => false, "message" => "Payment Date Empty!"]);
    $payment_type = $this->input->post("payment_type");
    if($payment_type == NULL) json_file(["status" => false, "message" => "Payment Type Empty!"]);
    $payment_proof = $this->input->post("payment_proof");
    if($payment_proof == NULL) json_file(["status" => false, "message" => "Payment Proof Empty!"]);
    $amount = $this->input->post("amount");
    if($amount == NULL) json_file(["status" => false, "message" => "Amount Empty!"]);
    $remark = $this->input->post("remark");

    $data["invoice_id"] = $invoice_id;
    $data["payment_date"] = $payment_date;
    $data["payment_type"] = $payment_type;
    $data["payment_proof"] = $payment_proof;
    $data["amount"] = unmask_money($amount);
    $data["remark"] = $remark;
    $data["cancel"] = 0;
    $data["input_date"] = date("Y-m-d");
    $data["input_by"] = get_session("uid");

    $this->payment_model->insert($data);
    $response["status"] = true;
    $response["message"] = "Success!";
    json_file($response);
  }

  public function cancel()
  {
    $id = $this->input->post("id");
    $this->payment_model->cancel($id);
    $response["status"] = true;
    $response["message"] = "Success!";
    json_file($response);
  }

  public function closed()
  {
    $invoice_id = $this->input->post("invoice_id");
    $this->payment_model->closed_invoice($invoice_id);
    $response["status"] = true;
    $response["message"] = "Success!";
    json_file($response);
  }

}
