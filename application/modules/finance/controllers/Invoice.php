<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    validate_access();

    $this->load->model("invoice_model");
    $this->load->model("customer_model");
    $this->load->model("item_model");
    $this->load->model("temp_data_model");
  }

  public function index()
  {
    $data = array();
    $data["list_customer"] = $this->customer_model->get_list_active();
    $this->load->view("invoice/index", $data);
  }

  public function add()
  {
    $id = $this->invoice_model->insert_draft();

    redirect("finance/invoice/edit?id=" . $id);
  }

  public function prepare_add()
  {
    $checked_id = $this->input->post("checked_id");
    $sales_order_id = $this->input->post("sales_order_id");
    if(is_array($checked_id) && count($checked_id) >= 1)
    {
      set_session("finance_invoice_checked_id", $checked_id);
      set_session("finance_invoice_sales_order_id", $sales_order_id);

      $response["status"] = true;
      $response["message"] = "Success!";
      $response["url"] = set_url("finance", "invoice", "add");
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Empty Checked!";
      $response["url"] = null;
    }

    json_file($response);
  }

  public function edit()
  {
    // Clear Temp
    $this->temp_data_model->delete_by_key("finance_invoice_edit");

    $id = $this->input->get("id");

    $data["new_number"] = $this->invoice_model->new_number();

    $edit = $this->invoice_model->get_data($id);
    if($edit->posted == false)
    {
      $data["edit"] = $edit;
      // Insert Temp
      $this->temp_data_model->delete_by_key("finance_invoice_edit");
      foreach($edit->items as $item)
      {
        $_temp_data["data"] = json_encode($item);
        $_temp_data["key"] = "finance_invoice_edit";
        $this->temp_data_model->insert($_temp_data);
      }

      $this->load->view("invoice/edit", $data);
    }
    else
    {
      set_alert("danger", "The document number \"".$edit->iv_number."\" can't be edit, because the document has been Posted!");
      redirect("finance/invoice");
    }

  }

  public function print_preview()
  {
    $this->load->library("dompdf_lib");
    $id = $this->input->get("id");

    $data = array();
    $res = $this->invoice_model->get_data($id);
    $data["data"] = $res;
    $this->dompdf_lib->setPaper('A4', 'potrait');
    $this->dompdf_lib->generate('invoice/print_preview', $data, "IV" . $res->iv_number . "-".$res->cust_name.".pdf");
  }

  public function datatable()
  {
    $config["table"] = "v_invoice";
    $where_all[] = "cancel = 0";
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function datatable_delivery_order()
  {
    $config["table"] = "v_delivery_order_for_invoice";
    $where_all[] = "cancel = 0";
    $where_all[] = "posted = 1";
    $where_all[] = "sales_order_id = " . $this->input->post_get("sales_order_id");
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  // Action
  public function update()
  {
    $id = $this->input->post("id");

    $billing_address = $this->input->post("billing_address");
    $iv_note = $this->input->post("iv_note");
    $input_date = date("Y-m-d H:i:s");
    $input_by = get_session("uid");
    $ppn = $this->input->post("ppn");
    $iv_date = $this->input->post("iv_date");
    $due_date = $this->input->post("due_date");
    $shipping_cost = $this->input->post("shipping_cost");
    $other_cost = $this->input->post("other_cost");
    $deduction_dp = $this->input->post("deduction_dp");

    $data["iv_date"] = $iv_date;
    $data["due_date"] = $due_date;
    $data["ppn"] = unmask_money($ppn);
    $data["shipping_cost"] = unmask_money($shipping_cost);
    $data["other_cost"] = unmask_money($other_cost);
    $data["deduction_dp"] = unmask_money($deduction_dp);
    $data["billing_address"] = $billing_address;
    $data["iv_note"] = $iv_note;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->invoice_model->update($data, $id);

    redirect("finance/invoice");
  }

  public function cancel()
  {
    $id = $this->input->post_get("id");
    $this->invoice_model->cancel($id);
    $response["status"] = true;
    $response["message"] = "Document Canceled!";
    json_file($response);
  }

  public function posted()
  {
    $id = $this->input->post_get("id");
    $act = $this->invoice_model->posted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because document is canceled!";
    }
    json_file($response);
  }

  public function unposted()
  {
    $id = $this->input->post_get("id");
    $act = $this->invoice_model->unposted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Un-posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because there is a related delivery order!";
    }
    json_file($response);
  }

  public function temp_save()
  {
    $temp_id = $this->input->post("temp_id");

    $temp_item_id = $this->input->post("temp_item_id");
    $temp_item_code = $this->input->post("temp_item_code");
    $temp_item_name = $this->input->post("temp_item_name");
    $temp_item_qty = $this->input->post("temp_item_qty");
    $temp_item_description = $this->input->post("temp_item_description");
    $temp_item_unit = $this->input->post("temp_item_unit");
    $temp_item_unit_price = $this->input->post("temp_item_unit_price");
    $temp_delivery_order_item_id = $this->input->post("temp_delivery_order_item_id");

    $temp_data["item_id"] = $temp_item_id;
    $temp_data["temp_delivery_order_item_id"] = $temp_delivery_order_item_id;
    $temp_data["item_code"] = $temp_item_code;
    $temp_data["item_name"] = $temp_item_name;
    $temp_data["qty"] = unmask_money($temp_item_qty);
    $temp_data["item_description"] = $temp_item_description;
    $temp_data["unit"] = $temp_item_unit;
    $temp_data["unit_price"] = unmask_money($temp_item_unit_price);

    $data["data"] = json_encode($temp_data);
    $data["key"] = "finance_invoice_edit";

    if($temp_id)
    {
      $res = $this->temp_data_model->update($data, $temp_id);
    }
    else
    {
      $res = $this->temp_data_model->insert($data);
    }


    if($res)
    {
      $response["status"] = TRUE;
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Failed!";
    }
    json_file($response);
  }

  public function temp_data()
  {
    $id = $this->input->post_get("id");

    $data = $this->temp_data_model->get_by_id($id);

    $response["status"] = TRUE;
    $response["id"] = $id;
    $response["data"] = json_decode($data);
    $response["message"] = "Success!";
    json_file($response);
  }

  public function temp_table()
  {
    $res = $this->temp_data_model->get_by_key("finance_invoice_edit");
    if($res !== FALSE)
    {
      $data["temp_data"] = $res;
      $response["status"] = TRUE;
      $response["html"] = $this->load->view("finance/invoice/temp_table", $data, TRUE);
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["html"] = "";
      $response["message"] = "Not Found!";
    }
    json_file($response);
  }

  public function temp_delete()
  {
    $id = $this->input->post_get("id");

    $this->temp_data_model->delete_by_id($id);

    $response["status"] = TRUE;
    $response["message"] = "Success!";
    json_file($response);
  }

}
