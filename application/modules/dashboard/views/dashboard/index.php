<!DOCTYPE html>
<html>
<head>
  <title>Dashboard</title>
  <?php echo assets_top(); ?>

  <style media="screen">
    .info-box-icon:hover i, .btn-shortcut:hover i{
      font-size: 40px;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">
          <div id="carousel-dashboard" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="<?php echo get_setting("dashboard_background"); ?>" alt="">
              </div>

            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <a title="Go to page.." href="<?php echo set_url("sales", "customer"); ?>"><span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span></a>

            <div class="info-box-content">
              <span class="info-box-text">Active Customers</span>
              <span class="info-box-number" id="total_customer">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <a title="Go to page.." href="<?php echo set_url("sales", "sales_order"); ?>"><span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span></a>

            <div class="info-box-content">
              <span class="info-box-text">Total Sales</span>
              <span class="info-box-number" id="total_sales">0</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Invoice</span>
              <span class="info-box-number" id="total_invoice"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <hr class="line-separator">
      <!-- <h3><i class="fa fa-link"></i> Shortcut</h3> -->
      <br>
      <div class="row">
        <div class="col-md-3">
          <a href="<?php echo set_url("sales/customer/add"); ?>" class="btn btn-block btn-primary btn-shortcut" style="font-size:14px; padding-top:30px;padding-bottom:30px;border-radius:0"><i class="fa fa-users fa-fw fa-3x"></i><hr style="border:1px #ddd solid;">New Customer</a>
        </div>

        <div class="col-md-3">
          <a href="<?php echo set_url("sales/quotation/add"); ?>" class="btn btn-block btn-warning btn-shortcut" style="font-size:14px; padding-top:30px;padding-bottom:30px;border-radius:0"><i class="fa fa-file-o fa-fw fa-3x"></i><hr style="border:1px #ddd solid;">New Quotation</a>
        </div>

        <div class="col-md-3">
          <a href="<?php echo set_url("procurement/sales_order/add"); ?>" class="btn btn-block btn-danger btn-shortcut" style="font-size:14px; padding-top:30px;padding-bottom:30px;border-radius:0"><i class="fa fa-shopping-cart fa-fw fa-3x"></i><hr style="border:1px #ddd solid;">New Sales Order</a>
        </div>

        <div class="col-md-3">
          <a href="<?php echo set_url("procurement/delivery_order/add"); ?>" class="btn btn-block btn-success btn-shortcut" style="font-size:14px; padding-top:30px;padding-bottom:30px;border-radius:0"><i class="fa fa-truck fa-fw fa-3x"></i><hr style="border:1px #ddd solid;">New Delivery Order</a>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$(document).ready(function() {
  $.ajax({
    url: '<?php echo set_url("api", "dashboard_data"); ?>',
    type: 'GET',
    dataType: 'JSON',
    beforeSend: function(){
      $("#total_customer").html("calculating..");
      $("#total_sales").html("calculating..");
    },
    success: function(response){
      if(response.data)
      {
        var data = response.data;

        $("#total_customer").html(number_format(data.total_customer));
        $("#total_sales").html(number_format(data.total_sales));
        $("#total_invoice").html(number_format(data.total_invoice));
      }
    }
  });
});
</script>
</body>
</html>
