<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Supplier - Procurement</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Supplier
        <small>Procurement</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-database"></i> Procurement</a></li>
        <li class="active">Supplier</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <div class="row">
            <div class="col-md-12">
              <a href="<?php echo set_url("procurement", "supplier", "add"); ?>" class="btn btn-primary" title="add new"><i class="fa fa-file-o"></i> Add New</a>
              <a href="<?php echo set_url("procurement", "supplier"); ?>" class="btn btn-info" title="Reload"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
          </div>
          <hr class="line-separator">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table1">
                <thead>
                  <tr>
                    <th>SUPP#</th>
                    <th class="col-md-6">Supplier</th>
                    <th>Obsolete</th>
                    <th>Created By</th>
                    <th>Created Date</th>
                    <th>Option</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($table_data->result() as $row): ?>
                  <tr<?php if($row->obsolete) echo ' class="text-danger"'; ?>>
                    <td><?php echo $row->supp_code; ?></td>
                    <td><?php echo $row->supp_name; ?></td>
                    <td class="text-center"><?php echo ($row->obsolete)?'YES':'NO'; ?></td>
                    <td><?php echo $row->username; ?></td>
                    <td><?php echo indo_datetime($row->input_date); ?></td>
                    <td>
                      <a href="<?php echo set_url("procurement", "supplier", "edit")."?id=".$row->id; ?>" class="btn btn-primary btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                      <?php if($row->obsolete): ?>
                      <a href="<?php echo set_url("procurement", "supplier", "obsolete")."?id=".$row->id; ?>" class="btn btn-success btn-sm" title="Un-Obsolete" onclick="return confirm('Un-Obsolete <?php echo $row->supp_name; ?>?');"><i class="fa fa-check"></i></a>
                      <?php else: ?>
                      <a href="<?php echo set_url("procurement", "supplier", "obsolete")."?id=".$row->id; ?>" class="btn btn-danger btn-sm" title="Obsolete" onclick="return confirm('Obsolete <?php echo $row->supp_name; ?>?');"><i class="fa fa-ban"></i></a>
                      <?php endif; ?>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  $(function () {
    $('#table1').DataTable()
  })
</script>

</body>
</html>
