<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Print Preview - Delivery Order</title>
    <style media="screen">
    @page { margin: 0px; }
      body { margin: 0px; }
      body{
        /* font-family: verdana; */
        font-size: 12px;
      }
      .paper{
        width: 100%;
        height: auto;
        margin-left: 0;
        margin-right: 0;
      }
      .print-container{
        margin-left: 30px;
        margin-right: 30px;
      }
      .print-header{
        padding-top:15px;
        padding-bottom: 15px;
        /* height: 240px; */
      }
      .print-body{
        /* height: 500px; */
      }
      .print-footer{
        height: 200px;
      }
      table {
           border-collapse: collapse;
       }
       table td{
         padding: 5px;
       }
       .unposted{
         width: 300px;
         position: absolute;
         display: block;
         font-size: 40px;
         z-index: 99999999;
         color: #dddd;
         top:50%;
         bottom:50%;
         left: 35%;
         right: 50%;
         margin-left: auto;
         margin-right: auto;
       }
    </style>
  </head>
  <body>
    <div class="paper">
      <?php if($data->posted == false): ?>
      <div class="unposted">
        <span>UN-POSTED</span>
      </div>
      <?php endif; ?>
      <?php if($data->cancel == true): ?>
      <div class="unposted">
        <span>CANCELED</span>
      </div>
      <?php endif; ?>
      <div class="print-container">
        <div class="print-header">
          <table style="width:100%;margin-bottom:15px">
            <tr>
              <td style="width:10%" valign="top">
                <img src="<?php echo "./assets/img/logo.png"; ?>" alt="" style="width:80px">
              </td>
              <td style="width:40%;text-align:left;" valign="center">
                <h1 style="margin-top:5px;margin-bottom:5px;font-size:18px"><?php echo get_setting("company_name"); ?></h1>
                <p style="margin-top:5px;margin-bottom:5px;"><?php echo get_setting("company_slogan"); ?>
                </p>
              </td>
              <td style="width:50%;">
                <h2 style="margin-top:5px;margin-bottom:5px;text-align:right;font-size:18px">DELIVERY ORDER</h2>
                <table style="font-size:12px;width:100%;margin-top:20px;">
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:40%;text-align:right;">Delivery Order#</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:40%; border:1px solid #000;text-align:center;"><?php echo $data->do_number; ?></td>
                  </tr>
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:30%;text-align:right;">Delivery Order Date</td>
                    <td style="padding-top:0px;padding-bottom:0px;border:1px solid #000;text-align:center;"><?php echo indo_date($data->do_date); ?></td>
                  </tr>
                  <tr>
                    <td style="padding-top:0px;padding-bottom:0px;">&nbsp;</td>
                    <td style="padding-top:0px;padding-bottom:0px;width:30%;text-align:right;">PO#</td>
                    <td style="padding-top:0px;padding-bottom:0px;border:1px solid #000;text-align:center;"><?php echo $data->po_number; ?></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table style="width:100%">
            <tr>
              <td style="width:45%;text-align:left;line-height:22px;">
                <b>From</b>: <br>
                <?php echo get_setting("company_name"); ?>
                <p style="margin-top:5px;margin-bottom:5px;font-size:12px;line-height:15px">
                  <?php echo str_replace("\n", "<br>", get_setting("office_address")); ?><br>
                  Telp : <?php echo get_setting("contact_telp"); ?> <br>
                  Email : <?php echo get_setting("contact_email"); ?>
                </p>

              </td>
              <td></td>
              <td style="width:45%;text-align:left;line-height:22px;">
                <b>Delivery To</b> :<br>
                <?php echo $data->cust_name; ?>
                <p style="margin-top:5px;margin-bottom:5px;font-size:12px;line-height:15px">
                  <?php echo str_replace("\n", "<br>", $data->shipping_address); ?> <br>
                  Telp : <?php echo $data->telp; ?>
              </p>
              </td>
            </tr>
          </table>
        </div>
        <div class="print-body">
          <table style="width:100%" cellpadding="0" rowpadding="0">
            <tr>
              <th style="width:5%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">NO</th>
              <th style="width:65%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">DESCRIPTION</th>
              <th style="width:10%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">QUANTITY</th>
              <th style="width:10%;text-align:center;border:1px solid #000;padding-top:5px;padding-bottom:5px;">UNIT</th>
            </tr>
            <?php $no = 1; ?>
            <?php for($i=0; $i<=8;$i++): ?>

            <?php
            $item_description = "&nbsp;";
            $qty = "&nbsp;";
            $unit = "&nbsp;";
            $n = "&nbsp;";
            if(!empty($data->items[$i]))
            {
              $item = $data->items[$i];
              $item_description = $item->item_name . "<br>" . str_replace("\n", "<br>", $item->item_description);
              $qty = number_format($item->qty, 2);
              $unit = $item->unit;
              $n = $no++;
            }
            ?>
            <tr>
              <td valign="top" style="font-size:12px;text-align: center;border-left:1px solid #000;"><?php echo $n; ?></td>
              <td valign="top" style="font-size:12px;text-align: left;border-left:1px solid #000;"><?php echo $item_description; ?></td>
              <td valign="top" style="font-size:12px;text-align: right;border-left:1px solid #000;"><?php echo $qty; ?></td>
              <td valign="top" style="font-size:12px;text-align: center;border-left:1px solid #000;border-right:1px solid #000;"><?php echo $unit; ?></td>
            </tr>
            <?php endfor; ?>
            <tr>
              <td valign="top" style="font-size:12px;text-align: left;border-top:1px solid #000;">&nbsp;</td>
              <td valign="top" style="font-size:12px;text-align: left;border-top:1px solid #000;">&nbsp;</td>
              <td valign="top" style="font-size:12px;text-align: right;border-top:1px solid #000;">&nbsp;</td>
              <td valign="top" style="font-size:12px;text-align: center;border-top:1px solid #000;">&nbsp;</td>
            </tr>
          </table>
        </div>
        <div class="print-footer">
          <table style="width:100%">
            <tr>
              <td style="width:30%;text-align:center">SOPIR</td>
              <td>&nbsp;</td>
              <td style="width:30%;text-align:center">
                <?php echo date("d F Y", strtotime($data->do_date)); ?>
                <br>Hormat Kami,
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td style="text-align:center;overflow:visible;height:60px">
                <img src="<?php echo "./assets/img/cap.png"; ?>" alt="" style="width:180px;position:absolute;top:-50;">
              </td>
            </tr>
            <tr>
              <td style="text-align:center">( <?php echo $data->vehicle_driver; ?> )<br> <?php echo $data->vehicle_number; ?></td>
              <td>&nbsp;</td>
              <td style="text-align:center"><?php echo get_setting("company_name"); ?></td>
            </tr>
          </table>
          <hr>
          <table style="width:100%">
            <tr>
              <td>Diterima Tanggal : </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width:30%;text-align:center">Penerima</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="text-align:center">(____________________)</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
