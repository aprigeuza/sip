<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Procurement - Delivery Order - Edit</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Delivery Order
        <small>Procurement</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Procurement</a></li>
        <li><a href="<?php echo set_url("procurement", "delivery_order"); ?>">Delivery Order</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <form action="<?php echo set_url("procurement", "delivery_order", "update"); ?>" method="post">
            <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit Delivery Order</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>DO Number</label>
                      <input type="text" class="form-control" id="do_number" name="do_number" placeholder="" value="<?php echo $edit->do_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>DO Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" id="do_date" name="do_date" placeholder="" data-inputmask="'alias': 'yyyy-mm-dd'" data-mask value="<?php echo $edit->do_date; ?>">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Vehicle Number</label>
                      <input type="text" class="form-control" id="vehicle_number" name="vehicle_number" placeholder="" value="<?php echo $edit->vehicle_number; ?>">
                    </div>
                    <div class="form-group">
                      <label>Driver</label>
                      <input type="text" class="form-control" id="vehicle_driver" name="vehicle_driver" placeholder="" value="<?php echo $edit->vehicle_driver; ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Customer</label>
                      <input type="text" class="form-control" value="<?php echo $edit->cust_name; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>Shipping Address</label>
                      <textarea class="form-control" id="shipping_address" name="shipping_address" rows="4" cols="20"><?php echo $edit->shipping_address; ?></textarea>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">
                    <div id="temp_table"></div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("procurement", "delivery_order"); ?>" class="btn btn-default" title="Kembali">Kembali</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<div class="modal" id="modal_define_item" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Define Item</h4>
      </div>
      <div class="modal-body">
        <form id="fm_temp_item">
          <input type="hidden" id="temp_id" name="temp_id" value="">
          <input type="hidden" id="temp_sales_order_item_id" name="temp_sales_order_item_id" value="">
          <input type="hidden" id="temp_item_id" name="temp_item_id" value="">
          <div class="form-group">
            <label>Item Code</label>
            <input type="text" class="form-control" id="temp_item_code" name="temp_item_code" readonly>
          </div>
          <div class="form-group">
            <label>Item Name</label>
            <input type="text" class="form-control" id="temp_item_name" name="temp_item_name" readonly>
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" id="temp_item_description" name="temp_item_description" rows="4" cols="80" readonly></textarea>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Qty</label>
                <input type="text" class="form-control text-right" id="temp_item_qty" name="temp_item_qty">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Unit</label>
                <input type="text" class="form-control" id="temp_item_unit" name="temp_item_unit" readonly>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveTmpItem()">Save</button>
      </div>
    </div>
  </div>
</div>

<script>
$('#do_date').datepicker({
  autoclose: true,
  format: 'yyyy-mm-dd'
});
</script>
<script type="text/javascript">

  function getTempTable(){
    var temp_table = $("#temp_table");
    $.ajax({
      url: '<?php echo set_url("procurement", "delivery_order", "temp_table"); ?>',
      type: 'GET',
      dataType: 'JSON',
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          temp_table.html(response.html);
        } else {
          temp_table.html("");
        }

        calculateSubtotal();

        loadingSpinner.hide();
      }
    });
  }

  function resetTemp(){
    $("#temp_id").val("");
    $("#temp_item_id").val("");
    $("#temp_item_code").val("");
    $("#temp_item_name").val("");
    $("#temp_item_qty").val("");
    $("#temp_item_description").val("");
    $("#temp_item_unit").val("");
    $("#temp_sales_order_item_id").val("");

    $("#temp_item_qty").numberFormat();

    $("#fm_temp_item").find("input, textarea").focusin(function(event) {
      $(this).select();
    });

    // $("#fm_temp_item").on("keypress", function(e){
    //   if(e.keyCode==13){
    //     saveTmpItem();
    //   }
    // });
  }

  $(document).ready(function() {
    getTempTable();
    resetTemp();
  });

  function editTmpItem(id){
    $.ajax({
      url: '<?php echo set_url("procurement", "delivery_order", "temp_data"); ?>',
      type: 'GET',
      dataType: 'JSON',
      data: {id: id},
      beforeSend: function(){
        loadingSpinner.show();
      },
      success: function(response){
        if(response.status==true){
          var data = response.data;
          $("#temp_id").val(response.id);
          $("#temp_item_id").val(data.item_id);
          $("#temp_item_code").val(data.item_code);
          $("#temp_item_name").val(data.item_name);
          $("#temp_item_qty").numberFormat("setValue", data.qty);
          $("#temp_item_description").val(data.item_description);
          $("#temp_item_unit").val(data.unit);
          $("#temp_sales_order_item_id").val(data.sales_order_item_id);

          $("#modal_define_item").modal("show");

          $("#tmp_items_id").select2("val", 0);
        } else {
          resetTemp();
          alertify.error(response.message);
        }
        loadingSpinner.hide();
      }
    });
  }

  function deleteTmpItem(id){
    if(confirm('Delete selected item?')){
      $.ajax({
        url: '<?php echo set_url("procurement", "delivery_order", "temp_delete"); ?>',
        type: 'GET',
        dataType: 'JSON',
        data: {id: id},
        beforeSend: function(){
          loadingSpinner.show();
        },
        success: function(response){
          if(response.status==true){
            resetTemp();
            alertify.success(response.message);
          } else {
            resetTemp();
            alertify.error(response.message);
          }
          getTempTable();
          loadingSpinner.hide();
        }
      });
    }
  }

  function saveTmpItem(){
    var fmTemp = $("#fm_temp_item");
    $.ajax({
      url: '<?php echo set_url("procurement", "delivery_order", "temp_save"); ?>',
      type: 'POST',
      dataType: 'JSON',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: fmTemp.serializeArray(),
      success: function(response){
        if(response.status==true){
          resetTemp();
          $("#modal_define_item").modal("hide");
          alertify.success(response.message);
        } else {
          resetTemp();
          $("#modal_define_item").modal("hide");
          alertify.error(response.message);
        }
        getTempTable();
        loadingSpinner.hide();

        $("#tmp_items_id").select2("val", 0);
      }
    });
  }

  function calculateSubtotal(){
    var subtotal_all = 0;
    var grand_total = 0;
    var ppn = $("#ppn").numberFormat("getValue");

    subtotal_all = $("#subtotal_all").numberFormat("getValue");
    $("#ppn_total").numberFormat("setValue", (subtotal_all * (ppn/100)));
    grand_total = subtotal_all + (subtotal_all / (ppn/100));

    $("#total").numberFormat("setValue", grand_total);

    $("#ppn_total").on('focusin', function(event) { $(this).select(); });

    $("#ppn").numberFormat();
    $("#ppn_total").numberFormat();
    $("#subtotal_all").numberFormat();
    $("#total").numberFormat();
  }

  $("input").on('change', function(event) {
    calculateSubtotal();
  });

  $(document).ready(function() {
    calculateSubtotal();
  });
</script>

</body>
</html>
