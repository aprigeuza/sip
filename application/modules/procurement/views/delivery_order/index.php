<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Delivery Order - Procurement</title>
  <?php echo assets_top(); ?>
  <style media="screen">
    .iCheckedId{
      cursor: pointer;
    }
    tr:has(.iCheckedId:checked){
      background-color: #ddd;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Delivery Order
        <small>Procurement</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-building"></i> Procurement</a></li>
        <li class="active">Delivery Order</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php echo show_alert(); ?>
          <div class="row">
            <div class="col-md-12">
              <a href="javascript:;" data-toggle="modal" data-target="#modal_sales_order" class="btn btn-primary" title="add new"><i class="fa fa-file-o"></i> Add New</a>
              <a href="<?php echo set_url("procurement", "delivery_order"); ?>" class="btn btn-info" title="Reload"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
          </div>
          <hr class="line-separator">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-hover" id="table1">
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  var table1;
  $(function () {
    table1 = $('#table1').DataTable({
      "bDestroy": true,
      "processing": true,
      "serverSide": true,
      "saveState": true,
      "ajax": '<?php echo set_url("procurement", "delivery_order", "datatable"); ?>',
      "columns":[
        { "data": "do_number", "title": "Delivery Order#"},
        { "data": "cust_code", "title": "Customer#"},
        { "data": "cust_name", "title": "Customer"},
        { "data": "vehicle_driver", "title": "Driver"},
        { "data": "vehicle_number", "title": "Vehicle#"},
        { "data": "username", "title": "Created By"},
        { "data": "input_date", "title": "Created Date"},
        { "data": "posted", "title": "Posted", className: "text-center", "createdCell" : function (td, cellData, rowData, row, col) {
          var html = "";
          if(cellData == true){
            $(td).html("Yes");
          }else{
            $(td).html("No");
          }
        }},
        {
          data: null,
          title: "Options",
          className: "text-center col-md-2",
          "createdCell": function (td, cellData, rowData, row, col) {
              var html = "";
              if(rowData.posted == false){
              html += '<a href="<?php echo set_url("procurement", "delivery_order", "edit"); ?>?id='+rowData.id+'" class="btn btn-xs btn-primary">Edit</a> ';
                html += '<a href="javascript:;" onclick="setCancel(\''+rowData.id+'\');" class="btn btn-xs btn-danger">Cancel</a> ';
                html += '<a href="javascript:;" onclick="setPosted(\''+rowData.id+'\');" class="btn btn-xs btn-primary">Posted</a> ';
              }else{
                // html += '<a href="javascript:;" class="btn btn-xs btn-primary disabled">Edit</a> ';
                // html += '<a href="javascript:;" class="btn btn-xs btn-danger disabled">Cancel</a> ';
                html += '<a href="javascript:;" onclick="setUnPosted(\''+rowData.id+'\');" class="btn btn-xs btn-warning">Un-Posted</a> ';
              }
              html += '<a href="<?php echo set_url("procurement", "delivery_order", "print_preview"); ?>?id='+rowData.id+'" class="btn btn-xs btn-info" target="_blank">Print</a> ';
              $(td).html(html);
          },
        }
      ],
    });
  })
</script>
<script>
  function setCancel(id){
    if(confirm('Cancel Document?')){
      $.ajax({
        url: '<?php echo set_url("procurement", "delivery_order", "cancel"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data: {id: id},
        success: function(response){
          if(response.status == true){
            alertify.success(response.message);
          }else{
            alertify.error(response.message);
          }
          table1.ajax.reload();
        }
      });
    }
  }
  function setPosted(id){
    if(confirm('Posted Document?')){
      $.ajax({
        url: '<?php echo set_url("procurement", "delivery_order", "posted"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data: {id: id},
        success: function(response){
          if(response.status == true){
            alertify.success(response.message);
          }else{
            alertify.error(response.message);
          }
          table1.ajax.reload();
        }
      });
    }
  }
  function setUnPosted(id){
    if(confirm('Un-Posted Document?')){
      $.ajax({
        url: '<?php echo set_url("procurement", "delivery_order", "unposted"); ?>',
        type: 'POST',
        dataType: 'JSON',
        data: {id: id},
        success: function(response){
          if(response.status == true){
            alertify.success(response.message);
          }else{
            alertify.error(response.message);
          }
          table1.ajax.reload();
        }
      });
    }
  }
</script>


<div class="modal" id="modal_sales_order" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-lg" style="margin-left:30px;margin-right:30px;width:auto">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="">Select Order Item</h4>
      </div>
      <div class="modal-body" style="overflow:hidden;">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label>Select Sales Order</label>
              <select class="form-control" name="sales_order_id" id="sales_order_id" style="width: 100%"></select>
            </div>
          </div>
        </div>
        <br>
        <table class="table table-bordered table-hover" id="table2" style="width:100%;">
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnCreateDONext">Next</button>
      </div>
    </div>
  </div>
</div>

<script>
var select2_sales_order_id;
var table2;
var checked_id;
function reloadTable2(sales_order_id){
  loadingSpinner.show();
  clearCheckedId();

  table2 = $('#table2').DataTable({
    "bDestroy": true,
    "processing": true,
    "serverSide": true,
    "saveState": true,
    "ajax": {
      "url": '<?php echo set_url("procurement", "delivery_order", "datatable_sales_order"); ?>',
      "data": function ( d ) {
        d.sales_order_id = sales_order_id;
      }
    },
    "columns":[
      {
        data: "sales_order_item_id",
        title: "Options",
        className: "text-center",
        "createdCell": function (td, cellData, rowData, row, col) {
          var html = '';
          var c = Array.isArray(checked_id);
          if(c){
            if(checked_id.includes(cellData)){
              html += '<input class="iCheckedId" checked type="checkbox" name="checked_id[]" value="'+cellData+'">';
            }else{
              html += '<input class="iCheckedId" type="checkbox" name="checked_id[]" value="'+cellData+'">';
            }
          }else{
            html += '<input class="iCheckedId" type="checkbox" name="checked_id[]" value="'+cellData+'">';
          }
          $(td).html(html);
        },
      },
      { "data": "so_number", "title": "SO#"},
      { "data": "so_date", "title": "SO Date"},
      { "data": "po_number", "title": "PO#"},
      { "data": "item_code", "title": "Item#"},
      { "data": "item_name", "title": "Item Name"},
      { "data": "unit", "title": "Unit"},
      { "data": "total_delivery", "title": "Total", className: "text-right",},
      { "data": "total_delivered", "title": "Delivered", className: "text-right",},
      { "data": "total_outstanding", "title": "Outstanding", className: "text-right",},
    ],
    "drawCallback": function( settings ) {
      iCheckedIdTrigger();
      loadingSpinner.hide();
    }
  });
}

$('#modal_sales_order').on('shown.bs.modal', function (e) {
  reloadTable2(0);
  select2_sales_order_id = $('#sales_order_id').select2({
    dropdownParent: $('#modal_sales_order'),
    ajax: {
      url: '<?php echo set_url("api", "select2_sales_order"); ?>',
      dataType: 'json',
      data: function (params) {
        var query = {
          keywords: params.term
        }
        return query;
      },
      processResults: function (data) {
        return {
          results: data.items
        };
      }
    }
  });
  select2_sales_order_id.on("select2:select", function (e) {
    reloadTable2($(this).val());
  });
});

</script>
<script>
function iCheckedIdTrigger(){
  $(".iCheckedId").on("click", function(){
    if(this.checked){
      saveCheckedId($(this).val());
    }else{
      deleteCheckedId($(this).val());
    }
  });
}
function saveCheckedId(id){
  var c = Array.isArray(checked_id);
  if(c){
    if(!checked_id.includes(id)){
      checked_id.push(id);
      localStorage.setItem("delivery_order_checked_id", JSON.stringify(checked_id));
    }
  }else{
    checked_id = new Array();
    checked_id.push(id);
    localStorage.setItem("delivery_order_checked_id", JSON.stringify(checked_id));
  }
  console.log(checked_id);
}
function deleteCheckedId(id){
  var index = checked_id.indexOf(id);
  if (index !== -1) checked_id.splice(index, 1);
  console.log(checked_id);
}
function clearCheckedId(){
  checked_id = [];
  localStorage.setItem("delivery_order_checked_id", JSON.stringify(checked_id));
}

$("#btnCreateDONext").on('click', function(event) {
  var iCheckedId = checked_id;
  var sales_order_id = $("#sales_order_id").val();
  $.ajax({
    url: '<?php echo set_url("procurement", "delivery_order", "prepare_add"); ?>',
    type: 'POST',
    dataType: 'JSON',
    data: {"checked_id": iCheckedId, "sales_order_id": sales_order_id},
    beforeSend: function(){
      loadingSpinner.show();
    },
    success: function(response){
      loadingSpinner.hide();
      if(response.status){
        window.location = response.url;
      }else{
        alertify.error(response.message);
      }
    }
  });
});
</script>

</body>
</html>
