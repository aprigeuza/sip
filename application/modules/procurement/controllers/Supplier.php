<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("supplier_model");
  }

  public function index()
  {
    $data["table_data"] = $this->supplier_model->table_data();
    $this->load->view("supplier/index", $data);
  }

  public function add()
  {
    $this->load->view("supplier/add");
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->supplier_model->get_data($id);
    $this->load->view("supplier/edit", $data);
  }

  // Action
  public function save()
  {
    $supp_code = $this->input->post("supp_code");
    $supp_name = $this->input->post("supp_name");
    $npwp = $this->input->post("npwp");
    $telp = $this->input->post("telp");
    $address = $this->input->post("address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = $this->session->userdata("uid");

    $data["supp_code"] = $supp_code;
    $data["supp_name"] = $supp_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["address"] = $address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->supplier_model->insert($data);
    redirect("procurement/supplier");
  }

  public function update()
  {
    $supp_name = $this->input->post("supp_name");
    $npwp = $this->input->post("npwp");
    $telp = $this->input->post("telp");
    $address = $this->input->post("address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = $this->session->userdata("uid");

    $id = $this->input->post("id");

    $data["supp_name"] = $supp_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["address"] = $address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->supplier_model->update($data, $id);
    redirect("procurement/supplier");
  }

  public function obsolete()
  {
    $id = $this->input->get("id");
    $this->supplier_model->obsolete($id);
    redirect("procurement/supplier");
  }

}
