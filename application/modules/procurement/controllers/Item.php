<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
        $this->load->model("item_model");
  }


    public function index()
    {
      $data["table_data"] = $this->item_model->table_data();
      $this->load->view("item/index", $data);
    }

    public function add()
    {
      $this->load->view("item/add");
    }

    public function edit()
    {
      $id = $this->input->get("id");
      $data["edit"] = $this->item_model->get_data($id);
      $this->load->view("item/edit", $data);
    }

    // Action
    public function save()
    {
      $item_code = $this->input->post("item_code");
      $item_name = $this->input->post("item_name");
      $item_description = $this->input->post("item_description");
      $input_date = date("Y-m-d H:i:s");
      $input_by = $this->session->userdata("uid");

      $data["item_code"] = $item_code;
      $data["item_name"] = $item_name;
      $data["item_description"] = $item_description;
      $data["input_date"] = $input_date;
      $data["input_by"] = $input_by;

      $this->item_model->insert($data);
      redirect("procurement/item");
    }

    public function update()
    {
      $item_name = $this->input->post("item_name");
      $item_description = $this->input->post("item_description");
      $input_date = date("Y-m-d H:i:s");
      $input_by = $this->session->userdata("uid");

      $id = $this->input->post("id");

      $data["item_name"] = $item_name;
      $data["item_description"] = $item_description;
      $data["input_date"] = $input_date;
      $data["input_by"] = $input_by;

      $this->item_model->update($data, $id);
      redirect("procurement/item");
    }

    public function obsolete()
    {
      $id = $this->input->get("id");
      $this->item_model->obsolete($id);
      redirect("procurement/item");
    }

}
