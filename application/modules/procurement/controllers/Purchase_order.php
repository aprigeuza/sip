<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    validate_access();

    $this->load->model("purchase_order_model");
    $this->load->model("supplier_model");
    $this->load->model("item_model");
    $this->load->model("temp_data_model");
  }

  public function index()
  {
    $data = array();
    $this->load->view("purchase_order/index", $data);
  }

  public function add()
  {
    $id = $this->purchase_order_model->insert_draft();

    redirect("procurement/purchase_order/edit?id=" . $id);
  }

  public function edit()
  {
    // Clear Temp
    $this->temp_data_model->delete_by_key("procurement_purchase_order_edit");

    $id = $this->input->get("id");

    $data["new_number"] = $this->purchase_order_model->new_number();
    $data["list_supplier"] = $this->supplier_model->get_list_active();
    $data["list_item"] = $this->item_model->get_list_active();

    $edit = $this->purchase_order_model->get_data($id);
    if($edit->posted == false)
    {
      $data["edit"] = $edit;
      // Insert Temp
      $this->temp_data_model->delete_by_key("procurement_purchase_order_edit");
      foreach($edit->items as $item)
      {
        $_temp_data["data"] = json_encode($item);
        $_temp_data["key"] = "procurement_purchase_order_edit";
        $this->temp_data_model->insert($_temp_data);
      }

      $this->load->view("purchase_order/edit", $data);
    }
    else
    {
      set_alert("danger", "The document number \"".$edit->po_number."\" can't be edit, because the document has been Posted!");
      redirect("procurement/purchase_order");
    }

  }

  public function print_preview()
  {
    $this->load->library("dompdf_lib");
    $id = $this->input->get("id");

    $data = array();
    $res = $this->purchase_order_model->get_data($id);
    $data["data"] = $res;
    $this->dompdf_lib->setPaper('A4', 'potrait');
    $this->dompdf_lib->generate('purchase_order/print_preview', $data, "SO" . $res->po_number . "-".$res->supp_name.".pdf");
  }

  public function datatable()
  {
    $config["table"] = "v_purchase_order";
    $config["where_all"] = [
      "cancel = 0"
    ];
    $this->datatable->generate($config);
  }

  // Action
  public function update()
  {
    $id = $this->input->post("id");
    $supplier_id = $this->input->post("supplier_id");

    $supplier = $this->supplier_model->get_data($supplier_id);

    $supp_code  = $supplier->supp_code;
    $supp_name  = $supplier->supp_name;
    $npwp       = $supplier->npwp;
    $telp       = $supplier->telp;
    $address = $this->input->post("address");
    $input_date = date("Y-m-d H:i:s");
    $input_by = get_session("uid");
    $ppn = $this->input->post("ppn");
    $total_dp = $this->input->post("total_dp");
    $po_date = $this->input->post("po_date");
    $delivery_date = $this->input->post("delivery_date");
    $sales_order_id = $this->input->post("sales_order_id");
    $shipping_cost = $this->input->post("shipping_cost");
    $other_cost = $this->input->post("other_cost");
    $po_note = $this->input->post("po_note");

    $data["supplier_id"] = $supplier_id;
    $data["po_date"] = $po_date;
    $data["sales_order_id"] = $sales_order_id;
    $data["delivery_date"] = $delivery_date;
    $data["ppn"] = unmask_money($ppn);
    $data["total_dp"] = unmask_money($total_dp);
    $data["shipping_cost"] = unmask_money($shipping_cost);
    $data["other_cost"] = unmask_money($other_cost);
    $data["supp_code"] = $supp_code;
    $data["supp_name"] = $supp_name;
    $data["npwp"] = $npwp;
    $data["telp"] = $telp;
    $data["address"] = $address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;
    $data["po_note"] = $po_note;

    $this->purchase_order_model->update($data, $id);

    $this->purchase_order_model->delete_item_by_header($id);

    $detail = $this->temp_data_model->get_by_key("procurement_purchase_order_edit");

    if($detail !== FALSE)
    {
      foreach($detail as $_row)
      {
        $row = json_decode($_row->data);

        $data_item["purchase_order_id"] = $id;
        $data_item["item_id"] = $row->item_id;
        $data_item["item_code"] = $row->item_code;
        $data_item["item_name"] = $row->item_name;
        $data_item["item_description"] = $row->item_description;
        $data_item["qty"] = $row->qty;
        $data_item["unit"] = $row->unit;
        $data_item["unit_price"] = $row->unit_price;
        $data_item["subtotal"] = ($row->qty * $row->unit_price);

        $this->purchase_order_model->insert_item($data_item);
      }
    }
    redirect("procurement/purchase_order");
  }

  public function cancel()
  {
    $id = $this->input->post_get("id");
    $this->purchase_order_model->cancel($id);
    $response["status"] = true;
    $response["message"] = "Document Canceled!";
    json_file($response);
  }

  public function posted()
  {
    $id = $this->input->post_get("id");
    $act = $this->purchase_order_model->posted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because document is canceled!";
    }
    json_file($response);
  }

  public function unposted()
  {
    $id = $this->input->post_get("id");
    $act = $this->purchase_order_model->unposted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Un-posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because there is a related delivery order!";
    }
    json_file($response);
  }

  public function temp_save()
  {
    $temp_id = $this->input->post("temp_id");

    $temp_item_id = $this->input->post("temp_item_id");
    $temp_item_code = $this->input->post("temp_item_code");
    $temp_item_name = $this->input->post("temp_item_name");
    $temp_item_qty = $this->input->post("temp_item_qty");
    $temp_item_description = $this->input->post("temp_item_description");
    $temp_item_unit = $this->input->post("temp_item_unit");
    $temp_item_unit_price = $this->input->post("temp_item_unit_price");

    $temp_data["item_id"] = $temp_item_id;
    $temp_data["item_code"] = $temp_item_code;
    $temp_data["item_name"] = $temp_item_name;
    $temp_data["qty"] = unmask_money($temp_item_qty);
    $temp_data["item_description"] = $temp_item_description;
    $temp_data["unit"] = $temp_item_unit;
    $temp_data["unit_price"] = unmask_money($temp_item_unit_price);

    $data["data"] = json_encode($temp_data);
    $data["key"] = "procurement_purchase_order_edit";

    if($temp_id)
    {
      $res = $this->temp_data_model->update($data, $temp_id);
    }
    else
    {
      $res = $this->temp_data_model->insert($data);
    }


    if($res)
    {
      $response["status"] = TRUE;
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Failed!";
    }
    json_file($response);
  }

  public function temp_data()
  {
    $id = $this->input->post_get("id");

    $data = $this->temp_data_model->get_by_id($id);

    $response["status"] = TRUE;
    $response["id"] = $id;
    $response["data"] = json_decode($data);
    $response["message"] = "Success!";
    json_file($response);
  }

  public function temp_table()
  {
    $res = $this->temp_data_model->get_by_key("procurement_purchase_order_edit");
    if($res !== FALSE)
    {
      $id = $this->input->get("id");
      $edit = $this->purchase_order_model->get_data($id);
      $data["edit"] = $edit;
      $data["temp_data"] = $res;
      $response["status"] = TRUE;
      $response["html"] = $this->load->view("procurement/purchase_order/temp_table", $data, TRUE);
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["html"] = "";
      $response["message"] = "Not Found!";
    }
    json_file($response);
  }

  public function temp_delete()
  {
    $id = $this->input->post_get("id");

    $this->temp_data_model->delete_by_id($id);

    $response["status"] = TRUE;
    $response["message"] = "Success!";
    json_file($response);
  }

  public function select2($opt)
  {
    $response = array();

    switch($opt)
    {
      case "sales_order":
      $this->load->model("sales_order_model");

      $search = $this->input->post_get("search");

      $res = $this->sales_order_model->search($search);

      $items = array();

      $item["id"] = 0;
      $item["text"] = "Select SO";
      $items[] = (object)$item;

      foreach($res->result() as $row)
      {
        $item["id"] = $row->id;
        $item["text"] = "SO#" . $row->po_number . " - " . $row->cust_name;
        $items[] = (object)$item;
      }

      $response["status"] = true;
      $response["items"] = $items;

      break;
    }

    json_file($response);
  }

}
