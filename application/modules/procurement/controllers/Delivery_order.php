<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_order extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    validate_access();

    $this->load->model("delivery_order_model");
    $this->load->model("customer_model");
    $this->load->model("item_model");
    $this->load->model("temp_data_model");

  }

  public function index()
  {
    $data["list_customer"] = $this->customer_model->get_list_active();
    $this->load->view("delivery_order/index", $data);
  }

  public function add()
  {
    $id = $this->delivery_order_model->insert_draft();

    redirect("procurement/delivery_order/edit?id=" . $id);
  }

  public function prepare_add()
  {
    $checked_id = $this->input->post("checked_id");
    $sales_order_id = $this->input->post("sales_order_id");
    if(is_array($checked_id) && count($checked_id) >= 1)
    {
      set_session("procurement_delivery_order_checked_id", $checked_id);
      set_session("procurement_delivery_order_sales_order_id", $sales_order_id);

      $response["status"] = true;
      $response["message"] = "Success!";
      $response["url"] = set_url("procurement", "delivery_order", "add");
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Empty Checked!";
      $response["url"] = null;
    }

    json_file($response);
  }

  public function edit()
  {
    // Clear Temp
    $this->temp_data_model->delete_by_key("procurement_delivery_order_edit");

    $id = $this->input->get("id");

    $data["new_number"] = $this->delivery_order_model->new_number();
    $data["list_customer"] = $this->customer_model->get_list_active();
    $data["list_item"] = $this->item_model->get_list_active();

    $edit = $this->delivery_order_model->get_data($id);
    $data["edit"] = $edit;
    // Insert Temp
    $this->temp_data_model->delete_by_key("procurement_delivery_order_edit");
    foreach($edit->items as $item)
    {
      $_temp_data["data"] = json_encode($item);
      $_temp_data["key"] = "procurement_delivery_order_edit";
      $this->temp_data_model->insert($_temp_data);
    }

    $this->load->view("delivery_order/edit", $data);
  }

  public function print_preview()
  {
    $this->load->library("dompdf_lib");
    $id = $this->input->get("id");

    $data = array();
    $res = $this->delivery_order_model->get_data($id);
    $data["data"] = $res;
    $this->dompdf_lib->setPaper('A4', 'potrait');
    $this->dompdf_lib->generate('delivery_order/print_preview', $data, "DO" . $res->do_number . "-".$res->cust_name.".pdf");
  }

  public function datatable()
  {
    $config["table"] = "v_delivery_order";
    $where_all[] = "cancel = 0";
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  public function datatable_sales_order()
  {
    $config["table"] = "v_sales_order_for_delivery_order";
    $where_all[] = "sales_order_id = " . $this->input->post_get("sales_order_id");
    $where_all[] = "total_outstanding > 0 ";
    $config["where_all"] = $where_all;
    $this->datatable->generate($config);
  }

  // Action
  public function update()
  {
    $id = $this->input->post("id");
    $shipping_address = $this->input->post("shipping_address");
    $vehicle_number = $this->input->post("vehicle_number");
    $vehicle_driver = $this->input->post("vehicle_driver");
    $input_date = date("Y-m-d H:i:s");
    $input_by = get_session("uid");
    $do_date = $this->input->post("do_date");

    $data["do_date"] = $do_date;
    $data["vehicle_number"] = $vehicle_number;
    $data["vehicle_driver"] = $vehicle_driver;
    $data["shipping_address"] = $shipping_address;
    $data["input_date"] = $input_date;
    $data["input_by"] = $input_by;

    $this->delivery_order_model->update($data, $id);

    $this->delivery_order_model->delete_item_by_header($id);

    $detail = $this->temp_data_model->get_by_key("procurement_delivery_order_edit");

    if($detail !== FALSE)
    {
      foreach($detail as $_row)
      {
        $row = json_decode($_row->data);

        $data_item["delivery_order_id"] = $id;
        $data_item["item_id"] = $row->item_id;
        $data_item["item_code"] = $row->item_code;
        $data_item["item_name"] = $row->item_name;
        $data_item["item_description"] = $row->item_description;
        $data_item["qty"] = $row->qty;
        $data_item["unit"] = $row->unit;
        $data_item["sales_order_item_id"] = $row->sales_order_item_id;

        $this->delivery_order_model->insert_item($data_item);
      }
    }
    redirect("procurement/delivery_order");
  }

  public function cancel()
  {
    $id = $this->input->post_get("id");
    $this->delivery_order_model->cancel($id);
    $response["status"] = true;
    $response["message"] = "Document Canceled!";
    json_file($response);
  }

  public function posted()
  {
    $id = $this->input->post_get("id");
    $act = $this->delivery_order_model->posted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be posted because document is canceled!";
    }
    json_file($response);
  }

  public function unposted()
  {
    $id = $this->input->post_get("id");
    $act = $this->delivery_order_model->unposted($id);
    if($act==true)
    {
      $response["status"] = true;
      $response["message"] = "Document Un-posted!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "The document cannot be un-posted because there is a related Invoices!";
    }
    json_file($response);
  }

  public function temp_save()
  {
    $temp_id = $this->input->post("temp_id");

    $temp_item_id = $this->input->post("temp_item_id");
    $temp_item_code = $this->input->post("temp_item_code");
    $temp_item_name = $this->input->post("temp_item_name");
    $temp_item_qty = $this->input->post("temp_item_qty");
    $temp_item_description = $this->input->post("temp_item_description");
    $temp_item_unit = $this->input->post("temp_item_unit");
    $temp_sales_order_item_id = $this->input->post("temp_sales_order_item_id");

    $temp_data["item_id"] = $temp_item_id;
    $temp_data["item_code"] = $temp_item_code;
    $temp_data["item_name"] = $temp_item_name;
    $temp_data["qty"] = unmask_money($temp_item_qty);
    $temp_data["item_description"] = $temp_item_description;
    $temp_data["unit"] = $temp_item_unit;
    $temp_data["sales_order_item_id"] = $temp_sales_order_item_id;

    $data["data"] = json_encode($temp_data);
    $data["key"] = "procurement_delivery_order_edit";

    if($temp_id)
    {
      $res = $this->temp_data_model->update($data, $temp_id);
    }
    else
    {
      $res = $this->temp_data_model->insert($data);
    }


    if($res)
    {
      $response["status"] = TRUE;
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["message"] = "Failed!";
    }
    json_file($response);
  }

  public function temp_data()
  {
    $id = $this->input->post_get("id");

    $data = $this->temp_data_model->get_by_id($id);

    $response["status"] = TRUE;
    $response["id"] = $id;
    $response["data"] = json_decode($data);
    $response["message"] = "Success!";
    json_file($response);
  }

  public function temp_table()
  {
    $res = $this->temp_data_model->get_by_key("procurement_delivery_order_edit");
    if($res !== FALSE)
    {
      $data["temp_data"] = $res;
      $response["status"] = TRUE;
      $response["html"] = $this->load->view("procurement/delivery_order/temp_table", $data, TRUE);
      $response["message"] = "Success!";
    }
    else
    {
      $response["status"] = FALSE;
      $response["html"] = "";
      $response["message"] = "Not Found!";
    }
    json_file($response);
  }

  public function temp_delete()
  {
    $id = $this->input->post_get("id");

    $this->temp_data_model->delete_by_id($id);

    $response["status"] = TRUE;
    $response["message"] = "Success!";
    json_file($response);
  }

}
