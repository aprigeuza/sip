<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MX_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    $this->load->model("temp_data_model");
    $this->load->model("item_model");
    $this->load->model("sales_order_model");
    $this->load->model("invoice_model");
    $this->load->model("customer_model");
  }

  public function index()
  {
    $response["name"] = "API";
    $response["version"] = "1.0.0";
    json_file($response);
  }

  public function get_item()
  {
    $id = $this->input->post_get("id");

    $item = $this->item_model->get_data($id);

    $response["status"] = TRUE;
    $response["data"] = $item;
    $response["message"] = "Success!";
    json_file($response);
  }

  public function get_items()
  {
    $items = $this->item_model->get_list_all();

    $response["status"] = TRUE;
    $response["data"] = $items->result();
    $response["message"] = "Success!";
    json_file($response);
  }

  public function select2_sales_order()
  {
    $keywords = $this->input->post_get("keywords");

    $res = $this->sales_order_model->search($keywords);

    $items = array();

    foreach($res->result() as $row)
    {
      if($row->cancel == false)
      {
        $item["id"] = $row->id;
        $item["text"] = implode(" - ", array($row->so_number, $row->cust_name));
        $items[] = $item;
      }
    }

    $response["status"] = true;
    $response["items"] = $items;

    json_file($response);
  }

  public function dashboard_data()
  {
    $data = array();

    $customers = $this->customer_model->get_list_active();
    $data["total_customer"] = $customers->num_rows();

    $sales = $this->sales_order_model->get_posted_sales();
    $data["total_sales"] = $sales->num_rows();

    $invoice = $this->invoice_model->get_posted_invoice();
    $data["total_invoice"] = $invoice->num_rows();

    $response["status"] = true;
    $response["data"] = $data;

    json_file($response);
  }
}
