<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config["app_version"] = "2.0";
$config["app_pondasiweb_url"] = "http://www.pondasiweb.com";
$config["app_pondasiweb_txt"] = "www.pondasiweb.com";
$config["app_copyright"] = "Copyright &copy; 2019";
$config["user_level"] = array("sa", "de");
$config["user_level_list"] = array(
  "sa" => "System Administrator",
  "de" => "Data Entry"
);
$config["gender_list"] = array( "L" => "Laki-Laki",
                                "P" => "Perempuan");
$config["marital_status_list"] = array( "BM" => "Belum Menikah", "M" => "Menikah");
$config["religion_list"] = array( "Islam", "Kristen", "Kristen Protestan", "Kristen Katolik", "Protestan", "Katolik", "Hindu", "Budha", "Konghucu", "Yahudi", "Baha'i", "Kristen Ortodoks");

$config["quot_number_prefix"] = "QTPGI";
$config["so_number_prefix"] = "SOPGI";
$config["do_number_prefix"] = "DOPGI";
$config["po_number_prefix"] = "POPGI";
$config["iv_number_prefix"] = "IVPGI";
