<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rep_datamining_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  public function get_customers()
  {
    $this->db->select();
    $this->db->from("customer");
    return $this->db->get();
  }

  public function export_data($filter)
  {
    $this->db->select();
    $this->db->from("v_datamining");
    if(is_array($filter))
    {
      foreach($filter as $name => $value)
      {
        if(!empty($name) && strlen($value) > 0)
        {
          $this->db->where($name, $value);
        }
      }
    }
    $res = $this->db->get();

    return $res;
  }

}
