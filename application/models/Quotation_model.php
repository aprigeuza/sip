<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("quotation", $data);

    return $this->db->affected_rows();
  }

  public function update($data, $id)
  {
    $this->db->update("quotation", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function cancel($id)
  {
    $data["cancel"] = true;
    $this->db->update("quotation", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }


  public function table_data()
  {
    $this->db->from("v_quotation");
    $this->db->where("cancel", false);
    return $this->db->get();
  }

  public function get_data($id)
  {
    $this->db->select("*");
    $this->db->from("v_quotation");
    $this->db->where("id", $id);
    $res = $this->db->get();
    $data = $res->row();

    $items = $this->db->get_where("quotation_item", array("quotation_id" => $data->id));
    $data->items = $items->result();

    return $data;
  }

  public function number_exists($code)
  {
    $res = $this->db->get_where("quotation", ["quotation_number" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }

  public function new_number()
  {
    $prefix = $this->config->item("quot_number_prefix");
    $last_num = 1;

    $this->db->select("quot_number");
    $this->db->from("quotation");
    $this->db->like("quot_number", $prefix . date("y"), "LEFT");
    $this->db->order_by("quot_number", "ASC");
    $res = $this->db->get();

    if($res->num_rows())
    {
      $row = $res->last_row();

      $n = substr($row->quot_number, -4);

      $_n = (int)$n + 1;

      $new_number = $prefix . date("ym") . sprintf ("%'.04d\n", (int)$_n);;

      return $new_number;
    }
    else
    {
      $last_num = $prefix . date("ym") . "0001";

      return $last_num;
    }
    return 0;
  }

  public function insert_draft()
  {
    $data["quot_number"] = $this->new_number();
    $data["quot_date"] = date("Y-m-d");
    $data["input_date"] = date("Y-m-d H:i:s");
    $data["input_by"] = get_session("uid");
    $data["quot_note"] = get_setting("quotation_note");
    $this->db->insert("quotation", $data);

    return $this->db->insert_id();
  }

  // Quotation Item`

  public function insert_item($data)
  {
    $this->db->insert("quotation_item", $data);
    return $this->db->insert_id();
  }

  public function update_item($data, $id)
  {
    $this->db->update("quotation_item", $data, ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item($data, $id)
  {
    $this->db->delete("quotation_item", ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item_by_header($id)
  {
    $this->db->delete("quotation_item", ["quotation_id" => $id]);
    return $this->db->affected_rows();
  }

}
