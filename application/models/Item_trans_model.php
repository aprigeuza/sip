<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_trans_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("item_trans", $data);

    return $this->db->insert_id();
  }

  public function update($data, $id)
  {
    $this->db->update("item_trans", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function obsolete($id)
  {
    $this->db->where("item_trans.id", $id);
    $res = $this->db->get("item_trans");
    $row = $res->row();
    $data["obsolete"] = !$row->obsolete;
    $this->db->update("item_trans", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function table_data()
  {
    $this->db->select("item_trans.*, user.username");
    $this->db->from("item_trans");
    $this->db->join("user", "user.id=item_trans.input_by");
    return $this->db->get();
  }

  public function get_list_active()
  {
    $this->db->select("item_trans.*, user.username");
    $this->db->from("item_trans");
    $this->db->join("user", "user.id=item_trans.input_by");
    $this->db->where("item_trans.obsolete", false);
    $res = $this->db->get();
    return $res;
  }

  public function get_data($id)
  {
    $this->db->select("item_trans.*, user.username");
    $this->db->from("item_trans");
    $this->db->join("user", "user.id=item_trans.input_by");
    $this->db->where("item_trans.id", $id);
    $res = $this->db->get();
    return $res->row();
  }

  public function code_exists($code)
  {
    $res = $this->db->get_where("item_trans", ["cust_code" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }
}
