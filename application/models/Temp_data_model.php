<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temp_data_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function get_by_id($id)
  {
    $where["id"] = $id;
    $where["username"] = get_session("un");
    $res = $this->db->get_where("temp_data", $where);

    if($res->num_rows())
    {
      $row = $res->row();

      return $row->data;
    }

    return NULL;
  }

  public function get_by_key($key)
  {
    $where["key"] = $key;
    $where["username"] = get_session("un");
    $this->db->select("id, data");
    $this->db->order_by("id", "ASC");
    $res = $this->db->get_where("temp_data", $where);

    if($res->num_rows())
    {
      return $res->result();
    }

    return NULL;
  }

  public function delete_by_id($id)
  {
    $where["id"] = $id;
    $where["username"] = get_session("un");
    $this->db->delete("temp_data", $where);
    if($this->db->affected_rows())
    {
      return true;
    }
    return false;
  }

  public function delete_by_key($key)
  {
    $where["key"] = $key;
    $where["username"] = get_session("un");
    $this->db->delete("temp_data", $where);
    if($this->db->affected_rows())
    {
      return true;
    }
    return false;
  }

  public function insert($data)
  {
    // Insert
    $data["username"] = get_session("un");
    $this->db->insert("temp_data", $data);
    if($this->db->affected_rows())
    {
      return true;
    }
    return false;
  }

  public function update($data, $id)
  {
    // Update
    $tmp_where["id"] = $id;
    $this->db->update("temp_data", $data, $tmp_where);
    if($this->db->affected_rows())
    {
      return true;
    }
  }

}
