<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rep_item_model extends CI_Model{

  var $tmp_table_name = "tmp_rep_item_support";

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->dbforge();
  }

  public function get_filter_years($search)
  {
    $this->db->select("YEAR(so_date) AS so_year, COUNT(id) AS total_sales");
    $this->db->from("sales_order");
    $this->db->like("YEAR(so_date)", $search);
    $this->db->where("cancel", 0);
    $this->db->where("posted", 1);
    $this->db->group_by("YEAR(so_date)");
    $res = $this->db->get();

    return $res;
  }

  public function get_filter_months($search, $filter_year)
  {
    $this->db->select("MONTH(so_date) AS so_month, MONTHNAME(so_date) AS so_month_name, COUNT(id) AS total_sales");
    $this->db->from("sales_order");
    $this->db->like("MONTHNAME(so_date)", $search);
    $this->db->where("YEAR(so_date)", $filter_year);
    $this->db->where("cancel", 0);
    $this->db->where("posted", 1);
    $this->db->group_by("MONTH(so_date)");
    $res = $this->db->get();

    return $res;
  }

  public function get_filter_items($search, $filter_year, $filter_month, $filter_minimal_frequent)
  {
    $this->db->select("*");
    $this->db->from("v_item_sales");
    $this->db->like("item_code", $search, "BOTH");
    $this->db->or_like("item_name", $search, "BOTH");
    $this->db->where("so_year", $filter_year);
    if($filter_month) $this->db->where("so_month", $filter_month);
    $this->db->where("frequent >=", $filter_minimal_frequent);
    $this->db->group_by("id");
    $res = $this->db->get();

    return $res;
  }

  public function get_support($item_id, $filter_year, $filter_month, $filter_minimal_frequent)
  {
    $this->db->from("sales_order");
    $this->db->where("posted", 1);
    $this->db->where("cancel", 0);
    $this->db->where("YEAR(so_date)", $filter_year);
    if($filter_month) $this->db->where("MONTH(so_date)", $filter_month);
    $so = $this->db->get();

    $this->db->select("(COUNT(DISTINCT sales_order_id) / ".$so->num_rows().") AS support");
    $this->db->from("v_item_sales2");
    $this->db->like("item_id", $item_id);
    $this->db->where("YEAR(so_date)", $filter_year);
    if($filter_month) $this->db->where("MONTH(so_date)", $filter_month);
    $this->db->having("COUNT(DISTINCT sales_order_id) >=", $filter_minimal_frequent);
    $item_res = $this->db->get();

    if($item_res->num_rows())
    {
      $r = $item_res->row();
      return $r->support;
    }
    return 0;
  }

  public function get_items()
  {
    $this->db->order_by("frequent", "DESC");
    $res = $this->db->get("v_item_sales");

    return $res;
  }

  public $tmp = array();

  public function process_analisists($items=array(), $filter_year, $filter_month, $filter_minimal_frequent)
  {
    $this->db->from("sales_order");
    $this->db->where("posted", 1);
    $this->db->where("cancel", 0);
    $this->db->where("YEAR(so_date)", $filter_year);
    if($filter_month) $this->db->where("MONTH(so_date)", $filter_month);
    $so = $this->db->get();

    $res = array();
    $res_items = array();
    if(is_array($items) && count($items) >= 2)
    {
      $this->do_analisists($items);

      // Get Total Sales
      // $total_sales = $this->db->get_where("sales_order", ["posted" => 0])->num_rows();


      $tmp = $this->tmp;
      foreach($tmp as $t)
      {
        $so2 = $this->get_support($t["item_1"], $filter_year, $filter_month, $filter_minimal_frequent);

        $this->db->select("
          (SELECT GROUP_CONCAT(id) FROM item WHERE id IN (".$t["item_1"].",".$t["item_2"].")) AS item_id,
          (SELECT GROUP_CONCAT(item_code) FROM item WHERE id IN (".$t["item_1"].",".$t["item_2"].")) AS item_code,
          (SELECT GROUP_CONCAT(item_name) FROM item WHERE id IN (".$t["item_1"].",".$t["item_2"].")) AS item_name,
          COUNT(DISTINCT sales_order_id) AS frequent,
          (".$so->num_rows().") AS total_sales,
          (COUNT(DISTINCT sales_order_id) / ".$so->num_rows().") AS support
        ");
        $this->db->from("v_item_sales2");
        $this->db->like("item_id", $t["item_1"], "BOTH");
        $this->db->like("item_id", $t["item_2"], "BOTH");
        $this->db->where("YEAR(so_date)", $filter_year);
        if($filter_month) $this->db->where("MONTH(so_date)", $filter_month);
        $this->db->having("COUNT(DISTINCT sales_order_id) >=", $filter_minimal_frequent);
        $item_res = $this->db->get();

        if($item_res->num_rows())
        {
          $item_row = $item_res->row();
          if($item_row->item_code != "")
          {
            $itt["item_id"] = $item_row->item_id;
            $itt["item_code"] = $item_row->item_code;
            $itt["size"] = 2;
            $itt["frequent"] = $item_row->frequent;
            $itt["total_sales"] = $item_row->total_sales;
            $itt["support"] = $item_row->support;
            $itt["confidence"] = ($so2 > 0) ? $item_row->support / $so2 : 0;
            $res_items[] = (object)$itt;
          }
        }
      }
    }
    $res["tmp"] = $this->tmp;
    $res["items"] = $res_items;

    return (object)$res;
  }

  function do_analisists($items)
  {
    $t1 = array();
    for($i=0; $i<count($items); $i++)
    {
      $t1[] = $items[$i];
      for($ii=0; $ii<count($items); $ii++)
      {
        if(!in_array($items[$ii], $t1))
        {
          $this->tmp[] = array(
            "item_1" => $items[$i],
            "item_2" => $items[$ii]
          );
        }
      }
    }
  }

}
