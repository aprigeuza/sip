<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }


  public function get_payment_list($invoice_id)
  {
    return $this->db->get_where("invoice_payment", ["invoice_id" => $invoice_id, "cancel" => false]);
  }

  public function insert($data)
  {
    $this->db->insert("invoice_payment", $data);
  }

  public function cancel($id)
  {
    $data["cancel"] = true;
    $this->db->update("invoice_payment", $data, ["id" => $id]);
  }

  public function closed_invoice($invoice_id)
  {
    $data["closed"] = true;
    $this->db->update("invoice", $data, ["id" => $invoice_id]);
  }

}
