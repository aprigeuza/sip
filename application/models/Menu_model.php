<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function hierachy($parent_id=0)
  {
    $res = array();

    $un = $this->session->userdata("un");
    $lv = $this->session->userdata("lv");

    if($lv == "sa")
    {
      $this->db->order_by("sort_number", "ASC");
      $list = $this->db->get_where("menu", array("parent_id" => $parent_id));

      foreach ($list->result() as $item)
      {
        $item->child = $this->hierachy($item->id);

        $res[] = $item;
      }

    }
    else if($lv == "de")
    {
      $this->db->select("menu.*");
      $this->db->from("user_menu");
      $this->db->join("menu", "menu.id=user_menu.menu_id", "INNER");
      $this->db->join("user", "user.id=user_menu.user_id", "INNER");
      $this->db->where("menu.parent_id", $parent_id);
      $this->db->where("user.username", $un);
      $this->db->order_by("sort_number", "ASC");
      $list = $this->db->get();

      foreach ($list->result() as $item)
      {
        $item->child = $this->hierachy($item->id);

        $res[] = $item;
      }
    }

    return $res;
  }

  public function get_data($name)
  {
    $active_class = $this->db->get_where("menu", array("name" => $name))->row();
    return $active_class;
  }
}
