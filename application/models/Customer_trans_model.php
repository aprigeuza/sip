<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_trans_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("customer_trans", $data);

    return $this->db->affected_rows();
  }

  public function update($data, $id)
  {
    $this->db->update("customer_trans", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function obsolete($id)
  {
    $this->db->where("customer_trans.id", $id);
    $res = $this->db->get("customer_trans");
    $row = $res->row();
    $data["obsolete"] = !$row->obsolete;
    $this->db->update("customer_trans", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function table_data()
  {
    $this->db->select("customer_trans.*, user.username");
    $this->db->from("customer_trans");
    $this->db->join("user", "user.id=customer_trans.input_by");
    return $this->db->get();
  }

  public function get_list_active()
  {
    $this->db->select("customer_trans.*, user.username");
    $this->db->from("customer_trans");
    $this->db->join("user", "user.id=customer_trans.input_by");
    $this->db->where("customer_trans.obsolete", false);
    $res = $this->db->get();
    return $res;
  }

  public function get_data($id)
  {
    $this->db->select("customer_trans.*, user.username");
    $this->db->from("customer_trans");
    $this->db->join("user", "user.id=customer_trans.input_by");
    $this->db->where("customer_trans.id", $id);
    $res = $this->db->get();
    return $res->row();
  }

  public function code_exists($code)
  {
    $res = $this->db->get_where("customer_trans", ["cust_code" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }
}
