<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("invoice", $data);

    return $this->db->affected_rows();
  }

  public function update($data, $id)
  {
    $this->db->update("invoice", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function cancel($id)
  {
    $data["cancel"] = true;
    $this->db->update("invoice", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function posted($id)
  {
    $data["posted"] = true;
    $this->db->update("invoice", $data, ["id" => $id, "cancel" => false]);

    return $this->db->affected_rows();
  }

  public function unposted($id)
  {
    $this->db->from("invoice_payment");
    $this->db->join("invoice", "invoice.id=invoice_payment.invoice_id", "INNER");
    $this->db->where("invoice_payment.invoice_id", $id);
    $this->db->where("invoice_payment.cancel", false);
    $res = $this->db->get();

    if($res->num_rows())
    {
      return false;
    }
    else
    {
      $data["posted"] = false;
      $this->db->update("invoice", $data, ["id" => $id]);
      return $this->db->affected_rows();
    }
  }

  public function table_data()
  {
    $this->db->from("v_invoice");
    $this->db->where("cancel", false);
    return $this->db->get();
  }

  public function get_data($id)
  {
    $this->db->select("*");
    $this->db->from("v_invoice");
    $this->db->where("id", $id);
    $res = $this->db->get();
    $data = $res->row();

    $items = $this->db->get_where("v_invoice_item_detail", array("invoice_id" => $data->id));
    $data->items = $items->result();

    return $data;
  }

  public function number_exists($code)
  {
    $res = $this->db->get_where("invoice", ["iv_number" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }

  public function new_number()
  {
    $last_num = 1;

    $this->db->select("iv_number");
    $this->db->from("invoice");
    $this->db->like("iv_number", date("y"), "LEFT");
    $this->db->order_by("iv_number", "ASC");
    $res = $this->db->get();

    if($res->num_rows())
    {
      $row = $res->last_row();
      return (int)$row->iv_number + 1;
    }
    else
    {
      $last_num = date("ym") . "0001";

      return (int)$last_num;
    }
    return 0;
  }

  public function insert_draft()
  {
    $data["iv_number"] = $this->new_number();
    $data["iv_date"] = date("Y-m-d");
    $data["input_date"] = date("Y-m-d H:i:s");
    $data["input_by"] = $this->session->userdata("uid");
    $data["iv_note"] = get_setting("invoice_note");

    $sales_order = $this->db->get_where("sales_order", ["id" => get_session("finance_invoice_sales_order_id")])->row();

    $data["sales_order_id"] = $sales_order->id;
    $data["customer_id"] = $sales_order->customer_id;
    $data["cust_code"] = $sales_order->cust_code;
    $data["cust_name"] = $sales_order->cust_name;
    $data["telp"] = $sales_order->telp;
    $data["npwp"] = $sales_order->npwp;
    $data["billing_address"] = $sales_order->billing_address;

    $data["ppn"] = $sales_order->ppn;
    $data["shipping_cost"] = $sales_order->shipping_cost;
    $data["other_cost"] = $sales_order->other_cost;
    $data["deduction_dp"] = $sales_order->total_dp;

    $this->db->insert("invoice", $data);

    $invoice_id = $this->db->insert_id();

    $checked_id = get_session("finance_invoice_checked_id");

    foreach($checked_id as $id)
    {
      $delivery_order_item = $this->db->get_where("v_delivery_order_item_for_invoice", ["delivery_order_id" => $id]);

      foreach($delivery_order_item->result() as $do_item)
      {
        $data_item["invoice_id"] = $invoice_id;
        $data_item["delivery_order_item_id"] = $do_item->id;
        $data_item["item_id"] = $do_item->item_id;
        $data_item["item_code"] = $do_item->item_code;
        $data_item["item_name"] = $do_item->item_name;
        $data_item["item_description"] = $do_item->item_description;
        $data_item["qty"] = $do_item->qty;
        $data_item["unit"] = $do_item->unit;
        $data_item["unit_price"] = $do_item->unit_price;
        $data_item["subtotal"] = $do_item->qty * $do_item->unit_price;

        $this->db->insert("invoice_item", $data_item);
      }
    }

    return $invoice_id;
  }

  // Sales_order Item`

  public function insert_item($data)
  {
    $this->db->insert("invoice_item", $data);
    return $this->db->insert_id();
  }

  public function update_item($data, $id)
  {
    $this->db->update("invoice_item", $data, ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item($data, $id)
  {
    $this->db->delete("invoice_item", ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item_by_header($id)
  {
    $this->db->delete("invoice_item", ["invoice_id" => $id]);
    return $this->db->affected_rows();
  }

  public function get_posted_invoice()
  {
    $this->db->select("*");
    $this->db->from("invoice");
    $this->db->where("posted", true);
    $this->db->where("cancel", false);
    $res = $this->db->get();

    return $res;
  }

}
