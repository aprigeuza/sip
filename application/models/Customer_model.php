<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("customer", $data);

    return $this->db->insert_id();
  }

  public function update($data, $id)
  {
    $this->db->update("customer", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function obsolete($id)
  {
    $this->db->where("customer.id", $id);
    $res = $this->db->get("customer");
    $row = $res->row();
    $data["obsolete"] = !$row->obsolete;
    $this->db->update("customer", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function table_data()
  {
    $this->db->select("customer.*, user.username");
    $this->db->from("customer");
    $this->db->join("user", "user.id=customer.input_by");
    return $this->db->get();
  }

  public function get_list_active()
  {
    $this->db->select("customer.*, user.username");
    $this->db->from("customer");
    $this->db->join("user", "user.id=customer.input_by");
    $this->db->where("customer.obsolete", false);
    $res = $this->db->get();
    return $res;
  }

  public function get_data($id)
  {
    $this->db->select("customer.*, user.username");
    $this->db->from("customer");
    $this->db->join("user", "user.id=customer.input_by");
    $this->db->where("customer.id", $id);
    $res = $this->db->get();
    return $res->row();
  }

  public function code_exists($code)
  {
    $res = $this->db->get_where("customer", ["cust_code" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }
}
