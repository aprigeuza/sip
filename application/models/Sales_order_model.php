<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_order_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("sales_order", $data);

    return $this->db->affected_rows();
  }

  public function update($data, $id)
  {
    $this->db->update("sales_order", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function cancel($id)
  {
    $data["cancel"] = true;
    $this->db->update("sales_order", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function posted($id)
  {
    $data["posted"] = true;
    $this->db->update("sales_order", $data, ["id" => $id, "cancel" => false]);

    return $this->db->affected_rows();
  }

  public function unposted($id)
  {
    $this->db->from("delivery_order_item");
    $this->db->join("delivery_order", "delivery_order.id=delivery_order_item.delivery_order_id", "INNER");
    $this->db->join("sales_order_item", "sales_order_item.id=delivery_order_item.sales_order_item_id", "INNER");
    $this->db->where("sales_order_item.sales_order_id", $id);
    $this->db->where("delivery_order.cancel", false);
    $this->db->where("delivery_order.posted", true);
    $res = $this->db->get();

    if($res->num_rows())
    {
      return false;
    }
    else
    {
      $data["posted"] = false;
      $this->db->update("sales_order", $data, ["id" => $id]);
      return $this->db->affected_rows();
    }
  }


  public function table_data()
  {
    $this->db->from("v_sales_order");
    $this->db->where("cancel", false);
    return $this->db->get();
  }

  public function get_data($id)
  {
    $this->db->select("*");
    $this->db->from("v_sales_order");
    $this->db->where("id", $id);
    $res = $this->db->get();
    $data = $res->row();

    $items = $this->db->get_where("sales_order_item", array("sales_order_id" => $data->id));
    $data->items = $items->result();

    return $data;
  }

  public function number_exists($code)
  {
    $res = $this->db->get_where("sales_order", ["so_number" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }

  public function new_number()
  {
    $prefix = $this->config->item("so_number_prefix");
    $last_num = 1;

    $this->db->select("so_number");
    $this->db->from("sales_order");
    $this->db->like("so_number", $prefix . date("y"), "LEFT");
    $this->db->order_by("so_number", "ASC");
    $res = $this->db->get();

    if($res->num_rows())
    {
      $row = $res->last_row();

      $n = substr($row->so_number, -4);

      $_n = (int)$n + 1;

      $new_number = $prefix . date("ym") . sprintf ("%'.04d\n", (int)$_n);;

      return $new_number;
    }
    else
    {
      $last_num = $prefix . date("ym") . "0001";

      return $last_num;
    }
    return 0;
  }

  public function insert_draft()
  {
    $data["so_number"] = $this->new_number();
    $data["so_date"] = date("Y-m-d");
    $data["input_date"] = date("Y-m-d H:i:s");
    $data["input_by"] = $this->session->userdata("uid");
    $this->db->insert("sales_order", $data);

    return $this->db->insert_id();
  }

  // Sales_order Item`

  public function insert_item($data)
  {
    $this->db->insert("sales_order_item", $data);
    return $this->db->insert_id();
  }

  public function update_item($data, $id)
  {
    $this->db->update("sales_order_item", $data, ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item($data, $id)
  {
    $this->db->delete("sales_order_item", ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item_by_header($id)
  {
    $this->db->delete("sales_order_item", ["sales_order_id" => $id]);
    return $this->db->affected_rows();
  }

  public function search($keywords="")
  {
    $this->db->select("*");
    $this->db->from("v_sales_order");
    $this->db->like("so_number", $keywords, "BOTH");
    $this->db->or_like("cust_code", $keywords, "BOTH");
    $this->db->or_like("cust_name", $keywords, "BOTH");
    $this->db->limit(100);
    $res = $this->db->get();

    return $res;
  }

  public function get_posted_sales()
  {
    $this->db->select("*");
    $this->db->from("v_sales_order");
    $this->db->where("posted", true);
    $this->db->where("cancel", false);
    $res = $this->db->get();

    return $res;
  }

}
