<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_order_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("delivery_order", $data);

    return $this->db->affected_rows();
  }

  public function update($data, $id)
  {
    $this->db->update("delivery_order", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function cancel($id)
  {
    $data["cancel"] = true;
    $this->db->update("delivery_order", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function posted($id)
  {
    $data["posted"] = true;
    $this->db->update("delivery_order", $data, ["id" => $id, "cancel" => false]);

    return $this->db->affected_rows();
  }

  public function unposted($id)
  {
    $this->db->from("invoice_item");
    $this->db->join("invoice", "invoice.id=invoice_item.invoice_id", "INNER");
    $this->db->join("delivery_order_item", "delivery_order_item.id=invoice_item.delivery_order_item_id");
    $this->db->where("delivery_order_item.delivery_order_id", $id);
    $this->db->where("invoice.cancel", false);
    $this->db->where("invoice.posted", true);
    $res = $this->db->get();

    if($res->num_rows())
    {
      return false;
    }
    else
    {
      $data["posted"] = false;
      $this->db->update("delivery_order", $data, ["id" => $id]);
      return $this->db->affected_rows();
    }
  }

  public function table_data()
  {
    $this->db->from("v_delivery_order");
    $this->db->where("cancel", false);
    return $this->db->get();
  }

  public function get_data($id)
  {
    $this->db->select("*");
    $this->db->from("v_delivery_order");
    $this->db->where("id", $id);
    $res = $this->db->get();
    $data = $res->row();

    $items = $this->db->get_where("delivery_order_item", array("delivery_order_id" => $data->id));
    $data->items = $items->result();

    return $data;
  }

  public function number_exists($code)
  {
    $res = $this->db->get_where("delivery_order", ["do_number" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }

  public function new_number()
  {
    $prefix = $this->config->item("do_number_prefix");
    $last_num = 1;

    $this->db->select("do_number");
    $this->db->from("delivery_order");
    $this->db->like("do_number", $prefix . date("y"), "LEFT");
    $this->db->order_by("do_number", "ASC");
    $res = $this->db->get();

    if($res->num_rows())
    {
      $row = $res->last_row();

      $n = substr($row->do_number, -4);

      $_n = (int)$n + 1;

      $new_number = $prefix . date("ym") . sprintf ("%'.04d\n", (int)$_n);;

      return $new_number;
    }
    else
    {
      $last_num = $prefix . date("ym") . "0001";

      return $last_num;
    }
    return 0;
  }

  public function insert_draft()
  {
    $data["do_number"] = $this->new_number();
    $data["do_date"] = date("Y-m-d");
    $data["input_date"] = date("Y-m-d H:i:s");
    $data["input_by"] = $this->session->userdata("uid");

    $sales_order = $this->db->get_where("sales_order", ["id" => get_session("procurement_delivery_order_sales_order_id")])->row();

    $data["sales_order_id"] = $sales_order->id;
    $data["customer_id"] = $sales_order->customer_id;
    $data["cust_code"] = $sales_order->cust_code;
    $data["cust_name"] = $sales_order->cust_name;
    $data["telp"] = $sales_order->telp;
    $data["shipping_address"] = $sales_order->shipping_address;

    $this->db->insert("delivery_order", $data);

    $delivery_order_id = $this->db->insert_id();

    $tmp_items = get_session("procurement_delivery_order_checked_id");

    foreach($tmp_items as $item)
    {
      $_item = $this->db->get_where("v_sales_order_for_delivery_order", ["sales_order_item_id" => $item]);
      if($_item->num_rows())
      {
        $_row_item = $_item->row();

        if($_row_item->total_outstanding)
        {
          $data_item["delivery_order_id"] = $delivery_order_id;
          $data_item["sales_order_item_id"] = $_row_item->sales_order_item_id;
          $data_item["item_id"] = $_row_item->item_id;
          $data_item["item_code"] = $_row_item->item_code;
          $data_item["item_name"] = $_row_item->item_name;
          $data_item["item_description"] = $_row_item->item_description;
          $data_item["qty"] = $_row_item->total_outstanding;
          $data_item["unit"] = $_row_item->unit;
          $this->db->insert("delivery_order_item", $data_item);
        }
      }
    }

    return $delivery_order_id;
  }

  // Sales_order Item`

  public function insert_item($data)
  {
    $this->db->insert("delivery_order_item", $data);
    return $this->db->insert_id();
  }

  public function update_item($data, $id)
  {
    $this->db->update("delivery_order_item", $data, ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item($data, $id)
  {
    $this->db->delete("delivery_order_item", ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item_by_header($id)
  {
    $this->db->delete("delivery_order_item", ["delivery_order_id" => $id]);
    return $this->db->affected_rows();
  }

}
