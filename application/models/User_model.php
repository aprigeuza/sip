<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();
  }

  public function validate_login_session()
  {
    $username = $this->session->userdata("un");
    $password = $this->session->userdata("pw");

    $validate = $this->db->get_where("user", array("username" => $username, "obsolete" => FALSE));

    if($validate->num_rows() == 1)
    {
      $row = $validate->row();

      $decrypt_row_password = $this->encryption->decrypt($row->password);
      $decrypt_ses_password = $this->encryption->decrypt($password);
      if($decrypt_row_password === $decrypt_ses_password)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    return false;
  }

  public function check_login($username, $password)
  {
    $validate = $this->db->get_where("user", array("username" => $username, "obsolete" => FALSE));

    if($validate->num_rows() == 1)
    {
      $row = $validate->row();
      $decrypt_row_password = $this->encryption->decrypt($row->password);

      if($decrypt_row_password === $password)
      {
        // Create session
        $user = $validate->row();

        $this->session->set_userdata("uid", $user->id);
        $this->session->set_userdata("un", $user->username);
        $this->session->set_userdata("pw", $user->password);
        $this->session->set_userdata("lv", $user->level);
        $this->session->set_userdata("ll", date("Y-m-d H:i:s"));

        $this->db->update("user", array("last_login" => date("Y-m-d H:i:s")), array("username" => $user->username));

        return true;
      }
      else
      {
        return false;
      }
    }
    return false;
  }

  public function logout()
  {
    $this->session->unset_userdata("un");
    $this->session->unset_userdata("pw");
    $this->session->unset_userdata("lv");
    return true;
  }

  public function get_list()
  {
    $res = $this->db->get("user");

    return $res;
  }

  public function check_data($id)
  {
    $res = $this->db->get_where("user", array("id" => $id));
    if($res->num_rows()) return true;
    return false;
  }

  public function check_username($username)
  {
    $res = $this->db->get_where("user", array("username" => $username));
    if($res->num_rows()) return true;
    return false;
  }

  public function get_data($id)
  {
    $res = $this->db->get_where("user", array("id" => $id));

    return $res->row();
  }

  public function insert($data)
  {
    return $this->db->insert("user", $data);
  }

  public function update($data, $id)
  {
    return $this->db->update("user", $data, array("id" => $id));
  }

  public function delete($id)
  {
    return $this->db->delete("user", array("id" => $id));
  }


  public function validate_access()
  {
    $un = $this->session->userdata("un");
    $lv = $this->session->userdata("lv");
    if($lv=="sa") return true;
    $mn = $this->router->fetch_class();
    $this->db->select("menu.*");
    $this->db->from("user_menu");
    $this->db->join("menu", "menu.id=user_menu.menu_id", "INNER");
    $this->db->join("user", "user.id=user_menu.user_id", "INNER");
    $this->db->where("menu.name", $mn);
    $this->db->where("user.username", $un);
    $list = $this->db->get();
    if($list->num_rows())
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public function user_menu($id, $parent_id=0)
  {
    $res = array();
    $this->db->select("menu.*, MENU_ACCESS(".$id.",menu.id) AS checked");
    $this->db->from("menu");
    $this->db->where("parent_id", $parent_id);
    $this->db->order_by("sort_number", "ASC");
    $list = $this->db->get();

    foreach ($list->result() as $item)
    {
      $item->child = $this->user_menu($id, $item->id);

      $res[] = $item;
    }


    return $res;
  }

  public function user_menu_prepare_update($user_id)
  {
    $where["user_id"] = $user_id;
    $this->db->delete("user_menu", $where);
  }

  public function user_menu_update($user_id, $menu_id)
  {
    $data["id"] = $user_id.".".$menu_id;
    $data["user_id"] = $user_id;
    $data["menu_id"] = $menu_id;
    $this->db->replace("user_menu", $data);
  }

  public function change_password($new_password)
  {
    $un = $this->session->userdata("un");

    $data["password"] = $this->encryption->encrypt($new_password);
    $this->db->update("user", $data, array("username" => $un));

    return true;
  }
}
