<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function insert($data)
  {
    $this->db->insert("purchase_order", $data);

    return $this->db->affected_rows();
  }

  public function update($data, $id)
  {
    $this->db->update("purchase_order", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function cancel($id)
  {
    $data["cancel"] = true;
    $this->db->update("purchase_order", $data, ["id" => $id]);

    return $this->db->affected_rows();
  }

  public function posted($id)
  {
    $data["posted"] = true;
    $this->db->update("purchase_order", $data, ["id" => $id, "cancel" => false]);

    return $this->db->affected_rows();
  }

  public function unposted($id)
  {
    $data["posted"] = false;
    $this->db->update("purchase_order", $data, ["id" => $id]);
    return $this->db->affected_rows();
  }


  public function table_data()
  {
    $this->db->from("v_purchase_order");
    $this->db->where("cancel", false);
    return $this->db->get();
  }

  public function get_data($id)
  {
    $this->db->select("*");
    $this->db->from("v_purchase_order");
    $this->db->where("id", $id);
    $res = $this->db->get();
    $data = $res->row();

    $items = $this->db->get_where("purchase_order_item", array("purchase_order_id" => $data->id));
    $data->items = $items->result();

    return $data;
  }

  public function number_exists($code)
  {
    $res = $this->db->get_where("purchase_order", ["po_number" => $code]);
    if($res->num_rows())
    {
      return true;
    }
    return false;
  }

  public function new_number()
  {
    $prefix = $this->config->item("po_number_prefix");
    $last_num = 1;

    $this->db->select("po_number");
    $this->db->from("purchase_order");
    $this->db->like("po_number", $prefix . date("y"), "LEFT");
    $this->db->order_by("po_number", "ASC");
    $res = $this->db->get();

    if($res->num_rows())
    {
      $row = $res->last_row();

      $n = substr($row->po_number, -4);

      $_n = (int)$n + 1;

      $new_number = $prefix . date("ym") . sprintf ("%'.04d\n", (int)$_n);;

      return $new_number;
    }
    else
    {
      $last_num = $prefix . date("ym") . "0001";

      return $last_num;
    }
    return 0;
  }

  public function insert_draft()
  {
    $data["po_number"] = $this->new_number();
    $data["po_date"] = date("Y-m-d");
    $data["input_date"] = date("Y-m-d H:i:s");
    $data["input_by"] = $this->session->userdata("uid");
    $data["po_note"] = get_setting("purchase_order_note");
    $this->db->insert("purchase_order", $data);

    return $this->db->insert_id();
  }

  // Sales_order Item`

  public function insert_item($data)
  {
    $this->db->insert("purchase_order_item", $data);
    return $this->db->insert_id();
  }

  public function update_item($data, $id)
  {
    $this->db->update("purchase_order_item", $data, ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item($data, $id)
  {
    $this->db->delete("purchase_order_item", ["id" => $id]);
    return $this->db->affected_rows();
  }

  public function delete_item_by_header($id)
  {
    $this->db->delete("purchase_order_item", ["purchase_order_id" => $id]);
    return $this->db->affected_rows();
  }

  public function search($keywords="")
  {
    $this->db->select("*");
    $this->db->from("v_purchase_order");
    $this->db->like("po_number", $keywords, "BOTH");
    $this->db->or_like("supp_code", $keywords, "BOTH");
    $this->db->or_like("supp_name", $keywords, "BOTH");
    $res = $this->db->get();

    return $res;
  }

  public function get_posted_sales()
  {
    $this->db->select("*");
    $this->db->from("v_purchase_order");
    $this->db->where("posted", true);
    $this->db->where("cancel", false);
    $res = $this->db->get();

    return $res;
  }

}
