<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('validate_login_session'))
{
	function validate_login_session()
	{
		$CI =& get_instance();
		$CI->load->model("user_model");
		$check = $CI->user_model->validate_login_session();

		if($check==false) redirect ("login");

		return $check;
	}
}
if ( ! function_exists('validate_access'))
{
	function validate_access()
	{
		$CI =& get_instance();
		$CI->load->model("user_model");
		$check = $CI->user_model->validate_access();

		if($check==false) redirect ("404");

		return $check;
	}
}
