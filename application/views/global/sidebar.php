
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <i class="fa fa-user fa-3x" style="color:#fff"></i>
      </div>
      <div class="pull-left info">
        <p><?php echo get_session("un"); ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <?php echo show_sidebar_menu(); ?>
  </section>
  <!-- /.sidebar -->
</aside>
