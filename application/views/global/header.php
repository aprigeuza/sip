

<header class="main-header">
  <!-- Logo -->
  <a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>SI</b>P</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>SI</b>P</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user-o"></i>
            <span class="hidden-xs">Welcome, <?php echo get_session("un"); ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <i class="fa fa-user fa-3x" style="color:#fff"></i>
              <p>
                <?php echo get_session("un"); ?>
                <small>Last Loged-in : <br><?php echo get_session("ll"); ?></small>
              </p>
            </li>
            <!-- Menu Body -->
            <!-- <li class="user-body">
              <div class="row">
                <div class="col-xs-6 text-center">
                  <a href="#" data-toggle="modal" data-target="#modalChangePassword">Ubah Password</a>
                </div>
                <div class="col-xs-6 text-center">
                  <a href="<?php echo set_url("setting/user/log_activity"); ?>">Lihat Aktivitas</a>
                </div>
              </div>
            </li> -->
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" data-toggle="modal" data-target="#modalChangePassword" class="btn btn-primary btn-flat">Change Password</a>
                <!-- <a href="<?php echo set_url("setting/user/my_profile"); ?>" class="btn btn-default btn-flat">Profile</a> -->
              </div>
              <div class="pull-right">
                <a href="<?php echo set_url("logout"); ?>" class="btn btn-warning btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <!-- <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li> -->
      </ul>
    </div>
  </nav>
</header>

<form action="<?php echo set_url("setting/user/change_password"); ?>" method="post">
  <div class="modal fade" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Change Password</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">Old Password</label>
            <input class="form-control" type="password" name="old_password" value="">
          </div>
          <hr>
          <div class="form-group">
            <label for="">New Password</label>
            <input class="form-control" type="password" name="new_password" value="">
          </div>
          <div class="form-group">
            <label for="">Type New Password Again</label>
            <input class="form-control" type="password" name="new_password2" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit & Logout</button>
        </div>
      </div>
    </div>
  </div>
</form>
