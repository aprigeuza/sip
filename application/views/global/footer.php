
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> <?php echo $app_version; ?>
  </div>
  <strong><?php echo $app_copyright; ?> <a href="<?php echo $app_pondasiweb_url; ?>"><?php echo $app_pondasiweb_txt; ?></a>.</strong> All rights
  reserved.
</footer>
